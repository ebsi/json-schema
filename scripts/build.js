/* eslint-disable no-restricted-syntax, no-await-in-loop */
import { dirname, basename, resolve } from "node:path";
import { fileURLToPath } from "node:url";
import fs from "node:fs";
import { globbySync } from "globby";
import $RefParser from "@apidevtools/json-schema-ref-parser";
import { Project, ScriptTarget } from "ts-morph";
import * as prettier from "prettier";
import { compile } from "json-schema-to-typescript";
import { generateName } from "json-schema-to-typescript/dist/src/utils.js";
import camelCase from "lodash.camelcase";

const ignoreList = [];

// Find all schemas (except the broken ones)
const currentDir = dirname(fileURLToPath(import.meta.url));
const rootDir = resolve(currentDir, "..");
const schemas = globbySync(["schemas/**/schema.json"], {
  cwd: rootDir,
  ignore: ignoreList,
  gitignore: true,
});

async function exportsSchemas() {
  for (const schemaFile of schemas) {
    // Extra title and description from schema
    const dataString = fs.readFileSync(resolve(rootDir, schemaFile), "utf8");
    const { title } = JSON.parse(dataString);

    // Bundle schema
    const bundledSchema = await $RefParser.bundle(resolve(rootDir, schemaFile));

    // Import metadata
    const schemaMetadataFile = schemaFile.replace(
      "schema.json",
      "schema.metadata.json",
    );
    const metadata = fs.readFileSync(
      resolve(rootDir, schemaMetadataFile),
      "utf8",
    );

    // Make sure the dist folder exists and is empty
    const distDir = resolve(dirname(resolve(rootDir, schemaFile)), "dist");
    fs.rmSync(distDir, { recursive: true, force: true });
    fs.mkdirSync(distDir, { recursive: true });

    // Export types
    const schemaTypings = await compile(bundledSchema, "Schema", {
      strictIndexSignatures: true,
      format: false,
      bannerComment: false,
    });

    // Find examples
    const exampleFiles = globbySync(["examples/**/*.json"], {
      cwd: dirname(schemaFile),
      ignore: ignoreList,
      gitignore: true,
    });

    const hasExamples = exampleFiles.length > 0;

    const examples = {};
    for (const exampleFile of exampleFiles) {
      const example = JSON.parse(
        fs.readFileSync(resolve(dirname(schemaFile), exampleFile), "utf8"),
      );
      examples[camelCase(basename(exampleFile).split(".")[0])] = example;
    }

    // Create TypeScript project
    const project = new Project({
      compilerOptions: {
        outDir: distDir,
        declaration: true,
        target: ScriptTarget.ES2021,
      },
    });

    // Stringify examples
    const typeName = generateName(title, new Set());
    const examplesTs = hasExamples
      ? `export const examples = {
${Object.keys(examples)
  .map((exampleName) => {
    return `  "${exampleName}": ${JSON.stringify(examples[exampleName])}${typeName ? ` satisfies ${typeName}` : ""},`;
  })
  .join("\n")}
} as const;`
      : "";

    // Create virtual index.ts file to be compiled to JS
    const indexTs = `export const metadata = ${JSON.stringify(JSON.parse(metadata))} as const;
export const schema = ${JSON.stringify(bundledSchema)} as const;
${examplesTs}
${schemaTypings}`;

    project.createSourceFile("index.ts", indexTs);

    // Emit JS and declaration files to memory
    const result = project.emitToMemory();

    // Run Prettier on emitted files
    for (const file of result.getFiles()) {
      file.text = await prettier.format(file.text, { parser: "typescript" });
    }

    // Write files to disk
    await result.saveFiles();
  }
}

exportsSchemas();
