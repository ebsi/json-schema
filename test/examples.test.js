import { dirname, resolve } from "node:path";
import { readFileSync } from "node:fs";
import { fileURLToPath } from "node:url";
import { test, expect } from "vitest";
import Ajv2020 from "ajv/dist/2020.js";
import addFormats from "ajv-formats";
import $RefParser from "@apidevtools/json-schema-ref-parser";
import { globbySync } from "globby";

// Ignore list
const ignoreList = ["schemas/vcdm2.0/timestamp-token/**/*"];

// Find all examples
const examples = [];
const currentDir = dirname(fileURLToPath(import.meta.url));
const rootDir = resolve(currentDir, "..");
const matches = globbySync(["schemas/**/examples/**/*.json"], {
  cwd: rootDir,
  ignore: ignoreList,
  gitignore: true,
});
matches.forEach((exampleFile) => {
  const schemaRoot = dirname(exampleFile).replace(/\/examples\/?.*/, "");
  examples.push({
    exampleFile,
    schemaFile: resolve(schemaRoot, "./schema.json"),
  });
});

const customAjvOptions = {
  "schemas/vcdm2.0/ehic/schema.json": {
    // See https://github.com/ajv-validator/ajv/issues/1571
    strictRequired: false,
    // See https://github.com/ajv-validator/ajv/issues/1898#issuecomment-1043618403
    strictTypes: false,
  },
};

test.each(examples)(
  "Example $exampleFile should be valid",
  async ({ exampleFile, schemaFile }) => {
    // Bundle schema
    const bundledSchema = await $RefParser.bundle(schemaFile);

    // Configure Ajv
    let ajv;
    switch (bundledSchema.$schema) {
      case "https://json-schema.org/draft/2020-12/schema": {
        ajv = new Ajv2020({
          allErrors: true,
          strict: true,
          ...(customAjvOptions[schemaFile.replace(`${rootDir}/`, "")] ?? {}),
        });
        break;
      }
      default: {
        throw new Error(`Unknown version "${bundledSchema.$schema}"`);
      }
    }
    addFormats(ajv);

    // Validate example
    const validate = ajv.compile(bundledSchema);
    const exampleData = JSON.parse(readFileSync(resolve(rootDir, exampleFile)));
    const valid = validate(exampleData);

    // Check results
    if (validate.errors) {
      console.error(validate.errors);
    }
    expect(validate.errors).toBeNull();
    expect(valid).toBe(true);
  },
);
