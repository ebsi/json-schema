import { dirname, resolve } from "node:path";
import { fileURLToPath } from "node:url";
import { describe, test, expect } from "vitest";
import Ajv2020 from "ajv/dist/2020.js";
import addFormats from "ajv-formats";
import $RefParser from "@apidevtools/json-schema-ref-parser";
import { globbySync } from "globby";

const ignoreList = [];

// Find all schemas (except the broken ones)
const currentDir = dirname(fileURLToPath(import.meta.url));
const rootDir = resolve(currentDir, "..");
const schemas = globbySync(["schemas/**/schema.json"], {
  cwd: rootDir,
  ignore: ignoreList,
  gitignore: true,
});

const customAjvOptions = {
  "schemas/vcdm2.0/ehic/schema.json": {
    // See https://github.com/ajv-validator/ajv/issues/1571
    strictRequired: false,
    // See https://github.com/ajv-validator/ajv/issues/1898#issuecomment-1043618403
    strictTypes: false,
  },
};

describe.each(schemas)("Schema %s", (schemaFile) => {
  test("should compile without errors (using Ajv)", async () => {
    // Bundle schema
    const bundledSchema = await $RefParser.bundle(resolve(rootDir, schemaFile));

    // Configure Ajv
    let ajv;
    switch (bundledSchema.$schema) {
      // https://github.com/json-schema-org/json-schema-spec/blob/2020-12/schema.json
      case "https://json-schema.org/draft/2020-12/schema": {
        if (customAjvOptions[schemaFile]) {
          console.warn(
            `Using custom options for ${schemaFile}: ${JSON.stringify(customAjvOptions[schemaFile])}`,
          );
        }

        ajv = new Ajv2020({
          allErrors: true,
          strict: true,
          ...(customAjvOptions[schemaFile] ?? {}),
        });
        break;
      }
      default: {
        throw new Error(`Unknown version "${bundledSchema.$schema}"`);
      }
    }
    addFormats(ajv);

    // Should compile without errors
    expect(ajv.compile(bundledSchema)).not.toThrow();
  });
});
