import { dirname, resolve } from "node:path";
import { fileURLToPath } from "node:url";
import fs from "node:fs";
import { describe, test, expect } from "vitest";
import { globbySync } from "globby";
import * as prettier from "prettier";
import { computeId } from "../cli/utils/jsonSchema.js";

describe.each(["1.1", "2.0"])("VCDM %s schemas", (vcdmVersion) => {
  const ignoreList = [];

  // Find all schemas (except the broken ones)
  const currentDir = dirname(fileURLToPath(import.meta.url));
  const rootDir = resolve(currentDir, "..");
  const schemas = globbySync([`schemas/vcdm${vcdmVersion}/**/schema.json`], {
    cwd: rootDir,
    ignore: ignoreList,
    gitignore: true,
  });

  describe.each(schemas)("The metadata of %s", (schemaFile) => {
    test("should contain the correct VCDM version and schema IDs", async () => {
      const { base16: idBase16, multibase_base58btc: idBase58 } =
        await computeId(schemaFile);

      const schemaMetadataFile = schemaFile.replace(
        "schema.json",
        "schema.metadata.json",
      );

      let metadata = {};

      if (fs.existsSync(schemaMetadataFile)) {
        const metadataString = fs.readFileSync(schemaMetadataFile, "utf8");
        metadata = JSON.parse(metadataString);
      }

      const { vcdm, id, ...rest } = metadata;

      const snapshot = await prettier.format(
        JSON.stringify({
          vcdm: vcdmVersion,
          id: {
            base16: idBase16,
            multibase_base58btc: idBase58,
          },
          ...rest,
        }),
        {
          parser: "json",
        },
      );

      await expect(snapshot).toMatchFileSnapshot(
        resolve(currentDir, "..", schemaMetadataFile),
      );
    });
  });
});
