import { dirname, resolve } from "node:path";
import { fileURLToPath } from "node:url";
import fs from "node:fs";
import { test, expect } from "vitest";
import { globbySync } from "globby";
import * as prettier from "prettier";
import { computeId } from "../cli/utils/jsonSchema.js";

const ignoreList = [];

// Find all schemas (except the broken ones)
const currentDir = dirname(fileURLToPath(import.meta.url));
const rootDir = resolve(currentDir, "..");
const schemas = globbySync(["schemas/**/schema.json"], {
  cwd: rootDir,
  ignore: ignoreList,
  gitignore: true,
});

test("Manifest should match the snapshot", async () => {
  const summary = (
    await Promise.all(
      schemas.map(async (schemaFile) => {
        const { base16: idBase16, multibase_base58btc: idBase58 } =
          await computeId(schemaFile);

        // Extra title and description from schema
        const dataString = fs.readFileSync(schemaFile, "utf8");
        const data = JSON.parse(dataString);

        // Extract package name and version from package.json
        const packageJson = schemaFile.replace("schema.json", "package.json");
        const packageJsonString = fs.readFileSync(packageJson, "utf8");
        const {
          name,
          version,
          private: isPrivate,
        } = JSON.parse(packageJsonString);

        if (isPrivate) {
          return null;
        }

        // Extract metadata
        const schemaMetadataFile = schemaFile.replace(
          "schema.json",
          "schema.metadata.json",
        );

        const metadataString = fs.readFileSync(schemaMetadataFile, "utf8");
        const metadata = JSON.parse(metadataString);

        const { id, ...rest } = metadata;

        return {
          name,
          version,
          title: data.title,
          description: data.description,
          idBase16,
          idBase58,
          file: schemaFile,
          ...rest,
        };
      }),
    )
  ).filter(Boolean);

  const snapshot = await prettier.format(JSON.stringify(summary), {
    parser: "json",
  });

  await expect(snapshot).toMatchFileSnapshot("../manifest.json");
});
