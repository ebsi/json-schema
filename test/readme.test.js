import { dirname, resolve } from "node:path";
import { fileURLToPath } from "node:url";
import fs from "node:fs";
import { describe, test, expect } from "vitest";
import { globbySync } from "globby";
import * as prettier from "prettier";
import { generateName } from "json-schema-to-typescript/dist/src/utils.js";
import { computeId } from "../cli/utils/jsonSchema.js";

const ignoreList = [];

// Find all schemas (except the broken ones)
const currentDir = dirname(fileURLToPath(import.meta.url));
const rootDir = resolve(currentDir, "..");
const schemas = globbySync([`schemas/**/schema.json`], {
  cwd: rootDir,
  ignore: ignoreList,
  gitignore: true,
});

describe.each(schemas)("The README of %s", (schemaFile) => {
  test("should contain the latest information", async () => {
    const { base16: idBase16, multibase_base58btc: idBase58 } =
      await computeId(schemaFile);

    // Extra title and description from schema
    const dataString = fs.readFileSync(schemaFile, "utf8");
    const { title, description } = JSON.parse(dataString);

    // Extract package name and version from package.json
    const packageJson = schemaFile.replace("schema.json", "package.json");
    const packageJsonString = fs.readFileSync(packageJson, "utf8");
    const { name, version } = JSON.parse(packageJsonString);

    const snapshot = await prettier.format(
      `
![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# ${name}

> ${title}
>
> ${description}

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:
- \`${idBase16}\` (hexadecimal)
- \`${idBase58}\` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

\`\`\`json
${dataString}
\`\`\`

## Installation

\`\`\`sh
# with npm
npm add ${name}@${version}

# with Yarn
yarn add ${name}@${version}

# with pnpm
pnpm add ${name}@${version}
\`\`\`

## Usage

The package exports the schema and its metadata as JavaScript objects:

\`\`\`js
import { schema, metadata } from "${name}";

// you can now use the schema and metadata
\`\`\`

In addition, the package exports a TypeScript type corresponding to the schema:

\`\`\`ts
import type { ${generateName(title, new Set())} } from "${name}";
\`\`\`

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
`,
      {
        parser: "markdown",
      },
    );

    const readmeFile = schemaFile.replace("schema.json", "README.md");

    await expect(snapshot).toMatchFileSnapshot(
      resolve(currentDir, "..", readmeFile),
    );
  });
});
