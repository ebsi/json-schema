/* eslint-disable no-param-reassign */
import inquirer from "inquirer";
import { Listr } from "listr2";
import axios from "axios";
import { hexToBytes, ES256Signer } from "did-jwt";
import { requestSiopJwt } from "../utils/requestSiopJwt.js";
import { getAccessToken } from "../utils/getAccessToken.js";
import { publishSchema } from "../utils/ledger.js";
import { getSchemaIds, validateSchema } from "../utils/jsonSchema.js";
import CONFIG from "../config.js";

export default (program) =>
  program
    .command("publish")
    .description("Publish the JSON Schema to the Trusted Schemas Registry")
    .argument("[paths...]", "Path(s) to JSON Schema(s)")
    .option("--all", "Publish all JSON Schemas")
    .option("-e, --env <env>", "EBSI Environment (test, conformance, pilot)")
    .action(async (paths, options) => {
      // Get IDs of the JSON Schemas the user wants to publish
      const schemaIds = await getSchemaIds(paths, options);

      let { env } = options;
      if (!env) {
        if (process.env.EBSI_ENV) {
          env = process.env.EBSI_ENV;
        } else {
          const answers = await inquirer.prompt([
            {
              type: "list",
              message: "EBSI environment?",
              name: "env",
              choices: [
                { value: "test" },
                { value: "conformance" },
                { value: "pilot" },
              ],
            },
          ]);
          env = answers.env;
        }
      }

      const allSchemaIds = await getSchemaIds([], { all: true });

      let kid;
      let privateKey;

      if (process.env.KID && process.env.PRIVATE_KEY) {
        kid = process.env.KID;
        privateKey = process.env.PRIVATE_KEY;
      } else {
        // Request user KID and private key
        const answers = await inquirer.prompt([
          {
            type: "input",
            name: "kid",
            message: "What's your KID?",
          },
          {
            type: "password",
            name: "privateKey",
            message: "Enter your hexadecimal private key (prefixed with 0x)",
          },
        ]);
        kid = answers.kid;
        privateKey = answers.privateKey;
      }

      let apisVersion; // "v2", "v3", "v4"
      if (
        process.env.TSR_API_VERSION &&
        (env === "test" ? ["v2", "v3", "v4"] : ["v2", "v3"]).includes(
          process.env.TSR_API_VERSION,
        )
      ) {
        apisVersion = process.env.TSR_API_VERSION;
      } else {
        // Request TSR API version
        const answers = await inquirer.prompt([
          {
            type: "list",
            name: "version",
            message: `On which version of TSR API do you want to publish the schema${schemaIds.length > 1 ? "s" : ""}?`,
            choices:
              env === "test"
                ? [{ value: "v2" }, { value: "v3" }, { value: "v4" }]
                : [{ value: "v2" }, { value: "v3" }],
          },
        ]);
        apisVersion = answers.version;
      }

      console.error("apisVersion", apisVersion);
      const tasks = new Listr(
        [
          {
            title: `Validating schema${schemaIds.length > 1 ? "s" : ""}`,
            task: async (ctx, task) => {
              // 1. List all the schemas to publish, including their dependencies
              const allSchemasToPublish = await schemaIds.reduce(
                async (promiseChain, { schema }) => {
                  const validSchemas = await promiseChain;
                  const schemas = await validateSchema(
                    schema,
                    CONFIG[env][apisVersion].TSR,
                    allSchemaIds,
                  );
                  return [...validSchemas, ...schemas];
                },
                Promise.resolve([]),
              );

              // 2. Filter duplicates
              const uniqueSchemas = allSchemasToPublish.filter(
                (element, index) =>
                  // Only keep the first occurrence of each schema
                  allSchemasToPublish.findIndex(
                    (el) => el.schema === element.schema,
                  ) === index,
              );

              ctx.schemasToPublish = uniqueSchemas;
              task.title = "Validation complete";
            },
          },
          apisVersion === "v2"
            ? {
                title: "Requesting SIOP JWT...",
                task: async (ctx, task) => {
                  // Get SIOP JWT
                  try {
                    const accessToken = await requestSiopJwt({
                      clientKid: kid,
                      clientPrivateKey: privateKey,
                      authorisationApiUrl: CONFIG[env][apisVersion].AUTH,
                      trustedAppsRegistryUrl: CONFIG[env][apisVersion].TAR,
                    });

                    ctx.accessToken = accessToken;
                    task.title = "SIOP JWT successfully requested";
                  } catch (e) {
                    if (process.env.DEBUG === "true") {
                      console.error(e);
                    }

                    // Handle Problem Details errors
                    if (
                      axios.isAxiosError(e) &&
                      e.response?.data &&
                      typeof e.response.data === "object" &&
                      e.response.data.detail
                    ) {
                      throw new Error(
                        `SIOP JWT request failed: ${e.response.data.detail}`,
                      );
                    }

                    throw new Error(
                      `SIOP JWT request failed: ${
                        e instanceof Error ? e.message : "unknown error"
                      }`,
                    );
                  }
                },
              }
            : {
                title:
                  'Requesting access token with "openid tsr_write" scope...',
                task: async (ctx, task) => {
                  // Get "openid tsr_write" access token
                  try {
                    const accessToken = await getAccessToken(
                      CONFIG[env][apisVersion].AUTH,
                      {
                        did: kid.split("#")[0],
                        kid,
                        alg: "ES256",
                        signer: ES256Signer(hexToBytes(privateKey)),
                      },
                      CONFIG[env][apisVersion].ebsiEnvConfig,
                    );

                    ctx.accessToken = accessToken;
                    task.title = "Access token successfully requested";
                  } catch (e) {
                    if (process.env.DEBUG === "true") {
                      console.error(e);
                    }

                    // Handle Problem Details errors
                    if (
                      axios.isAxiosError(e) &&
                      e.response?.data &&
                      typeof e.response.data === "object" &&
                      e.response.data.detail
                    ) {
                      throw new Error(
                        `Access token request failed: ${e.response.data.detail}`,
                      );
                    }

                    throw new Error(
                      `Access token request failed: ${
                        e instanceof Error ? e.message : "unknown error"
                      }`,
                    );
                  }
                },
              },
          {
            title: `Publishing schemas to ${env} environment...`,
            options: { collapse: false },
            task: async (ctx, task) =>
              task.newListr(
                [
                  ...ctx.schemasToPublish.map(
                    ({ id, schema, updatedSchema }) => ({
                      title: `Schema ${schema}`,
                      task: async (subctx, subtask) =>
                        task.newListr(() => [
                          {
                            title: "Preparing...",
                            task: async (_, subsubtask) => {
                              try {
                                const res = await publishSchema({
                                  schemaId: id,
                                  payload: updatedSchema,
                                  tsr: CONFIG[env][apisVersion].TSR,
                                  ledger: CONFIG[env][apisVersion].LEDGER,
                                  privateKey,
                                  accessToken: ctx.accessToken,
                                  task: subsubtask,
                                });

                                const url = `${CONFIG[env][apisVersion].TSR}/schemas/${id}`;
                                if (res === "published") {
                                  subtask.title = `Schema ${schema} published successfully: ${url}`;
                                } else if (res === "exists_already") {
                                  subtask.title = `Schema ${schema} already exists: ${url}`;
                                } else if (res === "updated") {
                                  subtask.title = `Schema ${schema} updated successfully: ${url}`;
                                }
                              } catch (e) {
                                if (process.env.DEBUG === "true") {
                                  console.error(e);
                                }

                                // Handle Problem Details errors
                                if (
                                  axios.isAxiosError(e) &&
                                  e.response?.data &&
                                  typeof e.response.data === "object" &&
                                  e.response.data.detail
                                ) {
                                  throw new Error(
                                    `Schema ${schema} was not published: ${e.response.data.detail}`,
                                  );
                                }

                                throw new Error(
                                  `Schema ${schema} was not published: ${
                                    e instanceof Error
                                      ? e.message
                                      : "unknown error"
                                  }`,
                                );
                              }
                            },
                          },
                        ]),
                    }),
                  ),
                  {
                    task: () => {
                      task.title = "Finished!";
                    },
                  },
                ],
                { exitOnError: false },
              ),
          },
        ],
        {
          concurrent: false,
          rendererOptions: {
            formatOutput: "wrap",
          },
        },
      );

      try {
        await tasks.run();
      } catch (e) {
        process.exit(1);
      }
    });
