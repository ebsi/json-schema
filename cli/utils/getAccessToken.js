import { randomUUID } from "node:crypto";
import { URLSearchParams } from "node:url";
import axios from "axios";
// Don't know why exactly, but ESLint fails to resolve this import
// eslint-disable-next-line import/no-unresolved
import { createVerifiablePresentationJwt } from "@cef-ebsi/verifiable-presentation";

/**
 * Get an actual "tsr_write" access token from Authorisation API.
 */
export async function getAccessToken(
  authorisationApiUrl,
  subject,
  ebsiEnvConfig,
) {
  const nonce = randomUUID();
  const vpPayload = {
    "@context": ["https://www.w3.org/2018/credentials/v1"],
    type: ["VerifiablePresentation"],
    verifiableCredential: [],
    holder: subject.did,
  };

  const vpJwt = await createVerifiablePresentationJwt(
    vpPayload,
    subject,
    authorisationApiUrl,
    {
      ...ebsiEnvConfig,
      skipValidation: true,
      nonce,
      // Manually add "exp" and "nbf" to the VP JWT because there's no VC to extract from
      exp: Math.floor(Date.now() / 1000) + 100,
      nbf: Math.floor(Date.now() / 1000) - 100,
    },
  );

  const presentationSubmission = {
    id: randomUUID(),
    definition_id: "tsr_write_presentation",
    descriptor_map: [],
  };

  try {
    const response = await axios.post(
      `${authorisationApiUrl}/token`,
      new URLSearchParams({
        grant_type: "vp_token",
        scope: "openid tsr_write",
        vp_token: vpJwt,
        presentation_submission: JSON.stringify(presentationSubmission),
      }).toString(),
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      },
    );

    // Decode access token
    const { access_token: accessToken } = response.data;

    return accessToken;
  } catch (e) {
    if (axios.isAxiosError(e)) {
      console.error(e.response?.data);
    } else {
      console.error(e);
    }
    throw new Error("Failed to get access token");
  }
}

export default getAccessToken;
