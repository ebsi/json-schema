import { dirname, resolve } from "node:path";
import { fileURLToPath } from "node:url";
import crypto from "node:crypto";
import { readFileSync } from "node:fs";
import $RefParser from "@apidevtools/json-schema-ref-parser";
import canonicalize from "canonicalize";
import { globbySync } from "globby";
import inquirer from "inquirer";
import Ajv2020 from "ajv/dist/2020.js";
import addFormats from "ajv-formats";
// eslint-disable-next-line import/extensions
import { base16 } from "multiformats/bases/base16";
// eslint-disable-next-line import/extensions
import { base58btc } from "multiformats/bases/base58";

const customAjvOptions = {
  "schemas/vcdm2.0/ehic/schema.json": {
    // See https://github.com/ajv-validator/ajv/issues/1571
    strictRequired: false,
    // See https://github.com/ajv-validator/ajv/issues/1898#issuecomment-1043618403
    strictTypes: false,
  },
};

export function removeAnnotations(obj) {
  /**
   * Lists of annotations keywords: https://json-schema.org/draft/2020-12/json-schema-validation.html#rfc.section.9
   * In addition to the annotations keywords, we also ignore comments: https://json-schema.org/understanding-json-schema/reference/comments
   */
  const keysToRemove = [
    "title",
    "description",
    "default",
    "deprecated",
    "readOnly",
    "writeOnly",
    "examples",
    "$comment",
  ];

  return JSON.parse(
    JSON.stringify(obj, (key, val) =>
      keysToRemove.includes(key) ? undefined : val,
    ),
  );
}

export function replaceRefs(obj, tsr, schemaPath, schemaIds) {
  const currentDir = dirname(fileURLToPath(import.meta.url));
  const rootDir = resolve(currentDir, "../..");

  const dependencies = [];
  const updatedSchema = JSON.parse(
    JSON.stringify(obj, (key, val) => {
      if (
        key !== "$ref" ||
        typeof val !== "string" ||
        !val.startsWith("./node_modules/")
      ) {
        return val;
      }

      // Relative path (from schemaPath to absolute path)
      const [referencedPath, fragment] = resolve(schemaPath, "..", val).split(
        "#",
      );
      const match = schemaIds.find(({ schema }) => {
        if (resolve(rootDir, schema) === referencedPath) {
          // Add to dependencies array
          if (!dependencies.includes(schema)) {
            dependencies.push(schema);
          }

          return true;
        }
        return false;
      });

      if (!match || !match.id) {
        throw new Error(`Unable to find ${referencedPath}`);
      }

      return `${tsr}/schemas/${match.id}${fragment ? `#${fragment}` : ""}`;
    }),
  );

  return {
    updatedSchema,
    dependencies,
  };
}

export async function computeId(schemaPath) {
  const fullPath = resolve(process.cwd(), schemaPath);

  // 1. Bundle schema
  const bundledSchema = await $RefParser.bundle(fullPath);

  // 2. Remove annotations
  const sanitizedDocument = removeAnnotations(bundledSchema);

  // 3. Canonicalise
  const canonicalizedDocument = canonicalize(sanitizedDocument);

  // 4. Compute sha256 of the stringified JSON document
  const hash = crypto
    .createHash("sha256")
    .update(JSON.stringify(canonicalizedDocument), "utf-8")
    .digest();

  // 5. Return multibase base58 (BTC) encoded hash
  return {
    base16: `0x${base16.baseEncode(hash)}`,
    multibase_base58btc: base58btc.encode(hash),
  };
}

const currentDir = dirname(fileURLToPath(import.meta.url));
const rootDir = resolve(currentDir, "../..");

export async function getSchemaIds(paths, options) {
  const SCHEMAS_PATTERNS = ["schemas/**/schema.json"];

  let schemas = [];
  if (Array.isArray(paths) && paths.length > 0) {
    schemas = globbySync(paths, { cwd: rootDir });
  } else if (options.all) {
    // Find all schemas
    schemas = globbySync(SCHEMAS_PATTERNS, { cwd: rootDir });
  } else {
    const allSchemas = globbySync(SCHEMAS_PATTERNS, {
      cwd: rootDir,
    });
    const answers = await inquirer.prompt([
      {
        type: "checkbox",
        message: "Which JSON Schema(s) do you want to compute?",
        name: "schemas",
        choices: allSchemas.map((schema) => ({
          name: schema,
        })),
        pageSize: 12,
        validate(answer) {
          if (answer.length < 1) {
            return "You must choose at least one JSON schema.";
          }

          return true;
        },
      },
    ]);
    schemas = answers.schemas;
  }

  if (!(schemas.length > 0)) {
    console.error("No JSON Schema found!");
    process.exit(1);
  }

  return Promise.all(
    schemas.map(async (schema) => {
      const { base16: id } = await computeId(schema);
      return { id, schema };
    }),
  );
}

export async function validateSchema(
  schema,
  trustedSchemaRegistry,
  allSchemaIds,
) {
  //  Validate JSON schema before publishing
  const bundledSchema = await $RefParser.bundle(resolve(rootDir, schema));

  try {
    let ajv;
    switch (bundledSchema.$schema) {
      // https://github.com/json-schema-org/json-schema-spec/blob/2020-12/schema.json
      case "https://json-schema.org/draft/2020-12/schema": {
        ajv = new Ajv2020({
          allErrors: true,
          strict: true,
          ...(customAjvOptions[schema] ?? {}),
        });
        break;
      }
      default: {
        throw new Error(`Unknown version "${bundledSchema.$schema}"`);
      }
    }
    addFormats(ajv);

    ajv.compile(bundledSchema);

    // If the schema is valid, replace $refs and push to schemasToPublish
    const originalSchemaContent = JSON.parse(
      readFileSync(resolve(rootDir, schema)),
    );

    const { updatedSchema, dependencies } = replaceRefs(
      originalSchemaContent,
      trustedSchemaRegistry,
      schema,
      allSchemaIds,
    );

    let deps = [];
    if (dependencies.length > 0) {
      // Validate dependencies
      deps = await dependencies.reduce(async (promiseChain, dependency) => {
        const validDeps = await promiseChain;
        const newDeps = await validateSchema(
          dependency,
          trustedSchemaRegistry,
          allSchemaIds,
        );
        return [...validDeps, ...newDeps];
      }, Promise.resolve([]));
    }

    const { base16: id } = await computeId(schema);

    return [
      ...deps,
      {
        id,
        schema,
        updatedSchema,
      },
    ];
  } catch (e) {
    throw new Error(
      `Invalid schema ${schema}. Error: ${
        e instanceof Error ? e.message : "unknown error"
      }`,
    );
  }
}
