export default {
  test: {
    // APIs to be used with TSR API v2 on testnet
    v2: {
      AUTH: "https://api-test.testnode02.ebsi.eu/authorisation/v2",
      LEDGER: "https://api-test.ebsi.eu/ledger/v3",
      TSR: "https://api-test.ebsi.eu/trusted-schemas-registry/v2",
      TAR: "https://api-test.ebsi.eu/trusted-apps-registry/v3",
    },
    // APIs to be used with TSR API v3 on testnet
    v3: {
      AUTH: "https://api-test.ebsi.eu/authorisation/v4",
      LEDGER: "https://api-test.ebsi.eu/ledger/v4",
      TSR: "https://api-test.ebsi.eu/trusted-schemas-registry/v3",
      ebsiEnvConfig: {
        network: "test",
        hosts: ["api-test.ebsi.eu"],
        services: {
          "did-registry": "v5",
          "trusted-issuers-registry": "v5",
          "trusted-policies-registry": "v3",
          "trusted-schemas-registry": "v3",
        },
      },
    },
    // APIs to be used with TSR API v4 on testnet
    v4: {
      AUTH: "https://api-test.ebsi.eu/authorisation/v5",
      LEDGER: "https://api-test.ebsi.eu/ledger/v4",
      TSR: "https://api-test.ebsi.eu/trusted-schemas-registry/v4",
      ebsiEnvConfig: {
        network: "test",
        hosts: ["api-test.ebsi.eu"],
        services: {
          "did-registry": "v6",
          "trusted-issuers-registry": "v6",
          "trusted-policies-registry": "v4",
          "trusted-schemas-registry": "v4",
        },
      },
    },
  },
  conformance: {
    // APIs to be used with TSR API v2 on Conformance
    v2: {
      AUTH: "https://api-conformance.ebsi.eu/authorisation/v2",
      LEDGER: "https://api-conformance.ebsi.eu/ledger/v3",
      TSR: "https://api-conformance.ebsi.eu/trusted-schemas-registry/v2",
      TAR: "https://api-conformance.ebsi.eu/trusted-apps-registry/v3",
    },
    // APIs to be used with TSR API v3 on Conformance
    v3: {
      AUTH: "https://api-conformance.ebsi.eu/authorisation/v4",
      LEDGER: "https://api-conformance.ebsi.eu/ledger/v3",
      TSR: "https://api-conformance.ebsi.eu/trusted-schemas-registry/v3",
      ebsiEnvConfig: {
        network: "conformance",
        hosts: ["api-conformance.ebsi.eu"],
        services: {
          "did-registry": "v5",
          "trusted-issuers-registry": "v5",
          "trusted-policies-registry": "v3",
          "trusted-schemas-registry": "v3",
        },
      },
    },
  },
  pilot: {
    // APIs to be used with TSR API v2 on Pilot
    v2: {
      AUTH: "https://api-pilot.ebsi.eu/authorisation/v2",
      LEDGER: "https://api-pilot.ebsi.eu/ledger/v3",
      TSR: "https://api-pilot.ebsi.eu/trusted-schemas-registry/v2",
      TAR: "https://api-pilot.ebsi.eu/trusted-apps-registry/v3",
    },
    // APIs to be used with TSR API v3 on Pilot
    v3: {
      AUTH: "https://api-pilot.ebsi.eu/authorisation/v4",
      LEDGER: "https://api-pilot.ebsi.eu/ledger/v3",
      TSR: "https://api-pilot.ebsi.eu/trusted-schemas-registry/v3",
      ebsiEnvConfig: {
        network: "pilot",
        hosts: ["api-pilot.ebsi.eu"],
        services: {
          "did-registry": "v5",
          "trusted-issuers-registry": "v5",
          "trusted-policies-registry": "v3",
          "trusted-schemas-registry": "v3",
        },
      },
    },
  },
};
