![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-e-origin-verifiable-business-card-schema

> Verifiable Business Card
>
> A digital, verifiable business card, issued as a Verifiable Credential. The Verifiable Business Card contains information about how to reach the subject, including submitting Verifiable Presentations.

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x70ab4378811f5c49b9ce236e564fd6f75f6e2c2732d63277013329fc3a4a79e9` (hexadecimal)
- `z8ap8TJQ2dgt1qKmifBbQQ2o75vfuQJBsVMWxMLe6xaVe` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Verifiable Business Card",
  "description": "A digital, verifiable business card, issued as a Verifiable Credential. The Verifiable Business Card contains information about how to reach the subject, including submitting Verifiable Presentations.",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "A signed business card meant to represent a person working for an organization.",
          "properties": {
            "type": {
              "type": "array",
              "readOnly": true,
              "const": ["Person"],
              "default": ["Person"],
              "items": {
                "type": "string",
                "enum": ["Person"]
              }
            },
            "firstName": {
              "title": "First Name",
              "description": "Person's first name",
              "type": "string"
            },
            "lastName": {
              "title": "Last Name",
              "description": "Person's last name",
              "type": "string"
            },
            "email": {
              "title": "Person's Email Address",
              "description": "Person's email address",
              "type": "string",
              "format": "email"
            },
            "wechatId": {
              "title": "ID WeChat",
              "description": "Person's WeChat identification",
              "type": "string"
            },
            "phoneNumber": {
              "title": "Phone Number",
              "description": "Person's contact phone number",
              "type": "string"
            },
            "worksFor": {
              "title": "Works For",
              "description": "Company or organization which employs the person",
              "type": "object",
              "properties": {
                "type": {
                  "type": "array",
                  "readOnly": true,
                  "const": ["Organization"],
                  "default": ["Organization"],
                  "items": {
                    "type": "string",
                    "enum": ["Organization"]
                  }
                },
                "role": {
                  "description": "Organization role.",
                  "type": "array",
                  "readOnly": true,
                  "items": {
                    "type": "string",
                    "enum": [
                      "Exporter",
                      "Consignor",
                      "Consignee",
                      "Importer",
                      "Forwarder",
                      "Carrier",
                      "Seller",
                      "Buyer",
                      "Declarant",
                      "Representative"
                    ]
                  }
                },
                "id": {
                  "title": "Identifier",
                  "description": "Organization identifier.",
                  "type": "string"
                },
                "name": {
                  "title": "Name",
                  "description": "Name of the organization.",
                  "type": "string"
                },
                "url": {
                  "title": "URL",
                  "description": "URL of the organization.",
                  "type": "string",
                  "format": "uri"
                },
                "description": {
                  "title": "Description",
                  "description": "Description of the company.",
                  "type": "string"
                },
                "location": {
                  "title": "Location",
                  "description": "The location of, for example, where an event is happening, where an organization is located, or where an action takes place.",
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "array",
                      "readOnly": true,
                      "const": ["Place"],
                      "default": ["Place"],
                      "items": {
                        "type": "string",
                        "enum": ["Place"]
                      }
                    },
                    "address": {
                      "title": "Postal Address",
                      "description": "The postal address for an organization or place.",
                      "type": "object",
                      "properties": {
                        "type": {
                          "type": "array",
                          "readOnly": true,
                          "const": ["PostalAddress"],
                          "default": ["PostalAddress"],
                          "items": {
                            "type": "string",
                            "enum": ["PostalAddress"]
                          }
                        },
                        "name": {
                          "title": "Name",
                          "description": "The name of the entity in text.",
                          "type": "string"
                        },
                        "streetAddress": {
                          "title": "Street Address",
                          "description": "The street address expressed as free form text. The street address is printed on paper as the first lines below the name. For example, the name of the street and the number in the street or the name of a building.",
                          "type": "string"
                        },
                        "addressLocality": {
                          "title": "Address Locality",
                          "description": "Text specifying the name of the locality; for example, a city.",
                          "type": "string"
                        },
                        "addressRegion": {
                          "title": "Address Region",
                          "description": "Text specifying a province or state in abbreviated format; for example, NJ.",
                          "type": "string"
                        },
                        "addressCountry": {
                          "title": "Address Country",
                          "description": "The two-letter ISO 3166-1 alpha-2 country code.",
                          "type": "string"
                        },
                        "crossStreet": {
                          "title": "Cross Street",
                          "description": "A street intersecting a main street (usually at right angles) and continuing on both sides of it.",
                          "type": "string"
                        },
                        "postalCode": {
                          "title": "Postal Code",
                          "description": "Text specifying the postal code for an address.",
                          "type": "string"
                        }
                      },
                      "additionalProperties": false,
                      "required": ["type"]
                    }
                  },
                  "additionalProperties": false,
                  "required": ["type", "address"]
                },
                "email": {
                  "title": "Email Address",
                  "description": "Organization's primary email address.",
                  "type": "string",
                  "format": "email"
                },
                "phoneNumber": {
                  "title": "Phone Number",
                  "description": "Organization's contact phone number.",
                  "type": "string"
                },
                "logo": {
                  "title": "Logo",
                  "description": "logo for this organization",
                  "type": "string",
                  "format": "uri",
                  "maxLength": 512000
                }
              },
              "additionalProperties": false,
              "required": ["type", "role", "id", "name", "url", "location"]
            },
            "jobTitle": {
              "title": "Job Title",
              "description": "Person's job title.",
              "type": "string"
            }
          },
          "required": [
            "type",
            "firstName",
            "lastName",
            "email",
            "worksFor",
            "jobTitle"
          ],
          "title": "Business card",
          "type": "object"
        },
        "proof": {
          "description": "A JSON Web Signature proof for a credential as defined by the VC data model",
          "properties": {
            "created": {
              "description": "Creation timestamp for the proof in the form of an XML datestring",
              "type": "string"
            },
            "jws": {
              "description": "The JSON Web Signature for the proof",
              "type": "string"
            },
            "proofPurpose": {
              "const": "assertionMethod",
              "description": "In the case of credentials, the proof should be the constant, 'assertionMethod'"
            },
            "type": {
              "description": "Signature suite for the proof",
              "enum": ["Ed25519Signature2018"],
              "type": "string"
            },
            "verificationMethod": {
              "description": "The fragment from which the public key can be de-referenced, in the form of a URI",
              "type": "string"
            }
          },
          "title": "proof",
          "type": "object"
        },
        "type": {
          "type": "array",
          "readOnly": true,
          "const": [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableBusinessCard"
          ],
          "default": [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableBusinessCard"
          ],
          "items": {
            "type": "string",
            "enum": [
              "VerifiableCredential",
              "VerifiableAttestation",
              "VerifiableBusinessCard"
            ]
          }
        }
      },
      "title": "Verifiable Business Card",
      "type": "object"
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-e-origin-verifiable-business-card-schema@1.0.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-e-origin-verifiable-business-card-schema@1.0.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-e-origin-verifiable-business-card-schema@1.0.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-e-origin-verifiable-business-card-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { VerifiableBusinessCard } from "@cef-ebsi/vcdm1.1-e-origin-verifiable-business-card-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
