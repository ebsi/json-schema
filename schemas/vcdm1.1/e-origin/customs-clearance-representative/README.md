![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-e-origin-customs-clearance-representative-schema

> Customs Clearance Representative
>
> Specialized Power of Attorney (PoA) schema for customs clearance in goods importation, tailored for corporate PoA use.

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x5337b2726b3d0b5e32809302cc27f604899bc1c98a1706d07bd3dcbfd0e564e3` (hexadecimal)
- `z6br6faZV1as1cm65p7VK64yKEM63c5utLYJC4WRPB7wY` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Customs Clearance Representative",
  "description": "Specialized Power of Attorney (PoA) schema for customs clearance in goods importation, tailored for corporate PoA use.",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Power of Attorney credential.",
          "properties": {
            "startingDate": {
              "title": "Starting Date",
              "description": "The POA mandate takes effect from this date",
              "type": "string",
              "format": "date"
            },
            "expirationDate": {
              "title": "Expiration Date",
              "description": "Date of expiration of the POA",
              "type": "string",
              "format": "date"
            },
            "customsRepresentative": {
              "title": "Customs Representative",
              "description": "Customs representative information.",
              "type": "object",
              "properties": {
                "companyRegistrationNumber": {
                  "title": "Company RegistrationNumber",
                  "description": "Company registration Number .",
                  "type": "string"
                },
                "vatNumber": {
                  "title": "VAT Number",
                  "description": "Company vat Number .",
                  "type": "string"
                },
                "customsRegistrationNumber": {
                  "title": "customs RegistrationNumber",
                  "description": "Customs Registration Number of the customs representative.",
                  "type": "string"
                },
                "address": {
                  "title": "Address of the custosm representative",
                  "description": "(Optional) The address of the customs representative.",
                  "type": "object",
                  "properties": {
                    "street": {
                      "title": "Street",
                      "description": "(Optional) Street address",
                      "type": "string"
                    },
                    "streetNr": {
                      "title": "Street Number",
                      "description": "(Optional) Street number",
                      "type": "integer"
                    },
                    "zipCode": {
                      "title": "Zip Code",
                      "description": "(Optional) Postal code",
                      "type": "string"
                    },
                    "city": {
                      "title": "City",
                      "description": "(Optional) City name",
                      "type": "string"
                    },
                    "country": {
                      "title": "Country",
                      "description": "(Optional) Country name",
                      "type": "string"
                    }
                  }
                }
              },
              "required": [
                "companyRegistrationNumber",
                "customsRegistrationNumber"
              ]
            },
            "relatedResource": {
              "title": "Related Resource",
              "description": "The issuer provides the verifier with additional details.",
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "id": {
                    "title": "Resource ID",
                    "description": "The identifier for the resource. Unique URL.",
                    "type": "string"
                  },
                  "fileName": {
                    "title": "File Name",
                    "description": "(Optional) file name",
                    "type": "string"
                  },
                  "mediaType": {
                    "title": "Media Type",
                    "description": "A valid media type as listed in the IANA Media Types registry.",
                    "type": "string"
                  },
                  "digestMultibase": {
                    "title": "Digest Multibase",
                    "description": "One or more cryptographic digests, as defined in the Multibase specification.",
                    "type": "string"
                  }
                },
                "required": ["id", "fileName", "mediaType", "digestMultibase"]
              }
            },
            "idCard": {
              "description": "Government-issued ID for the importer or business representative",
              "type": "object",
              "properties": {
                "ownerName": {
                  "title": "Importer or business representative name",
                  "description": "The legal representative of the company.",
                  "type": "string"
                },
                "expirationDate": {
                  "title": "Expiration Date",
                  "description": "Date of expiration of the id card",
                  "type": "string",
                  "format": "date"
                },
                "title": {
                  "title": "title",
                  "description": "Title/ Function of the person in the company",
                  "type": "string"
                },
                "registrationNumber": {
                  "title": "Registration Number",
                  "description": "Registration number of the ID card",
                  "type": "string"
                },
                "email": {
                  "title": "Email",
                  "description": "(Optional) contact email",
                  "type": "string"
                },
                "phoneNumber": {
                  "title": "Phone Number",
                  "description": "(Optional) contact phone number",
                  "type": "string"
                }
              },
              "required": ["ownerName", "expirationDate", "title"]
            },
            "businessLicense": {
              "description": "The business license of the importer",
              "type": "object",
              "properties": {
                "nameEN": {
                  "title": "Name in English",
                  "description": "The legal name of the importer in English (individual or business entity)",
                  "type": "string"
                },
                "nameCN": {
                  "title": "Name in Chinese",
                  "description": "The legal name of the importer in Chinese (individual or business entity)",
                  "type": "string"
                },
                "registrationNumber": {
                  "title": "Registration Number",
                  "description": "A number assigned to the business by the government.",
                  "type": "string"
                },
                "bankAccount": {
                  "title": "Bank Account",
                  "description": "(Optional) Necessary for financial transactions.",
                  "type": "string"
                },
                "address": {
                  "title": "Address",
                  "description": "(Optional) The physical location of the importer.",
                  "type": "object",
                  "properties": {
                    "street": {
                      "title": "Street",
                      "description": "(Optional) Street address",
                      "type": "string"
                    },
                    "streetNr": {
                      "title": "Street Number",
                      "description": "(Optional) Street number",
                      "type": "integer"
                    },
                    "zipCode": {
                      "title": "Zip Code",
                      "description": "(Optional) Postal code",
                      "type": "string"
                    },
                    "city": {
                      "title": "City",
                      "description": "(Optional) City name",
                      "type": "string"
                    },
                    "country": {
                      "title": "Country",
                      "description": "(Optional) Country name",
                      "type": "string"
                    }
                  }
                }
              },
              "required": ["nameEN", "registrationNumber", "address"]
            },
            "eori": {
              "description": "Economic Operators Registration and Identification number.",
              "type": "object",
              "properties": {
                "number": {
                  "title": "EORI Number",
                  "description": "Economic Operators Registration and Identification number.",
                  "type": "string"
                },
                "address": {
                  "title": "Address",
                  "description": "(Optional) The physical location of the importer.",
                  "type": "object",
                  "properties": {
                    "street": {
                      "title": "Street",
                      "description": "(Optional) Street address",
                      "type": "string"
                    },
                    "streetNr": {
                      "title": "Street Number",
                      "description": "(Optional) Street number",
                      "type": "integer"
                    },
                    "zipCode": {
                      "title": "Zip Code",
                      "description": "(Optional) Postal code",
                      "type": "string"
                    },
                    "city": {
                      "title": "City",
                      "description": "(Optional) City name",
                      "type": "string"
                    },
                    "country": {
                      "title": "Country",
                      "description": "(Optional) Country name",
                      "type": "string"
                    }
                  }
                }
              },
              "required": ["number", "address"]
            }
          },
          "required": [
            "startingDate",
            "customsRepresentative",
            "relatedResource",
            "idCard",
            "businessLicense",
            "eori"
          ],
          "title": "Importer",
          "type": "object"
        },
        "proof": {
          "description": "A JSON Web Signature proof for a credential as defined by the VC data model",
          "properties": {
            "created": {
              "description": "Creation timestamp for the proof in the form of an XML datestring",
              "type": "string"
            },
            "jws": {
              "description": "The JSON Web Signature for the proof",
              "type": "string"
            },
            "proofPurpose": {
              "const": "assertionMethod",
              "description": "In the case of credentials, the proof should be the constant, 'assertionMethod'"
            },
            "type": {
              "description": "Signature suite for the proof",
              "enum": ["Ed25519Signature2018"],
              "type": "string"
            },
            "verificationMethod": {
              "description": "The fragment from which the public key can be de-referenced, in the form of a URI",
              "type": "string"
            }
          },
          "title": "proof",
          "type": "object"
        }
      },
      "title": "Power of Attorney",
      "type": "object"
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-e-origin-customs-clearance-representative-schema@2.0.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-e-origin-customs-clearance-representative-schema@2.0.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-e-origin-customs-clearance-representative-schema@2.0.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-e-origin-customs-clearance-representative-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { CustomsClearanceRepresentative } from "@cef-ebsi/vcdm1.1-e-origin-customs-clearance-representative-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
