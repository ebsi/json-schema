# @cef-ebsi/vcdm1.1-e-origin-customs-clearance-representative-schema

## 2.0.1

### Patch Changes

- c7f7fa3: Update license text.
- Updated dependencies [c7f7fa3]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.3.1

## 2.0.0

### Major Changes

- ea23278: Rename package from `@cef-ebsi/vcdm1.1-e-origin-power-of-attorney-schema` to `@cef-ebsi/vcdm1.1-e-origin-customs-clearance-representative-schema`.
  Update schema title and description.

## 1.0.0

### Major Changes

- afac7fa: Create initial version of the schema.
