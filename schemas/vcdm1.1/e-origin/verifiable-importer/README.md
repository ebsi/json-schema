![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-e-origin-verifiable-importer-schema

> Importer
>
> A credential used to provide information of an importer in order to build a power of attorney (POA) typically includes various pieces of specific information that authenticate and authorize the importer in legal and business contexts.

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xefb5212ed9634ab93c75a8a4ef378fcf5c558519784d30d733d2663220031076` (hexadecimal)
- `zH8ide7AagqpGYhzzLSy2swcsNjSaneczYvNbcRZN8gzh` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Importer",
  "description": "A credential used to provide information of an importer in order to build a power of attorney (POA) typically includes various pieces of specific information that authenticate and authorize the importer in legal and business contexts.",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Importer data credential.",
          "properties": {
            "name": {
              "title": "Name of the importer",
              "description": "The legal name of the importer (individual or business entity)",
              "type": "string"
            },
            "idCard": {
              "description": "Government-issued ID for the importer or business representative",
              "type": "object",
              "properties": {
                "ownerName": {
                  "title": "Importer or business representative name",
                  "description": "The legal representative of the company.",
                  "type": "string"
                },
                "expirationDate": {
                  "title": "Expiration Date",
                  "description": "Date of expiration of the id card",
                  "type": "string",
                  "format": "date"
                },
                "title": {
                  "title": "title",
                  "description": "Title/ Function of the person in the company",
                  "type": "string"
                },
                "registrationNumber": {
                  "title": "Registration Number",
                  "description": "Registration number of the ID card",
                  "type": "string"
                },
                "email": {
                  "title": "Email",
                  "description": "(Optional) contact email",
                  "type": "string"
                },
                "phoneNumber": {
                  "title": "Phone Number",
                  "description": "(Optional) contact phone number",
                  "type": "string"
                },
                "relatedResource": {
                  "title": "Related Resource",
                  "description": "The issuer provides the verifier with additional details.",
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "title": "Resource ID",
                        "description": "The identifier for the resource. Unique URL.",
                        "type": "string"
                      },
                      "fileName": {
                        "title": "File Name",
                        "description": "(Optional) file name",
                        "type": "string"
                      },
                      "mediaType": {
                        "title": "Media Type",
                        "description": "A valid media type as listed in the IANA Media Types registry.",
                        "type": "string"
                      },
                      "digestMultibase": {
                        "title": "Digest Multibase",
                        "description": "One or more cryptographic digests, as defined in the Multibase specification.",
                        "type": "string"
                      }
                    },
                    "required": [
                      "id",
                      "fileName",
                      "mediaType",
                      "digestMultibase"
                    ]
                  }
                }
              },
              "required": ["ownerName", "expirationDate", "title"]
            },
            "businessLicense": {
              "description": "The business license of the importer",
              "type": "object",
              "properties": {
                "nameEN": {
                  "title": "Name in English",
                  "description": "The legal name of the importer in English (individual or business entity)",
                  "type": "string"
                },
                "nameCN": {
                  "title": "Name in Chinese",
                  "description": "The legal name of the importer in Chinese (individual or business entity)",
                  "type": "string"
                },
                "registrationNumber": {
                  "title": "Registration Number",
                  "description": "A number assigned to the business by the government.",
                  "type": "string"
                },
                "bankAccount": {
                  "title": "Bank Account",
                  "description": "(Optional) Necessary for financial transactions.",
                  "type": "string"
                },
                "address": {
                  "title": "Address",
                  "description": "(Optional) The physical location of the importer.",
                  "type": "object",
                  "properties": {
                    "street": {
                      "title": "Street",
                      "description": "(Optional) Street address",
                      "type": "string"
                    },
                    "streetNr": {
                      "title": "Street Number",
                      "description": "(Optional) Street number",
                      "type": "integer"
                    },
                    "zipCode": {
                      "title": "Zip Code",
                      "description": "(Optional) Postal code",
                      "type": "string"
                    },
                    "city": {
                      "title": "City",
                      "description": "(Optional) City name",
                      "type": "string"
                    },
                    "country": {
                      "title": "Country",
                      "description": "(Optional) Country name",
                      "type": "string"
                    }
                  }
                },
                "relatedResource": {
                  "title": "Related Resource",
                  "description": "The issuer provides the verifier with additional details.",
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "title": "Resource ID",
                        "description": "The identifier for the resource. Unique URL.",
                        "type": "string"
                      },
                      "fileName": {
                        "title": "File Name",
                        "description": "(Optional) file name",
                        "type": "string"
                      },
                      "mediaType": {
                        "title": "Media Type",
                        "description": "A valid media type as listed in the IANA Media Types registry.",
                        "type": "string"
                      },
                      "digestMultibase": {
                        "title": "Digest Multibase",
                        "description": "One or more cryptographic digests, as defined in the Multibase specification.",
                        "type": "string"
                      }
                    },
                    "required": [
                      "id",
                      "fileName",
                      "mediaType",
                      "digestMultibase"
                    ]
                  }
                }
              },
              "required": ["nameEN", "nameCN", "registrationNumber", "address"]
            },
            "eori": {
              "description": "Economic Operators Registration and Identification number.",
              "type": "object",
              "properties": {
                "number": {
                  "title": "EORI Number",
                  "description": "Economic Operators Registration and Identification number.",
                  "type": "string"
                },
                "address": {
                  "title": "Address",
                  "description": "(Optional) The physical location of the importer.",
                  "type": "object",
                  "properties": {
                    "street": {
                      "title": "Street",
                      "description": "(Optional) Street address",
                      "type": "string"
                    },
                    "streetNr": {
                      "title": "Street Number",
                      "description": "(Optional) Street number",
                      "type": "integer"
                    },
                    "zipCode": {
                      "title": "Zip Code",
                      "description": "(Optional) Postal code",
                      "type": "string"
                    },
                    "city": {
                      "title": "City",
                      "description": "(Optional) City name",
                      "type": "string"
                    },
                    "country": {
                      "title": "Country",
                      "description": "(Optional) Country name",
                      "type": "string"
                    }
                  }
                },
                "relatedResource": {
                  "title": "Related Resource",
                  "description": "The issuer provides the verifier with additional details.",
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "title": "Resource ID",
                        "description": "The identifier for the resource. Unique URL.",
                        "type": "string"
                      },
                      "fileName": {
                        "title": "File Name",
                        "description": "(Optional) file name",
                        "type": "string"
                      },
                      "mediaType": {
                        "title": "Media Type",
                        "description": "A valid media type as listed in the IANA Media Types registry.",
                        "type": "string"
                      },
                      "digestMultibase": {
                        "title": "Digest Multibase",
                        "description": "One or more cryptographic digests, as defined in the Multibase specification.",
                        "type": "string"
                      }
                    },
                    "required": [
                      "id",
                      "fileName",
                      "mediaType",
                      "digestMultibase"
                    ]
                  }
                }
              },
              "required": ["number", "address"]
            },
            "vat": {
              "description": "Value Added Tax number.",
              "type": "object",
              "properties": {
                "number": {
                  "title": "VAT Number",
                  "description": "Value Added Tax number.",
                  "type": "string"
                },
                "address": {
                  "title": "Address",
                  "description": "(Optional) The physical location of the importer.",
                  "type": "object",
                  "properties": {
                    "street": {
                      "title": "Street",
                      "description": "(Optional) Street address",
                      "type": "string"
                    },
                    "streetNr": {
                      "title": "Street Number",
                      "description": "(Optional) Street number",
                      "type": "integer"
                    },
                    "zipCode": {
                      "title": "Zip Code",
                      "description": "(Optional) Postal code",
                      "type": "string"
                    },
                    "city": {
                      "title": "City",
                      "description": "(Optional) City name",
                      "type": "string"
                    },
                    "country": {
                      "title": "Country",
                      "description": "(Optional) Country name",
                      "type": "string"
                    }
                  }
                },
                "relatedResource": {
                  "title": "Related Resource",
                  "description": "The issuer provides the verifier with additional details.",
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "title": "Resource ID",
                        "description": "The identifier for the resource. Unique URL.",
                        "type": "string"
                      },
                      "fileName": {
                        "title": "File Name",
                        "description": "(Optional) file name",
                        "type": "string"
                      },
                      "mediaType": {
                        "title": "Media Type",
                        "description": "A valid media type as listed in the IANA Media Types registry.",
                        "type": "string"
                      },
                      "digestMultibase": {
                        "title": "Digest Multibase",
                        "description": "One or more cryptographic digests, as defined in the Multibase specification.",
                        "type": "string"
                      }
                    },
                    "required": [
                      "id",
                      "fileName",
                      "mediaType",
                      "digestMultibase"
                    ]
                  }
                }
              },
              "required": ["number", "address"]
            },
            "taxReturn": {
              "description": "Previous Tax Return: Historical data of previous tax returns.",
              "type": "object",
              "properties": {
                "relatedResource": {
                  "title": "Related Resource",
                  "description": "The issuer provides the verifier with additional details.",
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "title": "Resource ID",
                        "description": "The identifier for the resource. Unique URL.",
                        "type": "string"
                      },
                      "fileName": {
                        "title": "File Name",
                        "description": "(Optional) file name",
                        "type": "string"
                      },
                      "mediaType": {
                        "title": "Media Type",
                        "description": "A valid media type as listed in the IANA Media Types registry.",
                        "type": "string"
                      },
                      "digestMultibase": {
                        "title": "Digest Multibase",
                        "description": "One or more cryptographic digests, as defined in the Multibase specification.",
                        "type": "string"
                      }
                    },
                    "required": [
                      "id",
                      "fileName",
                      "mediaType",
                      "digestMultibase"
                    ]
                  }
                }
              }
            },
            "importRecords": {
              "description": "Previous Import Records: Historical data of previous import records.",
              "type": "object",
              "properties": {
                "relatedResource": {
                  "title": "Related Resource",
                  "description": "The issuer provides the verifier with additional details.",
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "title": "Resource ID",
                        "description": "The identifier for the resource. Unique URL.",
                        "type": "string"
                      },
                      "fileName": {
                        "title": "File Name",
                        "description": "(Optional) file name",
                        "type": "string"
                      },
                      "mediaType": {
                        "title": "Media Type",
                        "description": "A valid media type as listed in the IANA Media Types registry.",
                        "type": "string"
                      },
                      "digestMultibase": {
                        "title": "Digest Multibase",
                        "description": "One or more cryptographic digests, as defined in the Multibase specification.",
                        "type": "string"
                      }
                    },
                    "required": [
                      "id",
                      "fileName",
                      "mediaType",
                      "digestMultibase"
                    ]
                  }
                }
              }
            }
          },
          "required": ["name", "idCard", "businessLicense", "eori"],
          "title": "Importer",
          "type": "object"
        },
        "proof": {
          "description": "A JSON Web Signature proof for a credential as defined by the VC data model",
          "properties": {
            "created": {
              "description": "Creation timestamp for the proof in the form of an XML datestring",
              "type": "string"
            },
            "jws": {
              "description": "The JSON Web Signature for the proof",
              "type": "string"
            },
            "proofPurpose": {
              "const": "assertionMethod",
              "description": "In the case of credentials, the proof should be the constant, 'assertionMethod'"
            },
            "type": {
              "description": "Signature suite for the proof",
              "enum": ["Ed25519Signature2018"],
              "type": "string"
            },
            "verificationMethod": {
              "description": "The fragment from which the public key can be de-referenced, in the form of a URI",
              "type": "string"
            }
          },
          "title": "proof",
          "type": "object"
        }
      },
      "title": "Verifiable Importer",
      "type": "object"
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-e-origin-verifiable-importer-schema@1.0.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-e-origin-verifiable-importer-schema@1.0.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-e-origin-verifiable-importer-schema@1.0.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-e-origin-verifiable-importer-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { Importer } from "@cef-ebsi/vcdm1.1-e-origin-verifiable-importer-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
