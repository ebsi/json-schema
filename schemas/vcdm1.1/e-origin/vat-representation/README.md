![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-e-origin-vat-representation-schema

> VAT Representation agreement
>
> A credential used to issue a VAT Representation agreement.

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x4b3955b8811fa0a4444bdad73bd6ffc97472519910a83e8aaae7c6efb0fbaaae` (hexadecimal)
- `z64eHWGGwoh4VqymuFXy4vhJFxRSjHGXcrwKciZAjL52m` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "VAT Representation agreement",
  "description": "A credential used to issue a VAT Representation agreement.",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "VAT representation credential.",
          "properties": {
            "startingDate": {
              "title": "Starting Date",
              "description": "The VAT representation mandate takes effect from this date",
              "type": "string",
              "format": "date"
            },
            "expirationDate": {
              "title": "Expiration Date",
              "description": "Date of expiration of the VAT representation",
              "type": "string",
              "format": "date"
            },
            "vatRepresentative": {
              "description": "VAT representative information.",
              "type": "object",
              "properties": {
                "vatNumber": {
                  "title": "VAT Number",
                  "description": "VAT Registration number.",
                  "type": "string"
                },
                "address": {
                  "title": "Address",
                  "description": "(The VAT address of the importer.",
                  "type": "object",
                  "properties": {
                    "street": {
                      "title": "Street",
                      "description": "(Optional) Street address",
                      "type": "string"
                    },
                    "streetNr": {
                      "title": "Street Number",
                      "description": "(Optional) Street number",
                      "type": "integer"
                    },
                    "zipCode": {
                      "title": "Zip Code",
                      "description": "(Optional) Postal code",
                      "type": "string"
                    },
                    "city": {
                      "title": "City",
                      "description": "(Optional) City name",
                      "type": "string"
                    },
                    "country": {
                      "title": "Country",
                      "description": "(Optional) Country name",
                      "type": "string"
                    }
                  }
                }
              },
              "required": ["vatNumber", "address"]
            },
            "relatedResource": {
              "title": "Related Resource",
              "description": "The issuer provides the verifier with additional details.",
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "id": {
                    "title": "Resource ID",
                    "description": "The identifier for the resource. Unique URL.",
                    "type": "string"
                  },
                  "fileName": {
                    "title": "File Name",
                    "description": "(Optional) file name",
                    "type": "string"
                  },
                  "mediaType": {
                    "title": "Media Type",
                    "description": "A valid media type as listed in the IANA Media Types registry.",
                    "type": "string"
                  },
                  "digestMultibase": {
                    "title": "Digest Multibase",
                    "description": "One or more cryptographic digests, as defined in the Multibase specification.",
                    "type": "string"
                  }
                },
                "required": ["id", "fileName", "mediaType", "digestMultibase"]
              }
            },
            "idCard": {
              "description": "Government-issued ID for the importer or business representative",
              "type": "object",
              "properties": {
                "ownerName": {
                  "title": "Importer or business representative name",
                  "description": "The legal representative of the company.",
                  "type": "string"
                },
                "expirationDate": {
                  "title": "Expiration Date",
                  "description": "Date of expiration of the id card",
                  "type": "string",
                  "format": "date"
                },
                "title": {
                  "title": "title",
                  "description": "Title/ Function of the person in the company",
                  "type": "string"
                },
                "registrationNumber": {
                  "title": "Registration Number",
                  "description": "Registration number of the ID card",
                  "type": "string"
                },
                "email": {
                  "title": "Email",
                  "description": "(Optional) contact email",
                  "type": "string"
                },
                "phoneNumber": {
                  "title": "Phone Number",
                  "description": "(Optional) contact phone number",
                  "type": "string"
                }
              },
              "required": ["ownerName", "expirationDate", "title"]
            },
            "businessLicense": {
              "description": "The business license of the importer",
              "type": "object",
              "properties": {
                "nameEN": {
                  "title": "Name in English",
                  "description": "The legal name of the importer in English (individual or business entity)",
                  "type": "string"
                },
                "nameCN": {
                  "title": "Name in Chinese",
                  "description": "The legal name of the importer in Chinese (individual or business entity)",
                  "type": "string"
                },
                "registrationNumber": {
                  "title": "Registration Number",
                  "description": "A number assigned to the business by the government.",
                  "type": "string"
                },
                "bankAccount": {
                  "title": "Bank Account",
                  "description": "(Optional) Necessary for financial transactions.",
                  "type": "string"
                },
                "address": {
                  "title": "Address",
                  "description": "(Optional) The physical location of the importer.",
                  "type": "object",
                  "properties": {
                    "street": {
                      "title": "Street",
                      "description": "(Optional) Street address",
                      "type": "string"
                    },
                    "streetNr": {
                      "title": "Street Number",
                      "description": "(Optional) Street number",
                      "type": "integer"
                    },
                    "zipCode": {
                      "title": "Zip Code",
                      "description": "(Optional) Postal code",
                      "type": "string"
                    },
                    "city": {
                      "title": "City",
                      "description": "(Optional) City name",
                      "type": "string"
                    },
                    "country": {
                      "title": "Country",
                      "description": "(Optional) Country name",
                      "type": "string"
                    }
                  }
                }
              },
              "required": ["nameEN", "registrationNumber", "address"]
            },
            "vat": {
              "description": "Value Added Tax number.",
              "type": "object",
              "properties": {
                "number": {
                  "title": "VAT Number",
                  "description": "Value Added Tax number.",
                  "type": "string"
                },
                "address": {
                  "title": "Address",
                  "description": "(Optional) The physical location of the importer.",
                  "type": "object",
                  "properties": {
                    "street": {
                      "title": "Street",
                      "description": "(Optional) Street address",
                      "type": "string"
                    },
                    "streetNr": {
                      "title": "Street Number",
                      "description": "(Optional) Street number",
                      "type": "integer"
                    },
                    "zipCode": {
                      "title": "Zip Code",
                      "description": "(Optional) Postal code",
                      "type": "string"
                    },
                    "city": {
                      "title": "City",
                      "description": "(Optional) City name",
                      "type": "string"
                    },
                    "country": {
                      "title": "Country",
                      "description": "(Optional) Country name",
                      "type": "string"
                    }
                  }
                }
              },
              "required": ["number", "address"]
            }
          },
          "required": [
            "startingDate",
            "vatRepresentative",
            "relatedResource",
            "idCard",
            "businessLicense",
            "vat"
          ],
          "title": "VAT Representation",
          "type": "object"
        },
        "proof": {
          "description": "A JSON Web Signature proof for a credential as defined by the VC data model",
          "properties": {
            "created": {
              "description": "Creation timestamp for the proof in the form of an XML datestring",
              "type": "string"
            },
            "jws": {
              "description": "The JSON Web Signature for the proof",
              "type": "string"
            },
            "proofPurpose": {
              "const": "assertionMethod",
              "description": "In the case of credentials, the proof should be the constant, 'assertionMethod'"
            },
            "type": {
              "description": "Signature suite for the proof",
              "enum": ["Ed25519Signature2018"],
              "type": "string"
            },
            "verificationMethod": {
              "description": "The fragment from which the public key can be de-referenced, in the form of a URI",
              "type": "string"
            }
          },
          "title": "proof",
          "type": "object"
        }
      },
      "title": "VAT Representation agreement",
      "type": "object"
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-e-origin-vat-representation-schema@1.0.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-e-origin-vat-representation-schema@1.0.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-e-origin-vat-representation-schema@1.0.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-e-origin-vat-representation-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { VATRepresentationAgreement } from "@cef-ebsi/vcdm1.1-e-origin-vat-representation-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
