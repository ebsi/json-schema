# @cef-ebsi/vcdm1.1-certificate-professional-competence-schema

## 1.0.0

### Major Changes

- 5185590: Create initial version of the Certificate Professional Competence schema.
