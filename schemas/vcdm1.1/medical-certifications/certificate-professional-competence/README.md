![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-certificate-professional-competence-schema

> Certificate Professional Competence
>
> Schema defining a verifiable credential that validates whether a registered physician has an up to date certificate of professional competence

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xdb2f27bb0c8d74d9c16190e4de1e22b39e005a00bd1e0aa51f0e29343bfcb4d6` (hexadecimal)
- `zFkbzRiX4Q7vcaMtNpgq7RzkvkhvRTt9KqMHqGWXrGn85` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Certificate Professional Competence",
  "description": "Schema defining a verifiable credential that validates whether a registered physician has an up to date certificate of professional competence",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Board Certified Doctor Credential",
          "type": "object",
          "properties": {
            "cip": {
              "description": "Indicates whether the registered physician has a current and valid certificate of professional competence.",
              "type": "boolean"
            }
          },
          "required": ["cip"]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-certificate-professional-competence-schema@1.0.0

# with Yarn
yarn add @cef-ebsi/vcdm1.1-certificate-professional-competence-schema@1.0.0

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-certificate-professional-competence-schema@1.0.0
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-certificate-professional-competence-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { CertificateProfessionalCompetence } from "@cef-ebsi/vcdm1.1-certificate-professional-competence-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
