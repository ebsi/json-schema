# @cef-ebsi/vcdm1.1-professional-medical-certification-schema

## 1.0.0

### Major Changes

- 5185590: Create initial version of the Professional Medical Certification schema.
