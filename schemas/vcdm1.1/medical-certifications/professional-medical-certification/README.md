![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-professional-medical-certification-schema

> Professional Medical Certifications
>
> Schema defining a verifiable credential associated with a registered physician

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x892d34859357311b1a60fa50666ec5ad6961e1a7238b602437d0c7cb628ea78b` (hexadecimal)
- `zAEUqPUrQVE7yiesF8xUVHYh4AnqjfHFxCv6GhZ3uvjkW` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Professional Medical Certifications",
  "description": "Schema defining a verifiable credential associated with a registered physician",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Board Certified Doctor Credential",
          "type": "object",
          "properties": {
            "medical_license_number": {
              "description": "Nationally registered physician number",
              "type": "string"
            },
            "name": {
              "description": "Natural person name",
              "type": "string"
            },
            "surname": {
              "description": "Natural person surname",
              "type": "string"
            },
            "medical_registration_status": {
              "description": "The status of the registered physician",
              "type": "string",
              "enum": ["ACTIVE", "LEAVE", "INHABILITED"]
            },
            "medical_practice_authorization": {
              "description": "Indicates if the physician is authorized to act",
              "type": "string"
            },
            "medical_board": {
              "description": "Corresponds to the official college of physicians where the professional is registered.",
              "type": "boolean"
            },
            "medical_specialties": {
              "description": "Describes the physician's professional specialization",
              "type": "array",
              "items": {
                "type": "string",
                "enum": [
                  "Allergology",
                  "Clinical Analysis",
                  "Pathological Anatomy",
                  "Anesthesiology and Reanimation",
                  "Angiology and Vascular Surgery",
                  "Digestive System",
                  "Clinical Biochemistry",
                  "Cardiology",
                  "Cardiovascular Surgery",
                  "General and Digestive Surgery",
                  "Oral and Maxillofacial Surgery",
                  "Orthopedic Surgery and Traumatology",
                  "Pediatric Surgery",
                  "Plastic, Aesthetic, and Reconstructive Surgery",
                  "Thoracic Surgery",
                  "Medical-Surgical Dermatology and Venereology",
                  "Endocrinology and Nutrition",
                  "Stomatology",
                  "Clinical Pharmacology",
                  "Geriatrics",
                  "Medical Hydrology",
                  "Hematology and Hemotherapy",
                  "Immunology",
                  "Occupational Medicine",
                  "Family and Community Medicine",
                  "Physical Education and Sports Medicine",
                  "Forensic Medicine",
                  "Physical Medicine and Rehabilitation",
                  "Intensive Medicine",
                  "Internal Medicine",
                  "Nuclear Medicine",
                  "Preventive Medicine and Public Health",
                  "Microbiology and Parasitology",
                  "Nephrology",
                  "Pulmonology",
                  "Neurosurgery",
                  "Clinical Neurophysiology",
                  "Neurology",
                  "Obstetrics and Gynecology",
                  "Ophthalmology",
                  "Medical Oncology",
                  "Radiation Oncology",
                  "Otorhinolaryngology",
                  "Pediatrics and its Specific Areas",
                  "Psychiatry",
                  "Radiology",
                  "Rheumatology",
                  "Urology",
                  "General Medicine",
                  "Child and Adolescent Psychiatry",
                  "Certified General Physician"
                ]
              }
            }
          },
          "required": [
            "medical_license_number",
            "name",
            "surname",
            "medical_registration_status",
            "medical_practice_authorization",
            "medical_board",
            "medical_specialties"
          ]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-professional-medical-certification-schema@1.0.0

# with Yarn
yarn add @cef-ebsi/vcdm1.1-professional-medical-certification-schema@1.0.0

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-professional-medical-certification-schema@1.0.0
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-professional-medical-certification-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { ProfessionalMedicalCertifications } from "@cef-ebsi/vcdm1.1-professional-medical-certification-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
