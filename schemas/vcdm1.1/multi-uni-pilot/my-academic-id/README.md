![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-multi-uni-pilot-my-academic-id-schema

> MyAcademicId
>
> Schema of an MyAcademicId Verifiable Credential

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x257539c110cd4f6050bbe6fbb805eea6099305fb0f03668616cf3165822b7b17` (hexadecimal)
- `z3XDm4kDtztE8DzLsVdhfshYvx2upnfLmqHtyVjkaXM1g` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "MyAcademicId",
  "description": "Schema of an MyAcademicId Verifiable Credential",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines additional properties on credentialSubject to describe IDs that do not have a substantial level of assurance.",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines a unique identifier of the credential subject. DID of the user",
              "type": "string"
            },
            "communityUserIdentifier": {
              "description": "User’s Community Identifier is an opaque and non-revocable identifier (i.e. it cannot change over time) that follows the syntax of eduPersonUniqueId  attribute of eduPerson. It consists of “uniqueID” part and fixed scope “erasmus.eduteams.org”, separated by at sign. The uniqueID part contains up to 64 hexadecimal digits (a-f, 0-9). The identifier is unique and persistent within the MyAcademicId namespace. The identifier can be used for identity matching, etc. OID: 1.3.6.1.4.1.5923.1.1.1.13 Definition: https: //wiki.refeds.org/display/STAN/eduPerson+2021-11#eduPerson202111-eduPersonUniqueId",
              "type": "string"
            },
            "europeanStudentIdentifier": {
              "description": "The European Student Identifier (ESI) of the user. ESI ensures mobility. Lifetime is limited to the period of student's mobility. ESI structure is defined in the document referenced below. ESI SHOULD NOT be parsed to extract information about the originating organisation of the student since the identifier structure is subject to a change. OID: 1.3.6.1.4.1.25178.1.2.14 Definition: https: //wiki.geant.org/display/SM/European+Student+Identifier",
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "externalAffiliation": {
              "description": "Affiliation within Home Organization.  One or more home organisations (such as, universities, research institutions or private companies) this user is affiliated with. The syntax and semantics follows eduPersonScopedAffiliation attribute. Affiliation is external to the MyAcademicId. OID: 1.3.6.1.4.1.25178.4.1.11 Definition: https: //wiki.refeds.org/display/STAN/eduPerson+2021-11#eduPerson202111-eduPersonScopedAffiliation",
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "organization": {
              "description": "This attribute describes the organization of this user. OID: 1.3.6.1.4.1.25178.1.2.9",
              "type": "string"
            },
            "displayName": {
              "description": "User’s name (firstname lastname). For more complex names. OID: 2.16.840.1.113730.3.1.241 Definition: https: //wiki.refeds.org/display/STAN/eduPerson+2021-11#eduPerson202111-displayName",
              "type": "string"
            },
            "givenName": {
              "description": "strings that are the part of a person's name that is not their surname (see RFC4519). OID: 2.5.4.Definition: https: //wiki.refeds.org/display/STAN/eduPerson+2021-11#eduPerson202111-givenName",
              "type": "string"
            },
            "familyName": {
              "description": "strings that are a person's surname (see RFC4519). OID: 2.5.4.4 Definition: https: //wiki.refeds.org/display/STAN/eduPerson+2021-11#eduPerson202111-sn",
              "type": "string"
            },
            "emailAddress": {
              "description": "address of the user. OID: 0.9.2342.19200300.100.1.3 Definition: https: //wiki.refeds.org/display/STAN/eduPerson+2021-11#eduPerson202111-homePostalAddress",
              "type": "string",
              "format": "email"
            },
            "entitlements": {
              "description": "This attribute describes the entitlements of this user. OID: 1.3.6.1.4.1.5923.1.1.1.7 Definition: https: //wiki.refeds.org/display/STAN/eduPerson+2021-11#eduPerson202111-eduPersonEntitlement",
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "assurance": {
              "description": "Assurance of the identity of the user, following REFEDS Assurance Framework (RAF). OID: 1.3.6.1.4.1.5923.1.1.1.11 Definition: https: //wiki.refeds.org/display/STAN/eduPerson+2021-11#eduPerson202111-eduPersonAssurance",
              "type": "array",
              "items": {
                "type": "string",
                "format": "uri"
              }
            }
          },
          "required": [
            "id",
            "communityUserIdentifier",
            "displayName",
            "givenName",
            "familyName",
            "emailAddress",
            "assurance"
          ]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-multi-uni-pilot-my-academic-id-schema@1.3.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-multi-uni-pilot-my-academic-id-schema@1.3.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-multi-uni-pilot-my-academic-id-schema@1.3.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-multi-uni-pilot-my-academic-id-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { MyAcademicId } from "@cef-ebsi/vcdm1.1-multi-uni-pilot-my-academic-id-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
