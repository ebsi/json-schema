![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-multi-uni-pilot-education-verifiable-accreditation-records-schema

> EBSI Education Verifiable Accreditation records
>
> EBSI Education Verifiable Accreditation record schema for educational contexts

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x4f9dc4947a020fccbdf435f53d5d7915d7706e0d8f910f73e6ff765202dd0467` (hexadecimal)
- `z6MnjrUbQrbadBDTJz2kzn3NMyxQxrkzFL4mQBSmPEbBt` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI Education Verifiable Accreditation records",
  "description": "EBSI Education Verifiable Accreditation record schema for educational contexts",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines a unique identifier of the Verifiable attestation",
              "type": "string"
            },
            "authorizationClaims": {
              "description": "Defines a list of claims that define/determine the authorization of an Issuer to issue certain types of VCs. Mandatory",
              "type": "object",
              "properties": {
                "accreditationType": {
                  "description": "Defines the type of accreditation. Mandatory*",
                  "type": "string",
                  "format": "uri"
                },
                "decision": {
                  "description": "Defines the Quality Decision issued by the Quality Assuring Authority",
                  "type": "string"
                },
                "report": {
                  "description": "Includes a publicly accessible report of the quality assurance decision",
                  "type": "array",
                  "items": {
                    "type": "string",
                    "format": "uri"
                  }
                },
                "limitQualification": {
                  "description": " Defines The qualification that was accredited ",
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "description": "Defines a portable and unique identifier of the qualification",
                        "type": "string",
                        "format": "uri"
                      },
                      "title": {
                        "description": "Defines the title of the qualification",
                        "type": "string"
                      },
                      "alternativeLabel": {
                        "description": "Defines an alternative name of the qualification",
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      },
                      "EQFLevel": {
                        "description": "Defines the qualification level as specified by the European Qualification Framework.",
                        "type": "string",
                        "format": "uri"
                      },
                      "NQFLevel": {
                        "description": "Defines the qualification level as specified by a National Qualification Framework.",
                        "type": "string",
                        "format": "uri"
                      }
                    }
                  }
                },
                "limitField": {
                  "description": "Defines The field of education for which the accreditation is valid",
                  "type": "array",
                  "items": {
                    "type": "string",
                    "format": "uri"
                  }
                },
                "limitQFLevel": {
                  "description": "Defines the european qualification level for which the accreditation is valid",
                  "type": "array",
                  "items": {
                    "type": "string",
                    "format": "uri"
                  }
                },
                "limitJurisdiction": {
                  "description": "Defines the jurisdiction for which the accreditation is valid",
                  "type": "array",
                  "items": {
                    "type": "string",
                    "format": "uri"
                  }
                },
                "reviewDate": {
                  "description": "Defines the The date when the accreditation has to be re-viewed",
                  "type": "string",
                  "format": "date-time"
                }
              },
              "required": ["accreditationType", "limitJurisdiction"]
            }
          },
          "required": ["id", "authorizationClaims"]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-multi-uni-pilot-education-verifiable-accreditation-records-schema@1.3.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-multi-uni-pilot-education-verifiable-accreditation-records-schema@1.3.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-multi-uni-pilot-education-verifiable-accreditation-records-schema@1.3.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-multi-uni-pilot-education-verifiable-accreditation-records-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSIEducationVerifiableAccreditationRecords } from "@cef-ebsi/vcdm1.1-multi-uni-pilot-education-verifiable-accreditation-records-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
