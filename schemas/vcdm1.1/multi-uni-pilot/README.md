# EBSI Multi Uni Pilot

Schemas for:

- [EBSI Education Verifiable Accreditation records](./education-verifiable-accreditation-records)
- [EBSI Natural Person Verifiable ID](./verifiable-attestation-individual-id)
- [EBSI Legal Entity Verifiable ID](./verifiable-attestation-organisational-id)
- [EBSI Verifiable Accreditation - Education Diploma](./verifiable-diploma)
- [MyAcademicId Verifiable Credential](./my-academic-id)
