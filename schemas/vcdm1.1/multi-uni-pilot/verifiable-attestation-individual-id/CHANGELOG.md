# @cef-ebsi/vcdm1.1-multi-uni-pilot-verifiable-attestation-individual-id-schema

## 1.3.1

### Patch Changes

- c7f7fa3: Update license text.
- Updated dependencies [c7f7fa3]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.3.1

## 1.3.0

### Minor Changes

- 191a769: Support `credentialStatus` as an array.

### Patch Changes

- 9e4aa93: Add "context" property to schema metadata.
- Updated dependencies [9e4aa93]
- Updated dependencies [191a769]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.3.0

## 1.3.0-next.0

### Minor Changes

- 191a769: Support `credentialStatus` as an array.

### Patch Changes

- 9e4aa93: Add "context" property to schema metadata.
- Updated dependencies [9e4aa93]
- Updated dependencies [191a769]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.3.0-next.0

## 1.2.0

### Minor Changes

- 021e4c2: Remove enum from `credentialSchema.type` on base Verifiable Attestation schema.

### Patch Changes

- Updated dependencies [021e4c2]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.2.0

## 1.1.2

### Patch Changes

- e1e8cf3: Export examples in JS file.
- Updated dependencies [e1e8cf3]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.1.2

## 1.1.1

### Patch Changes

- 85a6797: Embed examples in npm package, add JSON Schema to README file.
- 6152c5b: Fix typo in folder and package name.
- Updated dependencies [85a6797]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.1.1

## 1.1.0

### Minor Changes

- 5cf7cbb: Update Verifiable Attestation schema, support `JsonSchema` credential schema type.

### Patch Changes

- Updated dependencies [5cf7cbb]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.1.0

## 1.0.0

Define schema as npm package.

## Legacy versions

### 2024-02

- VCDM 1.1 baseline update

### 2022-11

- Pump to json-schema 2020-12

### 2022-02

- Changed `$schema` to `draft-07`.
- Updated EBSI Attestation schema to `2022-02`.

### 2021-12

Initial schema.
