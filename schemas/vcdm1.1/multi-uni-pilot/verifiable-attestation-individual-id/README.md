![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-multi-uni-pilot-verifiable-attestation-individual-id-schema

> EBSI Natural Person Verifiable ID
>
> Schema of an EBSI Verifiable ID for a natural person participating in the educational use cases

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xf32074e234c64a7f358c54599f2637b71b7d7ba67420878d82ac076700fdc303` (hexadecimal)
- `zHN4myAdVxwu6mZ3GMoCZDGVP8qLX9vEHkmwtmfnE9Dox` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI Natural Person Verifiable ID",
  "description": "Schema of an EBSI Verifiable ID for a natural person participating in the educational use cases",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines additional properties on credentialSubject to describe IDs that do not have a substantial level of assurance.",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines a unique identifier of the credential subject",
              "type": "string"
            },
            "identifier": {
              "description": "Defines an alternative identifier for the person ",
              "type": "array",
              "items": { "$ref": "#/$defs/identifier" }
            },
            "familyName": {
              "description": "Defines current family name(s) of the credential subject",
              "type": "string"
            },
            "firstName": {
              "description": "Defines current first name(s) of the credential subject",
              "type": "string"
            },
            "dateOfBirth": {
              "description": "Defines date of birth of the credential subject",
              "type": "string",
              "format": "date"
            },
            "personalIdentifier": {
              "description": "Defines the unique national identifier of the credential subject (constructed by the sending Member State in accordance with the technical specifications for the purposes of cross-border identification and which is as persistent as possible in time)",
              "type": "string"
            },
            "nameAndFamilyNameAtBirth": {
              "description": "Defines the first and the family name(s) of the credential subject at the time of their birth",
              "type": "string"
            },
            "placeOfBirth": {
              "description": "Defines the place where the credential subject is born",
              "type": "string"
            },
            "currentAddress": {
              "description": "Defines the current address of the credential subject",
              "type": "array",
              "items": { "type": "string" }
            },
            "gender": {
              "description": "Defines the gender of the credential subject",
              "type": "string"
            }
          },
          "required": ["id"]
        }
      }
    }
  ],
  "$defs": {
    "identifier": {
      "description": "Defines an alternative Identifier object ",
      "type": "object",
      "properties": {
        "schemeID": {
          "description": "Defines the schema used to define alternative identification",
          "type": "string"
        },
        "value": {
          "description": "Define the alternative identification value",
          "type": "string"
        },
        "id": {
          "description": "The URI of the identifier",
          "type": "string",
          "format": "uri"
        }
      },
      "required": ["schemeID", "value"]
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-multi-uni-pilot-verifiable-attestation-individual-id-schema@1.3.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-multi-uni-pilot-verifiable-attestation-individual-id-schema@1.3.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-multi-uni-pilot-verifiable-attestation-individual-id-schema@1.3.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-multi-uni-pilot-verifiable-attestation-individual-id-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSINaturalPersonVerifiableID } from "@cef-ebsi/vcdm1.1-multi-uni-pilot-verifiable-attestation-individual-id-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
