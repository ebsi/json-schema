# @cef-ebsi/vcdm1.1-alliance-id-schema

## 1.0.2

### Patch Changes

- c7f7fa3: Update license text.
- Updated dependencies [c7f7fa3]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.3.1

## 1.0.1

### Patch Changes

- ad939ad: Update schema title.

## 1.0.0

### Major Changes

- 9f95e71: Create initial version of the EBSI Verifiable University Alliance ID schema.
