![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-alliance-id-schema

> Verifiable AllianceID
>
> Schema of an EBSI Verifiable University Alliance ID for a natural person participating in the Alliance

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xa7b1794661f4b1532a18aa7f6c2091300fe79533cca782abbd302eb59f6719fc` (hexadecimal)
- `zCHc3ZfYg2871W2WftjLu4QNMQrDzG57oG5pvGoyHcagB` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Verifiable AllianceID",
  "description": "Schema of an EBSI Verifiable University Alliance ID for a natural person participating in the Alliance",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines additional properties on credentialSubject to describe IDs that do not have a substantial level of assurance.",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines a unique identifier of the credential subject",
              "type": "string"
            },
            "identifier": {
              "type": "object",
              "description": "Defines the identifier for the University Alliance. Format: urn:schac:europeanUniversityAllianceCode:int:euai:<sHO>:<code>. sHO: the schacHomeOrganization of the Alliance that issued the credential, <code> the university alliance code",
              "$ref": "#/$defs/identifier"
            }
          },
          "required": ["id", "identifier"]
        }
      }
    }
  ],
  "$defs": {
    "identifier": {
      "description": "Defines an alternative Identifier object",
      "type": "object",
      "properties": {
        "schemeID": {
          "description": "Defines the schema used to define alternative identification",
          "type": "string"
        },
        "value": {
          "description": "Define the alternative identification value",
          "type": "string"
        },
        "id": {
          "description": "The URI of the identifier",
          "type": "string",
          "format": "uri"
        }
      },
      "required": ["schemeID", "value"]
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-alliance-id-schema@1.0.2

# with Yarn
yarn add @cef-ebsi/vcdm1.1-alliance-id-schema@1.0.2

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-alliance-id-schema@1.0.2
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm1.1-alliance-id-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { VerifiableAllianceID } from "@cef-ebsi/vcdm1.1-alliance-id-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
