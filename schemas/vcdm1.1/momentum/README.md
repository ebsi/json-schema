![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-onepass-momentum-schema

> Momentum Credential
>
> Defines the schema of the Momentum credential used for scoring Start Ups investability

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x974236ed695e154ae5c894caff3337ce0375f6804f9297fccf0f7f7861b1ee11` (hexadecimal)
- `zBBT88HQMktUKDwizDJV99LF4XqVagC8bT1nh4wvUKqpC` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Momentum Credential",
  "description": "Defines the schema of the Momentum credential used for scoring Start Ups investability",
  "type": "object",
  "allOf": [
    {
      "$ref": "node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines basic information about the subject and the scores in the Momentum VC",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines the DID of the subject that is described by the Credential",
              "type": "string",
              "format": "uri"
            },
            "dataTimestamp": {
              "description": "Timestamp of the data received to issue the scores",
              "type": "string",
              "format": "date-time"
            },
            "benchmarkTimestamp": {
              "description": "Timestamp when the scores were benchmarked/calculated",
              "type": "string",
              "format": "date-time"
            },
            "legalName": {
              "description": "Official legal name of Credential Subject",
              "type": "string"
            },
            "VATRegistration": {
              "description": "VAT number of Credential Subject",
              "type": "string"
            },
            "scoreTeamSize": {
              "description": "Team size score",
              "type": "number"
            },
            "scoreFoundersExperience": {
              "description": "Founder's experience score",
              "type": "number"
            },
            "scoreDiversity": {
              "description": "Diversity score",
              "type": "number"
            },
            "scoreOriginalInsight": {
              "description": "Original insight score",
              "type": "number"
            },
            "scoreDirectorsBoard": {
              "description": "Director's board score",
              "type": "number"
            },
            "scoreVelocity": {
              "description": "Velocity score",
              "type": "number"
            }
          },
          "required": [
            "id",
            "legalName",
            "dataTimestamp",
            "benchmarkTimestamp",
            "VATRegistration",
            "scoreTeamSize",
            "scoreFoundersExperience",
            "scoreDiversity",
            "scoreOriginalInsight",
            "scoreDirectorsBoard",
            "scoreVelocity"
          ]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-onepass-momentum-schema@1.0.0

# with Yarn
yarn add @cef-ebsi/vcdm1.1-onepass-momentum-schema@1.0.0

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-onepass-momentum-schema@1.0.0
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm1.1-onepass-momentum-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { MomentumCredential } from "@cef-ebsi/vcdm1.1-onepass-momentum-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
