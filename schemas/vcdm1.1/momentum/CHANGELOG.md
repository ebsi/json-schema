# @cef-ebsi/vcdm1.1-onepass-momentum-schema

## 1.0.0

### Major Changes

- 5185590: Create initial version of the Momentum credential schema.
