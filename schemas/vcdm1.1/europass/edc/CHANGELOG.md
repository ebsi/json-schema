# @cef-ebsi/vcdm1.1-europass-edc-schema

## 2.1.2

### Patch Changes

- c7f7fa3: Update license text.
- Updated dependencies [c7f7fa3]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.3.1

## 2.1.1

### Patch Changes

- 96fe23f: Replace `credentialStatus.type` supported values "CredentialStatus" and "TrustedCredentialStatus2021" with "StatusList2021Entry".

## 2.1.0

### Minor Changes

- 191a769: Support `credentialStatus` as an array.

### Patch Changes

- 9e4aa93: Add "context" property to schema metadata.
- Updated dependencies [9e4aa93]
- Updated dependencies [191a769]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.3.0

## 2.1.0-next.0

### Minor Changes

- 191a769: Support `credentialStatus` as an array.

### Patch Changes

- 9e4aa93: Add "context" property to schema metadata.
- Updated dependencies [9e4aa93]
- Updated dependencies [191a769]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.3.0-next.0

## 2.0.0

### Major Changes

- b7427f2: - Edit `BooleanType` definition.
  - Add `Single!ConceptType` definition.
  - Replace `Many!ShaclValidator2017Type` definition with `Many!CredentialSchemaType` definition.
  - Add `VerifiableAttestation` to `EuropeanDigitalCredentialType` enum.

### Minor Changes

- 021e4c2: Remove enum from `credentialSchema.type` on base Verifiable Attestation schema.

### Patch Changes

- Updated dependencies [021e4c2]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.2.0

## 1.0.2

### Patch Changes

- e1e8cf3: Export examples in JS file.
- Updated dependencies [e1e8cf3]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.1.2

## 1.0.1

### Patch Changes

- 85a6797: Embed examples in npm package, add JSON Schema to README file.
- Updated dependencies [85a6797]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.1.1

## 1.0.0

### Major Changes

- 3e15f70: Create initial version of Europass EDC credential schema (VCDM 1.1).

### Patch Changes

- Updated dependencies [5cf7cbb]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.1.0
