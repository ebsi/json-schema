![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-type-extensions-terms-of-use-accreditation-policy-schema

> AccreditationPolicy
>
> Defines the Accreditation Policy under which the Verifiable Credential has been issued

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xeae40291dc6c7dbf1a54cfd9270650997e33fc9fcb3e637f390c0b6916102bc1` (hexadecimal)
- `zGov4ANWQ6CGwDt4P3k78uymkCM2y2tbapd7Jk3DPTeRn` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "AccreditationPolicy",
  "description": "Defines the Accreditation Policy under which the Verifiable Credential has been issued",
  "type": "object",
  "properties": {
    "termsOfUse": {
      "anyOf": [
        {
          "$ref": "#/$defs/termsOfUse"
        },
        {
          "type": "array",
          "description": "One item must match the type extension",
          "contains": {
            "$ref": "#/$defs/termsOfUse"
          }
        }
      ]
    }
  },
  "$defs": {
    "termsOfUse": {
      "description": "Defines the Accreditation Policy under which the Verifiable Credential has been issued",
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "description": "Type of the TermsOfUse entry. It MUST be AccreditationPolicy.",
          "enum": ["AccreditationPolicy"]
        },
        "trustFramework": {
          "type": "string",
          "description": "Name of the Trust Framework (TF)."
        },
        "policyId": {
          "type": "string",
          "description": "URI or URL identifying the policy under which the Trust Framework operates or Verifiable Accreditation or Attestation has been Issued.",
          "format": "uri"
        },
        "lib": {
          "type": "string",
          "description": "Legal basis for the tf, is a string as 'professional qualifications directive' or eIDAS. OPTIONAL.",
          "format": "uri"
        },
        "parentAccreditation": {
          "type": "string",
          "description": "URL to access the parent entity in the trust chain supporting the attestation. REQUIRED.",
          "format": "uri"
        },
        "rootAuthorisation": {
          "type": "string",
          "description": "URL to access the root entity in the trust chain supporting the attestation. REQUIRED.",
          "format": "uri"
        }
      },
      "required": ["type", "parentAccreditation", "rootAuthorisation"]
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-type-extensions-terms-of-use-accreditation-policy-schema@1.0.3

# with Yarn
yarn add @cef-ebsi/vcdm1.1-type-extensions-terms-of-use-accreditation-policy-schema@1.0.3

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-type-extensions-terms-of-use-accreditation-policy-schema@1.0.3
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-type-extensions-terms-of-use-accreditation-policy-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { AccreditationPolicy } from "@cef-ebsi/vcdm1.1-type-extensions-terms-of-use-accreditation-policy-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
