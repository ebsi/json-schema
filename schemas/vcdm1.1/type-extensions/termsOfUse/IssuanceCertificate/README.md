![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-type-extensions-terms-of-use-issuance-certificate-schema

> EBSI type extension for use of IssuanceCertificate
>
> Defines accredited trust chain bindings for an issued Verifiable Credentials

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x1d7146f8897aa6cd5c59321ea0756ec61277028e4a8b3c13ec1b310ec47e6495` (hexadecimal)
- `z2yw1Mq2Cq4jN8B2bJ66cGcFEqjrJQUinxHAUjZTkVKjz` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI type extension for use of IssuanceCertificate",
  "description": "Defines accredited trust chain bindings for an issued Verifiable Credentials",
  "type": "object",
  "properties": {
    "termsOfUse": {
      "anyOf": [
        {
          "$ref": "#/$defs/termsOfUse"
        },
        {
          "type": "array",
          "description": "One item must match the type extension",
          "contains": {
            "$ref": "#/$defs/termsOfUse"
          }
        }
      ]
    }
  },
  "$defs": {
    "termsOfUse": {
      "description": "Extension binds the Issuer's Verifiable Accreditation into any issued Verifiable Credential",
      "type": "object",
      "properties": {
        "id": {
          "description": "References the Verifiable Credential stored as an attribute of the issuer in the Trusted Issuers Registry (TIR).",
          "type": "string",
          "format": "uri"
        },
        "type": {
          "description": "Defines the term of use type",
          "type": "string",
          "const": "IssuanceCertificate"
        }
      },
      "required": ["id", "type"]
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-type-extensions-terms-of-use-issuance-certificate-schema@1.0.3

# with Yarn
yarn add @cef-ebsi/vcdm1.1-type-extensions-terms-of-use-issuance-certificate-schema@1.0.3

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-type-extensions-terms-of-use-issuance-certificate-schema@1.0.3
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-type-extensions-terms-of-use-issuance-certificate-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSITypeExtensionForUseOfIssuanceCertificate } from "@cef-ebsi/vcdm1.1-type-extensions-terms-of-use-issuance-certificate-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
