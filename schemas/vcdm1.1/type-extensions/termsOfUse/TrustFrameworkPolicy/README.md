![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-type-extensions-terms-of-use-trust-framework-policy-schema

> TrustFrameworkPolicy
>
> Defines Trust Framework Policy under which the Verifiable Credential has been issued

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xf3266840f47906803165259bf47c9195970e588ec473d7843ef33774df87d5aa` (hexadecimal)
- `zHNA3DDeLjaF4WvBiStkNHriNUCvZw3AdnbzJkddTkDRs` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "TrustFrameworkPolicy",
  "description": "Defines Trust Framework Policy under which the Verifiable Credential has been issued",
  "type": "object",
  "properties": {
    "termsOfUse": {
      "anyOf": [
        {
          "$ref": "#/$defs/termsOfUse"
        },
        {
          "type": "array",
          "description": "One item must match the type extension",
          "contains": {
            "$ref": "#/$defs/termsOfUse"
          }
        }
      ]
    }
  },
  "$defs": {
    "termsOfUse": {
      "description": "Defines Trust Framework Policy under which the Verifiable Credential has been issued",
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "description": "Type of the TermsOfUse entry. It MUST be TrustFrameworkPolicy. Other types may be added in the future.",
          "enum": ["TrustFrameworkPolicy"]
        },
        "trustFramework": {
          "type": "string",
          "description": "Name of the Trust Framework (TF). REQUIRED."
        },
        "policyId": {
          "type": "string",
          "description": "URI or URL identifying the policy under which the Trust Framework operates or Verifiable Accreditation or Attestation has been Issued. REQUIRED.",
          "format": "uri"
        },
        "lib": {
          "type": "string",
          "description": "Legal basis for the tf, is a string as 'professional qualifications directive' or eIDAS. OPTIONAL."
        }
      },
      "required": ["type", "trustFramework", "policyId"]
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-type-extensions-terms-of-use-trust-framework-policy-schema@1.0.3

# with Yarn
yarn add @cef-ebsi/vcdm1.1-type-extensions-terms-of-use-trust-framework-policy-schema@1.0.3

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-type-extensions-terms-of-use-trust-framework-policy-schema@1.0.3
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-type-extensions-terms-of-use-trust-framework-policy-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { TrustFrameworkPolicy } from "@cef-ebsi/vcdm1.1-type-extensions-terms-of-use-trust-framework-policy-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
