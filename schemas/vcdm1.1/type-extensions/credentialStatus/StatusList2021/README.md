![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-type-extensions-credential-status-status-list-2021-schema

> EBSI type extension for use of StatusList2021
>
> Defines revocation details for an issued Verifiable Credential

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x60d2b6a9a923dacfa6c2cb03d101b92c2b2683ed5e248ffa8fb414edbb6b9670` (hexadecimal)
- `z7WxViWAEwg9G3fadmM1k9YTtMq9QLkGgGMie86JMWajZ` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI type extension for use of StatusList2021",
  "description": "Defines revocation details for an issued Verifiable Credential",
  "type": "object",
  "properties": {
    "credentialStatus": {
      "description": "Contains information about how to verify the status of the Verifiable Attestation",
      "type": "object",
      "properties": {
        "id": {
          "description": "References the unique URI of the status",
          "type": "string",
          "format": "uri"
        },
        "type": {
          "description": "Defines the Verifiable Credential status type",
          "type": "string",
          "const": "StatusList2021Entry"
        },
        "statusPurpose": {
          "description": "Purpose of the status entry",
          "type": "string",
          "enum": ["revocation", "suspension"]
        },
        "statusListIndex": {
          "description": "Integer expressed as a string. The zero based index value identifies the bit position of the status",
          "type": "string"
        },
        "statusListCredential": {
          "description": "URL referencing the StatusList2021Credential containing the index",
          "type": "string",
          "format": "uri"
        }
      },
      "required": ["id", "type", "statusListIndex", "statusListCredential"]
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-type-extensions-credential-status-status-list-2021-schema@1.0.4

# with Yarn
yarn add @cef-ebsi/vcdm1.1-type-extensions-credential-status-status-list-2021-schema@1.0.4

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-type-extensions-credential-status-status-list-2021-schema@1.0.4
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-type-extensions-credential-status-status-list-2021-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSITypeExtensionForUseOfStatusList2021 } from "@cef-ebsi/vcdm1.1-type-extensions-credential-status-status-list-2021-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
