![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-presentation-schema

> EBSI Verifiable Presentation
>
> Schema of an EBSI Verifiable Presentation

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xdacd5a6b1a92d5da622514b9c10cf208ccbc1ad7e1e7d9d80073d8416114024e` (hexadecimal)
- `zFj7VdCiHdG4GB6fezdAUKhDEuxFR2bri2ihKLkiZYpE9` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI Verifiable Presentation",
  "description": "Schema of an EBSI Verifiable Presentation",
  "type": "object",
  "properties": {
    "@context": {
      "description": "Defines semantic context of the Verifiable Presentation",
      "type": "array",
      "items": {
        "type": "string",
        "format": "uri"
      }
    },
    "type": {
      "description": "Defines the Verifiable Presentation type",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "holder": {
      "description": "Defines unique identifier of the party who shares the Verifiable Presentation",
      "type": "string"
    },
    "verifiableCredential": {
      "description": "Contains the personal information intended to be shared",
      "type": "array",
      "items": {
        "oneOf": [{ "type": "object" }, { "type": "string" }]
      }
    },
    "proof": {
      "description": "Contains information about the proof",
      "type": "object",
      "properties": {
        "type": {
          "description": "Defines the proof type",
          "type": "string"
        },
        "proofPurpose": {
          "description": "Defines the purpose of the proof",
          "type": "string"
        },
        "created": {
          "description": "Defines the date and time, when the proof has been created",
          "type": "string",
          "format": "date-time"
        },
        "verificationMethod": {
          "description": "Contains information about the verification method / proof mechanisms",
          "type": "string"
        },
        "challenge": {
          "description": "Defines a random or pseudo-random value used by some authentication protocols to mitigate replay attacks",
          "type": "string"
        },
        "domain": {
          "description": "Defines a string value that specifies the operational domain of a digital proof",
          "type": "string",
          "format": "hostname"
        },
        "jws": {
          "description": "Defines the proof value in JWS format",
          "type": "string"
        }
      },
      "required": [
        "type",
        "proofPurpose",
        "created",
        "verificationMethod",
        "jws"
      ]
    }
  },
  "required": ["@context", "type", "holder", "verifiableCredential"]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-presentation-schema@1.0.4

# with Yarn
yarn add @cef-ebsi/vcdm1.1-presentation-schema@1.0.4

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-presentation-schema@1.0.4
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm1.1-presentation-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSIVerifiablePresentation } from "@cef-ebsi/vcdm1.1-presentation-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
