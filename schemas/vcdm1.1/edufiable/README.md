![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-edufiable-schema

> edufiable
>
> Schema defining a verifiable credential issued to a student upon successful course completion

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x79db8363cf5e8f294a8c4cd7894224258990eb69385a026e3e040219b97d019d` (hexadecimal)
- `z9CgUGFxgrBsnXzvN7ABk4PDQPxtzJuUVH2p1w7QDQMxY` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "edufiable",
  "description": "Schema defining a verifiable credential issued to a student upon successful course completion",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "type": "object",
          "properties": {
            "id": {
              "type": "string",
              "format": "uri",
              "description": "Defines the DID of the subject"
            },
            "dateOfBirth": {
              "type": "string",
              "format": "date",
              "description": "Date of birth of the subject"
            },
            "familyName": {
              "type": "string",
              "description": "Family name of the subject"
            },
            "givenNames": {
              "type": "string",
              "description": "Given names of the subject"
            },
            "diploma": {
              "type": "object",
              "description": "Defines the diploma awarded to the subject",
              "properties": {
                "id": {
                  "type": "string",
                  "format": "uri",
                  "description": "URL to the course description web page"
                },
                "diplomaType": {
                  "type": "string",
                  "description": "Type of diploma",
                  "examples": [
                    "Bachelor",
                    "Masters",
                    "PhD",
                    "Microcredential",
                    "etc"
                  ]
                },
                "diplomaTitle": {
                  "type": "string",
                  "description": "Title of the awarded diploma"
                },
                "diplomaDescription": {
                  "type": "string",
                  "description": "Description of the course and its objectives"
                },
                "awardingInstitution": {
                  "type": "object",
                  "properties": {
                    "institutionId": {
                      "type": "string",
                      "format": "uri",
                      "description": "DID of the institution"
                    },
                    "institutionName": {
                      "type": "string",
                      "description": "Name of the institution"
                    }
                  },
                  "required": ["institutionId"]
                },
                "dateOfDiploma": {
                  "type": "string",
                  "format": "date",
                  "description": "Date when the diploma was awarded"
                },
                "finalGrade": {
                  "type": "string",
                  "description": "Final grade achieved"
                },
                "gradingScheme": {
                  "type": "object",
                  "description": "Information about the grading system",
                  "properties": {
                    "description": {
                      "type": "string",
                      "description": "Description of the grading scheme used"
                    },
                    "url": {
                      "type": "string",
                      "format": "uri",
                      "description": "URL to the grading scheme definition"
                    }
                  },
                  "required": ["description"]
                },
                "studyProgram": {
                  "type": "array",
                  "description": "Details of the completed study program",
                  "items": {
                    "$ref": "#/$defs/studyProgramModule"
                  }
                }
              },
              "required": [
                "id",
                "diplomaType",
                "diplomaTitle",
                "diplomaDescription",
                "awardingInstitution",
                "dateOfDiploma",
                "finalGrade",
                "gradingScheme"
              ]
            }
          },
          "required": [
            "id",
            "dateOfBirth",
            "familyName",
            "givenNames",
            "diploma"
          ]
        }
      }
    }
  ],
  "$defs": {
    "studyProgramModule": {
      "type": "object",
      "properties": {
        "id": {
          "type": "string",
          "format": "uri",
          "description": "URL to the course description"
        },
        "vc": {
          "type": "string",
          "format": "uri",
          "description": "URL to the VC of this course"
        },
        "name": {
          "type": "string",
          "description": "Name of the module"
        },
        "description": {
          "type": "string",
          "description": "Description of module objectives and components"
        },
        "gradeObtained": {
          "type": "string",
          "description": "Grade obtained in this module"
        }
      },
      "required": ["id", "vc", "name", "description", "gradeObtained"]
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-edufiable-schema@1.0.0

# with Yarn
yarn add @cef-ebsi/vcdm1.1-edufiable-schema@1.0.0

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-edufiable-schema@1.0.0
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm1.1-edufiable-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { Edufiable } from "@cef-ebsi/vcdm1.1-edufiable-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
