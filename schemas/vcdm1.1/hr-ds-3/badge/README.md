![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-hr-ds-3-badge-schema

> HR.DS.3 Badge Credential
>
> Schema of a HR.DS.3 Badge Credential

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xfab56098b9070d83f8731c8b189c9406510ebfe46f4b28072dd4d4e857fb486c` (hexadecimal)
- `zHsfLTXNUo5xeBRMt8sGHHuKVCuQkYpCG2XyH19RgX4kb` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "HR.DS.3 Badge Credential",
  "description": "Schema of a HR.DS.3 Badge Credential",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "type": {
          "description": "Defines the Verifiable Credential type.",
          "type": "array",
          "contains": {
            "type": "string",
            "const": "AccessBadge"
          }
        },
        "credentialSubject": {
          "description": "Defines additional properties on credentialSubject to describe IDs that do not have a substantial level of assurance.",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines a unique identifier of the credential subject",
              "type": "string"
            },
            "issuerName": {
              "description": "Issuer name",
              "type": "string"
            },
            "givenName": {
              "description": "Name strings that are the part of a person's name that is not their surname (see RFC4519).",
              "type": "string"
            },
            "familyName": {
              "description": "Name strings that are a person's surname (see RFC4519).",
              "type": "string"
            },
            "emailAddress": {
              "description": "Email address of the user.",
              "type": "string",
              "format": "email"
            },
            "photoURI": {
              "description": "Photo uri for this credential",
              "type": "string",
              "format": "uri"
            },
            "badgeStatus": {
              "description": "Status for this badge",
              "type": "string"
            },
            "badgeSituation": {
              "description": "Situation for this badge",
              "type": "string"
            },
            "badgeInstitution": {
              "description": "Institution of this badge",
              "type": "string"
            },
            "perID": {
              "description": "Per ID for this credential",
              "type": "string"
            },
            "badgeID": {
              "description": "Badge ID for this credential",
              "type": "string"
            },
            "badgeFunction": {
              "description": "Per ID of this badge",
              "type": "string"
            },
            "xRayRequired": {
              "description": "Is x-ray required for this badge",
              "type": "boolean"
            }
          },
          "required": [
            "id",
            "issuerName",
            "givenName",
            "familyName",
            "emailAddress",
            "photoURI",
            "badgeStatus",
            "badgeSituation",
            "badgeInstitution",
            "badgeID",
            "perID",
            "xRayRequired"
          ]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-hr-ds-3-badge-schema@1.3.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-hr-ds-3-badge-schema@1.3.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-hr-ds-3-badge-schema@1.3.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm1.1-hr-ds-3-badge-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { HRDS3BadgeCredential } from "@cef-ebsi/vcdm1.1-hr-ds-3-badge-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
