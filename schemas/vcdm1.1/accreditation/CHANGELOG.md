# @cef-ebsi/vcdm1.1-accreditation-schema

## 1.4.1

### Patch Changes

- c7f7fa3: Update license text.
- Updated dependencies [c7f7fa3]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.3.1

## 1.4.0

### Minor Changes

- 900afc7: Make `credentialSubject.accreditedFor[x].limitJurisdiction` and `credentialStatus` properties optional.

## 1.3.0

### Minor Changes

- 191a769: Support `credentialStatus` as an array.

### Patch Changes

- 9e4aa93: Add "context" property to schema metadata.
- Updated dependencies [9e4aa93]
- Updated dependencies [191a769]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.3.0

## 1.3.0-next.0

### Minor Changes

- 191a769: Support `credentialStatus` as an array.

### Patch Changes

- 9e4aa93: Add "context" property to schema metadata.
- Updated dependencies [9e4aa93]
- Updated dependencies [191a769]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.3.0-next.0

## 1.2.0

### Minor Changes

- 021e4c2: Remove enum from `credentialSchema.type` on base Verifiable Attestation schema.

### Patch Changes

- Updated dependencies [021e4c2]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.2.0

## 1.1.2

### Patch Changes

- e1e8cf3: Export examples in JS file.
- Updated dependencies [e1e8cf3]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.1.2

## 1.1.1

### Patch Changes

- 85a6797: Embed examples in npm package, add JSON Schema to README file.
- Updated dependencies [85a6797]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.1.1

## 1.1.0

### Minor Changes

- 5cf7cbb: Update Verifiable Attestation schema, support `JsonSchema` credential schema type.

### Patch Changes

- Updated dependencies [5cf7cbb]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.1.0

## 1.0.0

Define schema as npm package.

## Legacy versions

### 2024-01

- VCDM 1.1 baseline update

### 2023-04

- Add `reservedAttributeId` to define TIR attribute location for the credential
- Mandatory `EbsiAccreditationEntry` revocation type to follow Trusted Registry state

### 2022-11_01

- Use `extended types` to identify between accreditation to accredit and to attest
- Add `types` property to define the exact accreditation in schema

### 2022-11

- Pump to json-schema 2020-12

### 2022-05

- Reference EBSI Accredited Verifiable Attestation in `schema.json`.
- Update EBSI Verifiable Accreditation schema to `2022-05`.

### 2022-03

- Updated EBSI Verifiable Accreditation according to the new data model.

### 2022-02

- Changed `$schema` to `draft-07`.
- Updated EBSI Attestation schema to `2022-02`.

### 2022-01

Initial schema.
