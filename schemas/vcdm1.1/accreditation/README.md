![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-accreditation-schema

> EBSI Verifiable Accreditation Record
>
> Schema of an EBSI Verifiable Accreditation

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xb52fa2224ac2097b4ce403203934b1a16e7b5bd63c1d31d9b71ee1e9fbce9764` (hexadecimal)
- `zDCGvPRUnnSTkVgnX6xRwQMz8hh4VYuJ4r7nxCNGgSTB1` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI Verifiable Accreditation Record",
  "description": "Schema of an EBSI Verifiable Accreditation",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines additional information about the subject that is described by the Verifiable Accreditation",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines a unique identifier of the Verifiable Attestation",
              "type": "string",
              "format": "uri"
            },
            "reservedAttributeId": {
              "description": "Defines the attributeId this Verifiable Accreditation has been created for",
              "type": "string"
            },
            "accreditedFor": {
              "description": "Defines a list of claims that define/determine the authorisation of an Issuer to issue certain types of VCs",
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "schemaId": {
                    "description": "Schema, registered in Trusted  Schemas Registry, which the accredited organisation is allowed to issue, as per their accreditation",
                    "type": "string",
                    "format": "uri"
                  },
                  "types": {
                    "type": "array",
                    "items": {
                      "type": "string"
                    }
                  },
                  "limitJurisdiction": {
                    "anyOf": [
                      {
                        "description": "Defines the jurisdiction for which the accreditation is valid",
                        "type": "string",
                        "format": "uri"
                      },
                      {
                        "type": "array",
                        "description": "Defines the jurisdictions for which the accreditation is valid",
                        "items": {
                          "type": "string",
                          "format": "uri"
                        }
                      }
                    ]
                  }
                },
                "required": ["schemaId", "types"]
              }
            }
          },
          "required": ["id", "reservedAttributeId", "accreditedFor"]
        },
        "credentialStatus": {
          "description": "Defines revocation details for the issued credential. Further redefined by type extension",
          "type": "object",
          "properties": {
            "id": {
              "description": "Exact identity for the credential status",
              "type": "string",
              "format": "uri"
            },
            "type": {
              "description": "Defines the revocation status type",
              "type": "string",
              "const": "EbsiAccreditationEntry"
            }
          },
          "required": ["id", "type"]
        }
      },
      "required": ["expirationDate", "credentialSubject", "termsOfUse"]
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-accreditation-schema@1.4.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-accreditation-schema@1.4.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-accreditation-schema@1.4.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm1.1-accreditation-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSIVerifiableAccreditationRecord } from "@cef-ebsi/vcdm1.1-accreditation-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
