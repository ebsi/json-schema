![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-vid-legal-entity-schema

> EBSI Legal Entity Verifiable ID
>
> Schema of an EBSI Verifiable ID for a legal entity

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x32e9ef7b96c89b72a86cbfc82e434fb319efd7442f4702a93fe368077abdb3f4` (hexadecimal)
- `z4RkHAFZirc3Ad24W9yoqq161wUeEDpRecLihzGducQUb` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI Legal Entity Verifiable ID",
  "description": "Schema of an EBSI Verifiable ID for a legal entity",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines information about the subject that is described by the Verifiable ID",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines the DID of the subject that is described by the Verifiable Attestation",
              "type": "string",
              "format": "uri"
            },
            "euid": {
              "description": "European Unique Identifier, for companies operating across multiple EU countries.",
              "type": "string"
            },
            "legalPersonIdentifier": {
              "description": "National/Legal Identifier of Credential Subject (constructed by the sending Member State in accordance with the technical specifications for the purposes of cross-border identification and which is as persistent as possible in time)",
              "type": "string"
            },
            "legalName": {
              "description": "Official legal name of Credential Subject",
              "type": "string"
            },
            "legalAddress": {
              "description": "Official legal address of Credential Subject",
              "type": "string"
            },
            "VATRegistration": {
              "description": "VAT number  of Credential Subject",
              "type": "string"
            },
            "taxReference": {
              "description": "Official tax reference number of Credential Subject",
              "type": "string"
            },
            "LEI": {
              "description": "Official legal entity identifier (LEI) of Credential Subject (referred to in Commission Implementing Regulation (EU) No 1247/2012)",
              "type": "string"
            },
            "EORI": {
              "description": "Economic Operator Registration and Identification (EORI) of Credential Subject (referred to in Commission Implementing Regulation (EU) No 1352/2013)",
              "type": "string"
            },
            "SEED": {
              "description": "System for Exchange of Excise Data (SEED) of Credential Subject (i.e. excise number provided in Article 2(12) of Council Regulation (EC) No 389/2012)",
              "type": "string"
            },
            "SIC": {
              "description": "Standard Industrial Classification (SIC) of Credential Subject (Article 3(1) of Directive 2009/101/EC of the European Parliament and of the Council.)",
              "type": "string"
            },
            "domainName": {
              "description": "Domain name  of Credential Subject",
              "type": "string"
            }
          },
          "required": ["id", "legalName"]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-vid-legal-entity-schema@1.4.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-vid-legal-entity-schema@1.4.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-vid-legal-entity-schema@1.4.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm1.1-vid-legal-entity-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSILegalEntityVerifiableID } from "@cef-ebsi/vcdm1.1-vid-legal-entity-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
