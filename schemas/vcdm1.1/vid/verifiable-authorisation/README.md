![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-vid-verifiable-authorisation-schema

> EBSI Verifiable Authorisation
>
> Schema of an EBSI Verifiable Authorisation

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xe4d55e732c117da53576dc3e4db8e76631cb125c5b44f00e4d9a2d8c44bcd3d7` (hexadecimal)
- `zGQGfG7mTVjQz7dRj5xHYzja9vJ9WTMiXNRvCAyVonjqg` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI Verifiable Authorisation",
  "description": "Schema of an EBSI Verifiable Authorisation",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines additional information about the subject that is described by the Verifiable Authorisation",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines the DID of the subject that is described by the Verifiable Attestation",
              "type": "string",
              "format": "uri"
            },
            "familyName": {
              "description": "Defines current family name(s) of the credential subject",
              "type": "string"
            },
            "firstName": {
              "description": "Defines current first name(s) of the credential subject",
              "type": "string"
            },
            "dateOfBirth": {
              "description": "Defines date of birth of the credential subject",
              "type": "string",
              "format": "date"
            },
            "personalIdentifier": {
              "description": "Defines the unique national identifier of the credential subject (constructed by the sending Member State in accordance with the technical specifications for the purposes of cross-border identification and which is as persistent as possible in time)",
              "type": "string"
            },
            "nameAndFamilyNameAtBirth": {
              "description": "Defines the first and the family name(s) of the credential subject at the time of their birth",
              "type": "string"
            },
            "placeOfBirth": {
              "description": "Defines the place where the credential subjectis born",
              "type": "string"
            },
            "currentAddress": {
              "description": "Defines the current address of the credential subject",
              "type": "string"
            },
            "gender": {
              "description": "Defines the gender of the credential subject",
              "type": "string"
            }
          },
          "required": ["id"]
        }
      }
    }
  ],
  "required": [
    "@context",
    "id",
    "type",
    "issuer",
    "issuanceDate",
    "validFrom",
    "credentialSubject",
    "credentialSchema"
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-vid-verifiable-authorisation-schema@1.3.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-vid-verifiable-authorisation-schema@1.3.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-vid-verifiable-authorisation-schema@1.3.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-vid-verifiable-authorisation-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSIVerifiableAuthorisation } from "@cef-ebsi/vcdm1.1-vid-verifiable-authorisation-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
