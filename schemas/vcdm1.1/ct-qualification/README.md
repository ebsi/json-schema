![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-ct-qualification-schema

> EBSI Conformance Testing Qualification Credential
>
> The Credential defines that the Conformance Testing was passed and qualified

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x79c454d55c8c69191988f5da9451404d1aecc6c7a064b3dc09a62e67b7b207bc` (hexadecimal)
- `z9CKy91QMGggAdwtB6T3CJBNRXWfvpzYbiPBsLDFvt1UP` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI Conformance Testing Qualification Credential",
  "description": "The Credential defines that the Conformance Testing was passed and qualified",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines additional information about the subject",
          "type": "object",
          "properties": {
            "version": {
              "description": "Completed conformance testing version number in SemVer",
              "type": "string"
            }
          },
          "required": ["version"]
        }
      },
      "required": ["credentialSubject"]
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-ct-qualification-schema@1.3.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-ct-qualification-schema@1.3.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-ct-qualification-schema@1.3.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm1.1-ct-qualification-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSIConformanceTestingQualificationCredential } from "@cef-ebsi/vcdm1.1-ct-qualification-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
