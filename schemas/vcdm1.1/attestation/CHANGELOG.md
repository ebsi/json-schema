# @cef-ebsi/vcdm1.1-attestation-schema

## 1.3.1

### Patch Changes

- c7f7fa3: Update license text.

## 1.3.0

### Minor Changes

- 191a769: Support `credentialStatus` as an array.

### Patch Changes

- 9e4aa93: Add "context" property to schema metadata.

## 1.3.0-next.0

### Minor Changes

- 191a769: Support `credentialStatus` as an array.

### Patch Changes

- 9e4aa93: Add "context" property to schema metadata.

## 1.2.0

### Minor Changes

- 021e4c2: Remove enum from `credentialSchema.type` on base Verifiable Attestation schema.

## 1.1.2

### Patch Changes

- e1e8cf3: Export examples in JS file.

## 1.1.1

### Patch Changes

- 85a6797: Embed examples in npm package, add JSON Schema to README file.

## 1.1.0

### Minor Changes

- 5cf7cbb: Update Verifiable Attestation schema, support `JsonSchema` credential schema type.

## 1.0.0

Define schema as npm package.

## Legacy versions

### 2024-01

VCDM1.1 with latest capabilities

- `issuer` can be a URI or an object
- `credentialSubject` can contain single or multiple subjects
- `proof` is removed
- `evidence` can contain single or multiple evidences

### 2022-11_01

- Improve descriptions on several fields
- modify `credentialSchema` to support single object or array of objects
  - will contain `type schema` and optionally `type extensions schema`
- add `termsOfUse`, to be used with `type extensions`
- evidence `id` is optional
- Removed `credentialStatus` StatusList2021Credential specification, as it is a type extension

### 2022-11

- Pump to json-schema 2020-12
