![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-attestation-schema

> EBSI Verifiable Attestation
>
> The schema defines a generic structure for any EBSI-related Verifiable Credentials according to the VCDM v1.1

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xbe77a21356835dc09d3d8149ea832ae0a4bae0ae9c869d18219ef8f4a74b4644` (hexadecimal)
- `zDpWGUBenmqXzurskry9Nsk6vq2R8thh9VSeoRqguoyMD` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI Verifiable Attestation",
  "description": "The schema defines a generic structure for any EBSI-related Verifiable Credentials according to the VCDM v1.1",
  "type": "object",
  "properties": {
    "@context": {
      "description": "Semantic context for the issued credential. First element MUST be https://www.w3.org/2018/credentials/v1",
      "type": "array",
      "items": {
        "type": "string",
        "format": "uri"
      },
      "contains": {
        "const": "https://www.w3.org/2018/credentials/v1"
      },
      "minItems": 1,
      "uniqueItems": true
    },
    "id": {
      "description": "Globally unique identifier for the issued credential",
      "type": "string",
      "format": "uri"
    },
    "type": {
      "description": "Full type chain, used to identify the credential base types",
      "type": "array",
      "items": {
        "type": "string"
      },
      "contains": {
        "type": "string",
        "const": "VerifiableAttestation"
      },
      "uniqueItems": true
    },
    "issuer": {
      "description": "Defines a property for expressing the issuer of a Verifiable Credential",
      "oneOf": [
        {
          "description": "DID of the credential issuer",
          "type": "string",
          "format": "uri"
        },
        {
          "type": "object",
          "required": ["id"],
          "properties": {
            "id": {
              "description": "DID of the credential issuer",
              "type": "string",
              "format": "uri"
            }
          }
        }
      ]
    },
    "issuanceDate": {
      "description": "Defines the date and time, when the issued credential becomes valid",
      "type": "string",
      "format": "date-time"
    },
    "issued": {
      "description": "Defines when the issued credential was issued",
      "type": "string",
      "format": "date-time"
    },
    "validFrom": {
      "description": "Defines the date and time, when the issued credential becomes valid",
      "type": "string",
      "format": "date-time"
    },
    "validUntil": {
      "description": "Defines the date and time, when the issued credential expires",
      "type": "string",
      "format": "date-time"
    },
    "expirationDate": {
      "description": "Defines the date and time, when the issued credential expires",
      "type": "string",
      "format": "date-time"
    },
    "credentialSubject": {
      "description": "Defines information about the subject that is defined by the type chain",
      "anyOf": [
        {
          "$ref": "#/$defs/credentialSubject"
        },
        {
          "type": "array",
          "items": {
            "$ref": "#/$defs/credentialSubject"
          }
        }
      ]
    },
    "credentialStatus": {
      "description": "Defines suspension and/or revocation details for the issued credential. Further redefined by the type extension",
      "anyOf": [
        {
          "$ref": "#/$defs/credentialStatus"
        },
        {
          "type": "array",
          "items": {
            "$ref": "#/$defs/credentialStatus"
          }
        }
      ]
    },
    "credentialSchema": {
      "description": "One or more schemas that validate the Verifiable Credential.",
      "anyOf": [
        {
          "$ref": "#/$defs/credentialSchema"
        },
        {
          "type": "array",
          "items": {
            "$ref": "#/$defs/credentialSchema"
          }
        }
      ]
    },
    "termsOfUse": {
      "description": "Contains the terms under which the issued credential was issued",
      "anyOf": [
        {
          "$ref": "#/$defs/termsOfUse"
        },
        {
          "type": "array",
          "items": {
            "$ref": "#/$defs/termsOfUse"
          }
        }
      ]
    },
    "evidence": {
      "description": "Contains the optional evidence used to issue this credential",
      "anyOf": [
        {
          "$ref": "#/$defs/evidence"
        },
        {
          "type": "array",
          "items": {
            "$ref": "#/$defs/evidence"
          }
        }
      ]
    }
  },
  "required": [
    "@context",
    "id",
    "type",
    "issuer",
    "issuanceDate",
    "issued",
    "validFrom",
    "credentialSubject",
    "credentialSchema"
  ],
  "$defs": {
    "credentialSubject": {
      "description": "Defines information about the subject that is defined by the type chain",
      "type": "object",
      "properties": {
        "id": {
          "description": "Defines the DID of the subject that is described by the issued credential",
          "type": "string",
          "format": "uri"
        }
      }
    },
    "credentialStatus": {
      "description": "Defines suspension and/or revocation details for the issued credential. Further redefined by the type extension",
      "type": "object",
      "properties": {
        "id": {
          "description": "Exact identity for the credential status",
          "type": "string",
          "format": "uri"
        },
        "type": {
          "description": "Defines the revocation type extension",
          "type": "string"
        }
      },
      "required": ["id", "type"]
    },
    "credentialSchema": {
      "description": "Contains information about the credential schema on which the issued credential is based",
      "type": "object",
      "properties": {
        "id": {
          "description": "References the credential schema stored on the Trusted Schemas Registry (TSR) on which the Verifiable Authorisation is based on",
          "type": "string",
          "format": "uri"
        },
        "type": {
          "description": "Defines credential schema type",
          "type": "string"
        }
      },
      "required": ["id", "type"]
    },
    "termsOfUse": {
      "description": "Contains the terms under which the issued credential was issued",
      "type": "object",
      "properties": {
        "id": {
          "description": "Contains a URL that points to where more information about this instance of terms of use can be found.",
          "type": "string"
        },
        "type": {
          "description": "Defines the type extension",
          "type": "string"
        }
      },
      "required": ["type"]
    },
    "evidence": {
      "type": "object",
      "properties": {
        "id": {
          "description": "If present, it SHOULD contain a URL that points to where more information about this instance of evidence can be found.",
          "type": "string"
        },
        "type": {
          "anyOf": [
            {
              "description": "Defines the evidence type extension",
              "type": "string"
            },
            {
              "description": "Defines the evidence type extension",
              "type": "array",
              "items": {
                "type": "string"
              }
            }
          ]
        }
      },
      "required": ["type"]
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-attestation-schema@1.3.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-attestation-schema@1.3.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-attestation-schema@1.3.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm1.1-attestation-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSIVerifiableAttestation } from "@cef-ebsi/vcdm1.1-attestation-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
