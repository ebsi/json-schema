![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-euipo-certificate-of-trademark-registration-schema

> EUIPO - Certificate of Trade Mark Registration for IP Register
>
> Schema of a certificate of trade mark registration

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x6e753c2f4e614abde0aa4b8bde42121622a6f3b4ee49ff9bceffd89d16ab2637` (hexadecimal)
- `z8SBXk9Zs5aG5xQV2e3oenrsPe8DYh1Xkivg3jxQ3qREe` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EUIPO - Certificate of Trade Mark Registration for IP Register",
  "description": "Schema of a certificate of trade mark registration",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines additional information about the subject that is described by the Verifiable Attestation",
          "type": "object",
          "properties": {
            "id": {
              "description": "ID of the subject",
              "type": "string"
            },
            "registrationNumber": {
              "description": "Registration number of the IP Right",
              "type": "string"
            },
            "registrationDate": {
              "type": "string",
              "description": "Date of registration of the IP Right",
              "format": "date"
            },
            "expirationDate": {
              "type": "string",
              "format": "date",
              "description": "Expiration date of the IP Right"
            },
            "applicationNumber": {
              "type": "string",
              "description": "Application number of the IP Right"
            },
            "goodsAndServices": {
              "type": "string",
              "description": "Goods and services classes of the IP Right"
            },
            "wordMark": {
              "type": "string",
              "description": "Denomination of the IP Right"
            },
            "status": {
              "type": "string",
              "description": "Status of the IP Right"
            },
            "officeCode": {
              "type": "string",
              "description": "Office code of the IP Right where it was registered"
            },
            "figurativeMark": {
              "type": "string",
              "description": "Hash of the image of the IP Right"
            },
            "applicantNameAndAddress": {
              "type": "string",
              "description": "Name and address of the applicant"
            },
            "representativeNameAndAddress": {
              "type": "string",
              "description": "Name and address of the representative"
            },
            "markFeatureType": {
              "type": "string",
              "description": "Type of the IP Right"
            }
          },
          "required": ["id"]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-euipo-certificate-of-trademark-registration-schema@1.3.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-euipo-certificate-of-trademark-registration-schema@1.3.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-euipo-certificate-of-trademark-registration-schema@1.3.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-euipo-certificate-of-trademark-registration-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EUIPOCertificateOfTradeMarkRegistrationForIPRegister } from "@cef-ebsi/vcdm1.1-euipo-certificate-of-trademark-registration-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
