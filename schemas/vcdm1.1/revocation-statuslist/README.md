![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-revocation-statuslist-schema

> EBSI StatusList2021 Credential
>
> Schema of an EBSI StatusList2021 Verifiable Credential

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x5c6bc6d5d778034f093c8aeb5f9381e3f0229d396b0f42d0ae7e14aab5856f99` (hexadecimal)
- `z7DmpwC9doRxZrtQ9PBjz8qQgGWqojkGExELnrw1x8qdi` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI StatusList2021 Credential",
  "description": "Schema of an EBSI StatusList2021 Verifiable Credential",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines information about the subject that is described by the Verifiable Attestation",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines the DID of the subject that is described by the Verifiable Attestation",
              "type": "string",
              "format": "uri"
            },
            "type": {
              "description": "Defines the Verifiable Credential subject type",
              "type": "string",
              "const": "StatusList2021"
            },
            "statusPurpose": {
              "description": "Purpose of the status entry",
              "type": "string",
              "enum": ["revocation", "suspension"]
            },
            "encodedList": {
              "description": "Encoded and compressed list of statuses",
              "type": "string"
            }
          },
          "required": ["id", "type", "statusPurpose", "encodedList"]
        }
      },
      "required": ["credentialSubject"]
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-revocation-statuslist-schema@1.3.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-revocation-statuslist-schema@1.3.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-revocation-statuslist-schema@1.3.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm1.1-revocation-statuslist-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSIStatusList2021Credential } from "@cef-ebsi/vcdm1.1-revocation-statuslist-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
