![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-idunion-poa-schema

> Power-of-Attorney
>
> Legal document that allows one person (the principal) to appoint another person (the agent or attorney-in-fact) to act on their behalf in various matters.

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x1bd1afd9a838d8c3f1679fca6478f8e05f6e0380ef29ca84c671df72142637a2` (hexadecimal)
- `z2sbTT23X2zfsdMCPFMC7GiUwyHE91Lg9BpeEA5uqtT9s` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Power-of-Attorney",
  "description": "Legal document that allows one person (the principal) to appoint another person (the agent or attorney-in-fact) to act on their behalf in various matters.",
  "type": "object",
  "allOf": [
    {
      "$ref": "node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines information about the subject that is described by the Verifiable Power-of-Attorney",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines a unique identifier of the natural person for which the Power of Attorney is issued (e.g. DID-subject)",
              "type": "string"
            },
            "poaScope": {
              "description": "Attribute that describes the range of the PoA (if unlimited. Full_PoA = True; if limited. PoA = false)",
              "type": "boolean"
            },
            "poaType": {
              "description": "A positive list of human readable areas for which the PoA is valid",
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            "familyName": {
              "description": "Defines current family name(s) of the natural person",
              "type": "string"
            },
            "firstName": {
              "description": "Defines current first name(s) of the natural person",
              "type": "string"
            },
            "dateOfBirth": {
              "description": "Defines date of birth of the natural person",
              "type": "string",
              "format": "date"
            },
            "placeOfBirth": {
              "description": "The city where the natural person was born",
              "type": "string"
            }
          },
          "required": [
            "id",
            "poaScope",
            "familyName",
            "firstName",
            "dateOfBirth",
            "placeOfBirth"
          ]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-idunion-poa-schema@1.0.2

# with Yarn
yarn add @cef-ebsi/vcdm1.1-idunion-poa-schema@1.0.2

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-idunion-poa-schema@1.0.2
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm1.1-idunion-poa-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { PowerOfAttorney } from "@cef-ebsi/vcdm1.1-idunion-poa-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
