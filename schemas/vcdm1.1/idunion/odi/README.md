![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-idunion-odi-schema

> Organization ID (ODI)
>
> Defines schema for an organization ID document

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xd53bf237f3b24b5f4aa5ea8a2c7b87b8952a97ab8c0c033b30a3f9843a8f3add` (hexadecimal)
- `zFMNrfecxyCEaLgpusf9CP5aq651BpWUV4BM9x8KXxzNY` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Organization ID (ODI)",
  "description": "Defines schema for an organization ID document",
  "type": "object",
  "allOf": [
    {
      "$ref": "node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines information about the subject that is described by the Verifiable ID",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines a unique identifier of the organization for which the ODI is issued (e.g. DID-subject)",
              "type": "string"
            },
            "legalName": {
              "title": "Legal Name",
              "description": "Legal name of the organization the ODI is issued to",
              "type": "string"
            },
            "legalForm": {
              "title": "Legal Form",
              "description": "The legal form associated to the organization",
              "type": "string"
            },
            "issuingAuthorityName": {
              "title": "Issuing authority name",
              "description": "Legal name of the organization that issued the ODI",
              "type": "string"
            },
            "issuingCountry": {
              "title": "Issuing country",
              "description": "An Alpha-2 country code as defined in ISO-3166-1",
              "type": "string"
            }
          },
          "required": [
            "id",
            "legalName",
            "legalForm",
            "issuingAuthorityName",
            "issuingCountry"
          ]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-idunion-odi-schema@1.0.1

# with Yarn
yarn add @cef-ebsi/vcdm1.1-idunion-odi-schema@1.0.1

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-idunion-odi-schema@1.0.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm1.1-idunion-odi-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { OrganizationIDODI } from "@cef-ebsi/vcdm1.1-idunion-odi-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
