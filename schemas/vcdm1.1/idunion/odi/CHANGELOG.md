# @cef-ebsi/vcdm1.1-idunion-odi-schema

## 1.0.1

### Patch Changes

- c7f7fa3: Update license text.
- Updated dependencies [c7f7fa3]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.3.1

## 1.0.0

### Major Changes

- 8760c06: Create initial version of the schema.
