# @cef-ebsi/vcdm1.1-trusted-nodes-list-schema

## 1.0.2

### Patch Changes

- e1bd474: Remove `apps` property.

## 1.0.1

### Patch Changes

- c7f7fa3: Update license text.
- Updated dependencies [c7f7fa3]
  - @cef-ebsi/vcdm1.1-attestation-schema@1.3.1

## 1.0.0

### Major Changes

- a968806: Create initial version of the EBSI Trusted Nodes List schema.
