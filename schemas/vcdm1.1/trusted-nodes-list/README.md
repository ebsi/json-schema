![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm1.1-trusted-nodes-list-schema

> EBSI Trusted Nodes List
>
> Schema of an EBSI Trusted Nodes List

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x5667f1d85b355ddca91e1f64d8811080b53dd0593abd3a31befb13fdc80e6545` (hexadecimal)
- `z6pHzVWcE9hygBq8UWRXXyR8nRst6rUXMKUkTjMoh3sfJ` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI Trusted Nodes List",
  "description": "Schema of an EBSI Trusted Nodes List",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm1.1-attestation-schema/schema.json"
    },
    {
      "properties": {
        "type": {
          "description": "Full type chain, used to identify the credential base types",
          "type": "array",
          "items": {
            "type": "string"
          },
          "contains": {
            "type": "string",
            "const": "TrustedNodesList"
          },
          "uniqueItems": true
        },
        "credentialSubject": {
          "description": "Defines additional information about the Trusted Nodes List",
          "type": "object",
          "properties": {
            "id": {
              "description": "DID of Service Ops Manager who approved the Trusted Nodes List",
              "type": "string",
              "format": "uri"
            },
            "environment": {
              "description": "Defines the environment for which the Trusted Nodes List is valid",
              "type": "string",
              "enum": ["test", "pilot", "conformance", "preprod", "prod"]
            },
            "chainId": {
              "description": "Defines the chain ID, which is part of the transaction signing process to protect against transaction replay attacks",
              "type": "number"
            },
            "version": {
              "description": "Defines the version of the Trusted Nodes List. First version MUST be 1",
              "type": "number"
            },
            "nodesTotal": {
              "description": "Defines the total number of nodes in the list",
              "type": "number"
            },
            "nodes": {
              "description": "Defines the list of nodes",
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "apis": {
                    "description": "MUST be a valid domain name starting with https://api-{environment}.ebsi. On production, \"-{environment}\" is skipped. The URL will therefore start with \"https://api.ebsi.\"",
                    "type": "string",
                    "format": "uri"
                  },
                  "explorer": {
                    "description": "MUST be a valid domain name starting with https://blockexplorer-{environment}.ebsi. On production, \"-{environment}\" is skipped. The URL will therefore start with \"https://blockexplorer.ebsi.\"",
                    "type": "string",
                    "format": "uri"
                  },
                  "country": {
                    "description": "MUST be a three-letter country code as per ISO 3166-1 alpha-3",
                    "type": "string",
                    "minLength": 3,
                    "maxLength": 3
                  }
                },
                "required": ["apis", "country"]
              }
            }
          },
          "required": [
            "id",
            "environment",
            "chainId",
            "version",
            "nodesTotal",
            "nodes"
          ]
        }
      },
      "required": ["credentialSubject", "termsOfUse"]
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm1.1-trusted-nodes-list-schema@1.0.2

# with Yarn
yarn add @cef-ebsi/vcdm1.1-trusted-nodes-list-schema@1.0.2

# with pnpm
pnpm add @cef-ebsi/vcdm1.1-trusted-nodes-list-schema@1.0.2
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm1.1-trusted-nodes-list-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSITrustedNodesList } from "@cef-ebsi/vcdm1.1-trusted-nodes-list-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
