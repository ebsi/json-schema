![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm2.0-timestamp-token-schema

> Timestamp Token
>
> Defines structure for a Timestamp Token

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x58a50eec5d3a5449fd8978528cec403724a1fc81669191ed4a73f19480773cf0` (hexadecimal)
- `z6y2rdttQJt21z5bzs8Jv6b2Ev5gvwRYdG4SMm7Uj1skj` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Timestamp Token",
  "description": "Defines structure for a Timestamp Token",
  "type": "object",
  "properties": {
    "@context": {
      "description": "Semantic context for the issued credential",
      "type": "array",
      "items": {
        "type": "string",
        "format": "uri"
      }
    },
    "id": {
      "description": "Unique identifier of the timestamp token. Serial number as defined in rfc3161.",
      "type": "string"
    },
    "type": {
      "description": "Full type chain, used to identify the credential base types",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "issuer": {
      "description": "DID which issued this credential",
      "type": "object",
      "properties": {
        "id": {
          "type": "string"
        },
        "verifiableId": {
          "description": "verifiable id or claimed information about the TSA. See TSA in RFC3161",
          "type": "object"
        }
      }
    },
    "validFrom": {
      "description": "Defines the date and time, when the issued credential becomes valid",
      "type": "string",
      "format": "date-time"
    },
    "validUntil": {
      "description": "Defines the date and time, when the issued credential expires",
      "type": "string",
      "format": "date-time"
    },
    "credentialSubject": {
      "description": "Defines information about the subject that is defined by the type chain",
      "type": "object",
      "properties": {
        "version": {
          "description": "Describes the version of the timestamp token.",
          "type": "integer"
        },
        "policy": {
          "description": "TSA policy id",
          "type": "string"
        },
        "messageImprint": {
          "description": "Multihash+Multibase value of the timestamped object",
          "type": "string"
        },
        "genTime": {
          "description": "genTime is the time at which the time-stamp token has been created by the TSA.  It is expressed as UTC time (Coordinated Universal Time) to reduce confusion with the local time zone use. GeneralizedTime syntax can include fraction-of-second details. MUST be ISO 8601 compliant",
          "type": "string",
          "format": "date-time"
        },
        "accuracy": {
          "description": "Accuracy expressed in seconds, milliseconds, and/or microseconds. Property can be omitted if the accuracy is defined by the policy.",
          "type": "object",
          "properties": {
            "seconds": {
              "type": "integer"
            },
            "millis": {
              "description": "Range: 1..999",
              "type": "integer"
            },
            "micros": {
              "description": "Range: 1..999",
              "type": "integer"
            }
          }
        },
        "ordering": {
          "description": "If the ordering field is missing, or if the ordering field is present and set to false, then the genTime field only indicates the time at which the time-stamp token has been created by the TSA.  In such a case, the ordering of time-stamp tokens issued by the same TSA or different TSAs is only possible when the difference between the genTime of the first time-stamp token and the genTime of the second time-stamp token is greater than the sum of the accuracies of the genTime for each time-stamp token.",
          "type": "boolean"
        }
      }
    },
    "credentialSchema": {
      "anyOf": [
        {
          "$ref": "#/$defs/credentialSchema"
        },
        {
          "type": "array",
          "items": {
            "$ref": "#/$defs/credentialSchema"
          }
        }
      ]
    },
    "termsOfUse": {
      "anyOf": [
        {
          "$ref": "#/$defs/termsOfUse"
        },
        {
          "type": "array",
          "items": {
            "$ref": "#/$defs/termsOfUse"
          }
        }
      ]
    }
  },
  "required": [
    "@context",
    "type",
    "issuer",
    "validFrom",
    "credentialSubject",
    "credentialSchema"
  ],
  "$defs": {
    "credentialSchema": {
      "description": "Contains information about the credential schema on which the issued credential is based",
      "type": "object",
      "properties": {
        "id": {
          "description": "References the credential schema stored on the Trusted Schemas Registry (TSR) on which the Verifiable Authorisation is based",
          "type": "string",
          "format": "uri"
        },
        "type": {
          "description": "Defines credential schema type",
          "type": "string",
          "enum": ["FullJsonSchemaValidator2021"]
        }
      },
      "required": ["id", "type"]
    },
    "termsOfUse": {
      "description": "Contains the terms under which the issued credential was issued",
      "type": "object",
      "properties": {
        "id": {
          "description": "Contains a URL that points to where more information about this instance of terms of use can be found.",
          "type": "string",
          "format": "uri"
        },
        "type": {
          "description": "Defines the type extension",
          "type": "string"
        }
      },
      "required": ["type"]
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm2.0-timestamp-token-schema@1.0.4

# with Yarn
yarn add @cef-ebsi/vcdm2.0-timestamp-token-schema@1.0.4

# with pnpm
pnpm add @cef-ebsi/vcdm2.0-timestamp-token-schema@1.0.4
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm2.0-timestamp-token-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { TimestampToken } from "@cef-ebsi/vcdm2.0-timestamp-token-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
