![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm2.0-w3id-traceability-commercial-invoice-credential-schema

> Commercial Invoice Credential
>
> The document recording a transaction between the seller and the buyer.

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x208fc6e42ffa68660bd7ad084e92b3012cb63f7d3d1b699a8c9f96f8a5cb5d14` (hexadecimal)
- `z3C7Co7tsUJuEfEDzAZEeik2mY1YsJCsnVYcNxbmQb5Uf` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Commercial Invoice Credential",
  "description": "The document recording a transaction between the seller and the buyer.",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm2.0-attestation-schema/schema.json"
    },
    {
      "properties": {
        "@context": {
          "const": [
            "https://www.w3.org/ns/credentials/v2",
            "https://w3id.org/traceability/v1"
          ],
          "default": [
            "https://www.w3.org/ns/credentials/v2",
            "https://w3id.org/traceability/v1"
          ],
          "items": {
            "enum": [
              "https://www.w3.org/ns/credentials/v2",
              "https://w3id.org/traceability/v1"
            ],
            "type": "string"
          },
          "readOnly": true,
          "type": "array"
        },
        "credentialSubject": {
          "description": "A statement of the money due for goods or services; a bill.",
          "properties": {
            "billOfLadingNumber": {
              "description": "Carrier-issued number for the associated Bill of Lading or Waybill.",
              "title": "Bill of Lading Number",
              "type": "string"
            },
            "buyer": {
              "description": "Importer of record. Party placing the order or paying the invoice.",
              "properties": {
                "email": {
                  "description": "Buyer's primary email address.",
                  "title": "Email Address",
                  "type": "string"
                },
                "id": {
                  "description": "Identifier for the buyer",
                  "title": "Identifier",
                  "type": "string"
                },
                "location": {
                  "description": "Buyer's physical location, such as address or coordinates.",
                  "properties": {
                    "address": {
                      "description": "The postal address for an organization or place.",
                      "properties": {
                        "addressCountry": {
                          "description": "The two-letter ISO 3166-1 alpha-2 country code.",
                          "title": "Address Country",
                          "type": "string"
                        },
                        "addressLocality": {
                          "description": "Text specifying the name of the locality; for example, a city.",
                          "title": "Address Locality",
                          "type": "string"
                        },
                        "addressRegion": {
                          "description": "Text specifying a province or state in abbreviated format; for example, NJ.",
                          "title": "Address Region",
                          "type": "string"
                        },
                        "postalCode": {
                          "description": "Text specifying the postal code for an address.",
                          "title": "Postal Code",
                          "type": "string"
                        },
                        "streetAddress": {
                          "description": "The street address expressed as free form text. The street address is printed on paper as the first lines below the name. For example, the name of the street and the number in the street or the name of a building.",
                          "title": "Street Address",
                          "type": "string"
                        },
                        "type": {
                          "const": ["PostalAddress"],
                          "default": ["PostalAddress"],
                          "items": {
                            "enum": ["PostalAddress"],
                            "type": "string"
                          },
                          "readOnly": true,
                          "type": "array"
                        }
                      },
                      "required": ["type"],
                      "title": "Postal Address",
                      "type": "object"
                    },
                    "type": {
                      "const": ["Place"],
                      "default": ["Place"],
                      "items": {
                        "enum": ["Place"],
                        "type": "string"
                      },
                      "readOnly": true,
                      "type": "array"
                    }
                  },
                  "required": ["type"],
                  "title": "Location",
                  "type": "object"
                },
                "name": {
                  "description": "Name of the buyer",
                  "title": "Name",
                  "type": "string"
                },
                "phoneNumber": {
                  "description": "Buyer's contact phone number.",
                  "title": "Phone Number",
                  "type": "string"
                },
                "type": {
                  "const": ["Organization"],
                  "default": ["Organization"],
                  "items": {
                    "enum": ["Organization"],
                    "type": "string"
                  },
                  "readOnly": true,
                  "type": "array"
                },
                "url": {
                  "description": "URL of the buyer",
                  "title": "URL",
                  "type": "string"
                }
              },
              "required": ["type"],
              "title": "Buyer",
              "type": "object"
            },
            "comments": {
              "description": "Free text comments.",
              "items": {
                "type": "string"
              },
              "title": "Comments",
              "type": "array"
            },
            "consignee": {
              "description": "The consignee party for this supply chain consignment.",
              "items": {
                "properties": {
                  "email": {
                    "description": "Consignee's primary email address.",
                    "title": "Email Address",
                    "type": "string"
                  },
                  "id": {
                    "description": "Identifier for the consginee.",
                    "title": "Identifier",
                    "type": "string"
                  },
                  "location": {
                    "description": "Consignee's physical location, such as address or coordinates.",
                    "properties": {
                      "address": {
                        "description": "The postal address for an organization or place.",
                        "properties": {
                          "addressCountry": {
                            "description": "The two-letter ISO 3166-1 alpha-2 country code.",
                            "title": "Address Country",
                            "type": "string"
                          },
                          "addressLocality": {
                            "description": "Text specifying the name of the locality; for example, a city.",
                            "title": "Address Locality",
                            "type": "string"
                          },
                          "addressRegion": {
                            "description": "Text specifying a province or state in abbreviated format; for example, NJ.",
                            "title": "Address Region",
                            "type": "string"
                          },
                          "postalCode": {
                            "description": "Text specifying the postal code for an address.",
                            "title": "Postal Code",
                            "type": "string"
                          },
                          "streetAddress": {
                            "description": "The street address expressed as free form text. The street address is printed on paper as the first lines below the name. For example, the name of the street and the number in the street or the name of a building.",
                            "title": "Street Address",
                            "type": "string"
                          },
                          "type": {
                            "const": ["PostalAddress"],
                            "default": ["PostalAddress"],
                            "items": {
                              "enum": ["PostalAddress"],
                              "type": "string"
                            },
                            "readOnly": true,
                            "type": "array"
                          }
                        },
                        "required": ["type"],
                        "title": "Postal Address",
                        "type": "object"
                      },
                      "type": {
                        "const": ["Place"],
                        "default": ["Place"],
                        "items": {
                          "enum": ["Place"],
                          "type": "string"
                        },
                        "readOnly": true,
                        "type": "array"
                      }
                    },
                    "required": ["type"],
                    "title": "Location",
                    "type": "object"
                  },
                  "name": {
                    "description": "Name of the consginee.",
                    "title": "Name",
                    "type": "string"
                  },
                  "phoneNumber": {
                    "description": "Consignee's contact phone number.",
                    "title": "Phone Number",
                    "type": "string"
                  },
                  "type": {
                    "const": ["Organization"],
                    "default": ["Organization"],
                    "items": {
                      "enum": ["Organization"],
                      "type": "string"
                    },
                    "readOnly": true,
                    "type": "array"
                  },
                  "url": {
                    "description": "URL of the consignee.",
                    "title": "URL",
                    "type": "string"
                  }
                },
                "required": ["type"],
                "type": "object"
              },
              "maxItems": 1,
              "minItems": 0,
              "title": "Consignee",
              "type": "array"
            },
            "currencyOfSettlement": {
              "description": "Currency agreed upon between seller and buyer as payment.",
              "title": "Terms of Settlement",
              "type": "string"
            },
            "customerReferenceNumber": {
              "description": "Oversees customerâ€™s reference number.",
              "title": "Customer's Reference Number",
              "type": "string"
            },
            "deductions": {
              "description": "Applicable additions.",
              "items": {
                "properties": {
                  "price": {
                    "description": "The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.",
                    "title": "Price",
                    "type": "number"
                  },
                  "priceCurrency": {
                    "description": "The currency of the price, or a price component when attached to PriceSpecification and its subtypes.",
                    "title": "Price Currency",
                    "type": "string"
                  },
                  "type": {
                    "const": ["PriceSpecification"],
                    "default": ["PriceSpecification"],
                    "items": {
                      "enum": ["PriceSpecification"],
                      "type": "string"
                    },
                    "readOnly": true,
                    "type": "array"
                  }
                },
                "required": ["type"],
                "type": "object"
              },
              "title": "Additions",
              "type": "array"
            },
            "destinationCountry": {
              "description": "Country to which the purchased product will be delivered",
              "title": "Destination Country",
              "type": "string"
            },
            "discounts": {
              "description": "Applicable discounts.",
              "items": {
                "properties": {
                  "price": {
                    "description": "The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.",
                    "title": "Price",
                    "type": "number"
                  },
                  "priceCurrency": {
                    "description": "The currency of the price, or a price component when attached to PriceSpecification and its subtypes.",
                    "title": "Price Currency",
                    "type": "string"
                  },
                  "type": {
                    "const": ["PriceSpecification"],
                    "default": ["PriceSpecification"],
                    "items": {
                      "enum": ["PriceSpecification"],
                      "type": "string"
                    },
                    "readOnly": true,
                    "type": "array"
                  }
                },
                "required": ["type"],
                "type": "object"
              },
              "title": "Discounts",
              "type": "array"
            },
            "freightCost": {
              "description": "Included cost of freight.",
              "properties": {
                "price": {
                  "description": "The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.",
                  "title": "Price",
                  "type": "number"
                },
                "priceCurrency": {
                  "description": "The currency of the price, or a price component when attached to PriceSpecification and its subtypes.",
                  "title": "Price Currency",
                  "type": "string"
                },
                "type": {
                  "const": ["PriceSpecification"],
                  "default": ["PriceSpecification"],
                  "items": {
                    "enum": ["PriceSpecification"],
                    "type": "string"
                  },
                  "readOnly": true,
                  "type": "array"
                }
              },
              "required": ["type"],
              "title": "Freight Cost",
              "type": "object"
            },
            "identifier": {
              "description": "Identifiers for a property.",
              "title": "Property Identifier",
              "type": "string"
            },
            "insuranceCost": {
              "description": "Included cost of insurance.",
              "properties": {
                "price": {
                  "description": "The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.",
                  "title": "Price",
                  "type": "number"
                },
                "priceCurrency": {
                  "description": "The currency of the price, or a price component when attached to PriceSpecification and its subtypes.",
                  "title": "Price Currency",
                  "type": "string"
                },
                "type": {
                  "const": ["PriceSpecification"],
                  "default": ["PriceSpecification"],
                  "items": {
                    "enum": ["PriceSpecification"],
                    "type": "string"
                  },
                  "readOnly": true,
                  "type": "array"
                }
              },
              "required": ["type"],
              "title": "Freight Cost",
              "type": "object"
            },
            "invoiceDate": {
              "description": "A date, time, date time, or other date time value of the invoice in the trade settlement.",
              "title": "Invoice Date",
              "type": "string"
            },
            "invoiceNumber": {
              "description": "Invoice Number.",
              "title": "Invoice Number",
              "type": "string"
            },
            "invoiceSubtotal": {
              "description": "The subtotal of line items.",
              "properties": {
                "price": {
                  "description": "The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.",
                  "title": "Price",
                  "type": "number"
                },
                "priceCurrency": {
                  "description": "The currency of the price, or a price component when attached to PriceSpecification and its subtypes.",
                  "title": "Price Currency",
                  "type": "string"
                },
                "type": {
                  "const": ["PriceSpecification"],
                  "default": ["PriceSpecification"],
                  "items": {
                    "enum": ["PriceSpecification"],
                    "type": "string"
                  },
                  "readOnly": true,
                  "type": "array"
                }
              },
              "required": ["type"],
              "title": "Invoice Subtotal",
              "type": "object"
            },
            "isProforma": {
              "description": "Indicates if this credential is a Pro Forma Invoice, or a normal invoice.",
              "title": "is Pro Forma",
              "type": "boolean"
            },
            "itemsShipped": {
              "description": "Itemized list of shipped goods.",
              "items": {
                "properties": {
                  "countryOfOrigin": {
                    "description": "Identify the 2-digit ISO country code of the country of production.",
                    "title": "Country of Origin",
                    "type": "string"
                  },
                  "deemedResellerCategory": {
                    "description": "The category of deemed reseller. This applies to selling partners that are not based in the EU and is used to help them meet the VAT Deemed Reseller tax laws in the EU and UK.",
                    "enum": ["IOSS", "UOSS"],
                    "title": "DeemedResellerCategory",
                    "type": "string"
                  },
                  "description": {
                    "title": "Description of product",
                    "type": "string"
                  },
                  "freeTradeAgreement": {
                    "description": "Relevant Free Trade Agreement",
                    "title": "Free Trade Agreement",
                    "type": "string"
                  },
                  "grossWeight": {
                    "description": "A measure of the gross weight (mass) which includes packaging but excludes any transport equipment.",
                    "properties": {
                      "type": {
                        "const": ["QuantitativeValue"],
                        "default": ["QuantitativeValue"],
                        "items": {
                          "enum": ["QuantitativeValue"],
                          "type": "string"
                        },
                        "readOnly": true,
                        "type": "array"
                      },
                      "unitCode": {
                        "description": "Unit of measurement.",
                        "title": "Unit Code",
                        "type": "string"
                      },
                      "value": {
                        "description": "Value or amount.",
                        "title": "Value",
                        "type": "string"
                      }
                    },
                    "required": ["type"],
                    "title": "Trade Line Item Gross Weight",
                    "type": "object"
                  },
                  "ioss": {
                    "description": "The IOSS number for the marketplace. Sellers shipping to the European Union (EU) from outside of the EU must provide this IOSS number to their carrier when Amazon has collected the VAT on the sale.",
                    "title": "IOSS",
                    "type": "string"
                  },
                  "itemCount": {
                    "description": "A quantity despatched in the trade delivery.",
                    "title": "Item Count",
                    "type": "number"
                  },
                  "lineItemFreightCost": {
                    "description": "Included cost of freight.",
                    "properties": {
                      "price": {
                        "description": "The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.",
                        "title": "Price",
                        "type": "number"
                      },
                      "priceCurrency": {
                        "description": "The currency of the price, or a price component when attached to PriceSpecification and its subtypes.",
                        "title": "Price Currency",
                        "type": "string"
                      },
                      "type": {
                        "const": ["PriceSpecification"],
                        "default": ["PriceSpecification"],
                        "items": {
                          "enum": ["PriceSpecification"],
                          "type": "string"
                        },
                        "readOnly": true,
                        "type": "array"
                      }
                    },
                    "required": ["type"],
                    "title": "Freight Cost",
                    "type": "object"
                  },
                  "lineItemInsuranceCost": {
                    "description": "Included cost of insurance.",
                    "properties": {
                      "price": {
                        "description": "The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.",
                        "title": "Price",
                        "type": "number"
                      },
                      "priceCurrency": {
                        "description": "The currency of the price, or a price component when attached to PriceSpecification and its subtypes.",
                        "title": "Price Currency",
                        "type": "string"
                      },
                      "type": {
                        "const": ["PriceSpecification"],
                        "default": ["PriceSpecification"],
                        "items": {
                          "enum": ["PriceSpecification"],
                          "type": "string"
                        },
                        "readOnly": true,
                        "type": "array"
                      }
                    },
                    "required": ["type"],
                    "title": "Insurance Cost",
                    "type": "object"
                  },
                  "lineItemTax": {
                    "description": "Applicable tax.",
                    "properties": {
                      "price": {
                        "description": "The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.",
                        "title": "Price",
                        "type": "number"
                      },
                      "priceCurrency": {
                        "description": "The currency of the price, or a price component when attached to PriceSpecification and its subtypes.",
                        "title": "Price Currency",
                        "type": "string"
                      },
                      "type": {
                        "const": ["PriceSpecification"],
                        "default": ["PriceSpecification"],
                        "items": {
                          "enum": ["PriceSpecification"],
                          "type": "string"
                        },
                        "readOnly": true,
                        "type": "array"
                      }
                    },
                    "required": ["type"],
                    "title": "Tax",
                    "type": "object"
                  },
                  "lineItemTotalPrice": {
                    "description": "Total line item price.",
                    "properties": {
                      "price": {
                        "description": "The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.",
                        "title": "Price",
                        "type": "number"
                      },
                      "priceCurrency": {
                        "description": "The currency of the price, or a price component when attached to PriceSpecification and its subtypes.",
                        "title": "Price Currency",
                        "type": "string"
                      },
                      "type": {
                        "const": ["PriceSpecification"],
                        "default": ["PriceSpecification"],
                        "items": {
                          "enum": ["PriceSpecification"],
                          "type": "string"
                        },
                        "readOnly": true,
                        "type": "array"
                      }
                    },
                    "required": ["type"],
                    "title": "Total Price",
                    "type": "object"
                  },
                  "name": {
                    "description": "Name of the Tradeline item.",
                    "title": "Name",
                    "type": "string"
                  },
                  "netWeight": {
                    "description": "A measure of the net weight (mass).",
                    "properties": {
                      "type": {
                        "const": ["QuantitativeValue"],
                        "default": ["QuantitativeValue"],
                        "items": {
                          "enum": ["QuantitativeValue"],
                          "type": "string"
                        },
                        "readOnly": true,
                        "type": "array"
                      },
                      "unitCode": {
                        "description": "Unit of measurement.",
                        "title": "Unit Code",
                        "type": "string"
                      },
                      "value": {
                        "description": "Value or amount.",
                        "title": "Value",
                        "type": "string"
                      }
                    },
                    "required": ["type"],
                    "title": "Trade Line Item Net Weight",
                    "type": "object"
                  },
                  "product": {
                    "description": "Product details.",
                    "properties": {
                      "batchNumber": {
                        "description": "A tracking number for commodities",
                        "title": "Batch Number",
                        "type": "string"
                      },
                      "category": {
                        "description": "A category for the item.",
                        "title": "Category",
                        "type": "string"
                      },
                      "commodity": {
                        "description": "Product commodity code, codification system and description",
                        "properties": {
                          "commodityCode": {
                            "description": "The commodity code at a given granularity, commonly a 6-digit HS or a 10-digit HTS code.",
                            "title": "Commodity Code",
                            "type": "string"
                          },
                          "commodityCodeType": {
                            "description": "Commodity code type",
                            "enum": ["HS", "HTS"],
                            "title": "Commodity Code Type"
                          },
                          "description": {
                            "description": "Description of the commodity.",
                            "title": "Commodity Description",
                            "type": "string"
                          },
                          "type": {
                            "const": ["Commodity"],
                            "default": ["Commodity"],
                            "items": {
                              "enum": ["Commodity"],
                              "type": "string"
                            },
                            "readOnly": true,
                            "type": "array"
                          }
                        },
                        "required": ["type"],
                        "title": "Commodity",
                        "type": "object"
                      },
                      "description": {
                        "description": "Product description.",
                        "title": "Description",
                        "type": "string"
                      },
                      "gtin": {
                        "title": "Global Trade Item Number (GTIN)",
                        "type": "string"
                      },
                      "id": {
                        "description": "The product identifier, such as ISBN.",
                        "title": "Product Identifier",
                        "type": "string"
                      },
                      "manufacturer": {
                        "description": "The manufacturer of the product.",
                        "properties": {
                          "id": {
                            "description": "Identifier for the buyer",
                            "title": "Identifier",
                            "type": "string"
                          },
                          "name": {
                            "description": "Name of the buyer",
                            "title": "Name",
                            "type": "string"
                          },
                          "type": {
                            "const": ["Organization"],
                            "default": ["Organization"],
                            "items": {
                              "enum": ["Organization"],
                              "type": "string"
                            },
                            "readOnly": true,
                            "type": "array"
                          }
                        },
                        "required": ["type"],
                        "title": "Manufacturer",
                        "type": "object"
                      },
                      "name": {
                        "description": "Product name.",
                        "title": "Name",
                        "type": "string"
                      },
                      "productPrice": {
                        "description": "One or more detailed price specifications, indicating the unit price and delivery or payment charges.",
                        "properties": {
                          "price": {
                            "description": "The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.",
                            "title": "Price",
                            "type": "number"
                          },
                          "priceCurrency": {
                            "description": "The currency of the price, or a price component when attached to PriceSpecification and its subtypes.",
                            "title": "Price Currency",
                            "type": "string"
                          },
                          "type": {
                            "const": ["PriceSpecification"],
                            "default": ["PriceSpecification"],
                            "items": {
                              "enum": ["PriceSpecification"],
                              "type": "string"
                            },
                            "readOnly": true,
                            "type": "array"
                          }
                        },
                        "required": ["type"],
                        "title": "Product Price",
                        "type": "object"
                      },
                      "sku": {
                        "description": "The Stock Keeping Unit (SKU), i.e., a merchant-specific identifier for a product or service, or for the product to which the offer refers.",
                        "title": "SKU Number",
                        "type": "string"
                      },
                      "type": {
                        "const": ["Product"],
                        "default": ["Product"],
                        "items": {
                          "enum": ["Product"],
                          "type": "string"
                        },
                        "readOnly": true,
                        "type": "array"
                      },
                      "weight": {
                        "description": "Weight of the product.",
                        "properties": {
                          "type": {
                            "const": ["QuantitativeValue"],
                            "default": ["QuantitativeValue"],
                            "items": {
                              "enum": ["QuantitativeValue"],
                              "type": "string"
                            },
                            "readOnly": true,
                            "type": "array"
                          },
                          "unitCode": {
                            "description": "Unit of measurement.",
                            "title": "Unit Code",
                            "type": "string"
                          },
                          "value": {
                            "description": "Value or amount.",
                            "title": "Value",
                            "type": "string"
                          }
                        },
                        "required": ["type"],
                        "title": "Weight",
                        "type": "object"
                      }
                    },
                    "required": ["type"],
                    "title": "Product",
                    "type": "object"
                  },
                  "type": {
                    "const": ["TradeLineItem"],
                    "default": ["TradeLineItem"],
                    "items": {
                      "enum": ["TradeLineItem"],
                      "type": "string"
                    },
                    "readOnly": true,
                    "type": "array"
                  }
                },
                "required": ["type"],
                "type": "object"
              },
              "minItems": 1,
              "title": "Items Shipped",
              "type": "array"
            },
            "letterOfCreditNumber": {
              "description": "A letter of credit document referenced in the trade agreement or settlement.",
              "title": "Letter of Credit Number",
              "type": "string"
            },
            "originCountry": {
              "description": "A country of origin for the consignment, consignment item, or product.",
              "title": "Origin Country",
              "type": "string"
            },
            "packageQuantity": {
              "description": "Total number of packages.",
              "title": "Package Quantity",
              "type": "number"
            },
            "portOfEntry": {
              "description": "Port where the purchased goods will enter first.",
              "properties": {
                "iataAirportCode": {
                  "description": "IATA airport code (3 letter)",
                  "title": "IATA Airport Code",
                  "type": "string"
                },
                "type": {
                  "const": ["Place"],
                  "default": ["Place"],
                  "items": {
                    "enum": ["Place"],
                    "type": "string"
                  },
                  "readOnly": true,
                  "type": "array"
                },
                "unLocode": {
                  "description": "LOCODE",
                  "title": "UN Locode",
                  "type": "string"
                }
              },
              "required": ["type"],
              "title": "Port Of Entry",
              "type": "object"
            },
            "purchaseDate": {
              "description": "The date that payment is made.",
              "title": "Purchase Date",
              "type": "string"
            },
            "purchaseOrderNumbers": {
              "description": "The Order(s) related to this Invoice. One or more Orders may be combined into a single Invoice.",
              "items": {
                "type": "string"
              },
              "title": "Purchase Order Numbers",
              "type": "array"
            },
            "seller": {
              "description": "An entity which offers (sells, leases, lends, or loans) the services or goods. A seller may also be a provider.",
              "properties": {
                "email": {
                  "description": "Seller's primary email address.",
                  "title": "Email Address",
                  "type": "string"
                },
                "id": {
                  "description": "Identifier for the seller",
                  "title": "Identifier",
                  "type": "string"
                },
                "location": {
                  "description": "Seller's physical location, such as address or coordinates.",
                  "properties": {
                    "address": {
                      "description": "The postal address for an organization or place.",
                      "properties": {
                        "addressCountry": {
                          "description": "The two-letter ISO 3166-1 alpha-2 country code.",
                          "title": "Address Country",
                          "type": "string"
                        },
                        "addressLocality": {
                          "description": "Text specifying the name of the locality; for example, a city.",
                          "title": "Address Locality",
                          "type": "string"
                        },
                        "addressRegion": {
                          "description": "Text specifying a province or state in abbreviated format; for example, NJ.",
                          "title": "Address Region",
                          "type": "string"
                        },
                        "postalCode": {
                          "description": "Text specifying the postal code for an address.",
                          "title": "Postal Code",
                          "type": "string"
                        },
                        "streetAddress": {
                          "description": "The street address expressed as free form text. The street address is printed on paper as the first lines below the name. For example, the name of the street and the number in the street or the name of a building.",
                          "title": "Street Address",
                          "type": "string"
                        },
                        "type": {
                          "const": ["PostalAddress"],
                          "default": ["PostalAddress"],
                          "items": {
                            "enum": ["PostalAddress"],
                            "type": "string"
                          },
                          "readOnly": true,
                          "type": "array"
                        }
                      },
                      "required": ["type"],
                      "title": "Postal Address",
                      "type": "object"
                    },
                    "type": {
                      "const": ["Place"],
                      "default": ["Place"],
                      "items": {
                        "enum": ["Place"],
                        "type": "string"
                      },
                      "readOnly": true,
                      "type": "array"
                    }
                  },
                  "required": ["type"],
                  "title": "Location",
                  "type": "object"
                },
                "name": {
                  "description": "Name of the seller",
                  "title": "Name",
                  "type": "string"
                },
                "phoneNumber": {
                  "description": "Seller's contact phone number.",
                  "title": "Phone Number",
                  "type": "string"
                },
                "type": {
                  "const": ["Organization"],
                  "default": ["Organization"],
                  "items": {
                    "enum": ["Organization"],
                    "type": "string"
                  },
                  "readOnly": true,
                  "type": "array"
                },
                "url": {
                  "description": "URL of the seller",
                  "title": "URL",
                  "type": "string"
                }
              },
              "required": ["type"],
              "title": "Seller",
              "type": "object"
            },
            "tax": {
              "description": "Applicable tax.",
              "properties": {
                "price": {
                  "description": "The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.",
                  "title": "Price",
                  "type": "number"
                },
                "priceCurrency": {
                  "description": "The currency of the price, or a price component when attached to PriceSpecification and its subtypes.",
                  "title": "Price Currency",
                  "type": "string"
                },
                "type": {
                  "const": ["PriceSpecification"],
                  "default": ["PriceSpecification"],
                  "items": {
                    "enum": ["PriceSpecification"],
                    "type": "string"
                  },
                  "readOnly": true,
                  "type": "array"
                }
              },
              "required": ["type"],
              "title": "Tax",
              "type": "object"
            },
            "termsOfDelivery": {
              "description": "The conditions agreed upon between the parties with regard to the delivery of goods and or services.",
              "title": "Terms of Delivery",
              "type": "string"
            },
            "termsOfPayment": {
              "description": "Terms, conditions, and currency of settlement, as agreed upon by the vendor and purchaser per the pro forma invoice, customer purchase order, and/or the letter of credit.",
              "title": "Terms of Payment",
              "type": "string"
            },
            "totalPaymentDue": {
              "description": "The total amount due.",
              "properties": {
                "price": {
                  "description": "The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.",
                  "title": "Price",
                  "type": "number"
                },
                "priceCurrency": {
                  "description": "The currency of the price, or a price component when attached to PriceSpecification and its subtypes.",
                  "title": "Price Currency",
                  "type": "string"
                },
                "type": {
                  "const": ["PriceSpecification"],
                  "default": ["PriceSpecification"],
                  "items": {
                    "enum": ["PriceSpecification"],
                    "type": "string"
                  },
                  "readOnly": true,
                  "type": "array"
                }
              },
              "required": ["type", "price", "priceCurrency"],
              "title": "Total Payment Due",
              "type": "object"
            },
            "totalWeight": {
              "description": "Total weight of the products.",
              "properties": {
                "type": {
                  "const": ["QuantitativeValue"],
                  "default": ["QuantitativeValue"],
                  "items": {
                    "enum": ["QuantitativeValue"],
                    "type": "string"
                  },
                  "readOnly": true,
                  "type": "array"
                },
                "unitCode": {
                  "description": "Unit of measurement.",
                  "title": "Unit Code",
                  "type": "string"
                },
                "value": {
                  "description": "Value or amount.",
                  "title": "Value",
                  "type": "string"
                }
              },
              "required": ["type"],
              "title": "Total Weight",
              "type": "object"
            },
            "type": {
              "const": ["Invoice"],
              "default": ["Invoice"],
              "items": {
                "enum": ["Invoice"],
                "type": "string"
              },
              "readOnly": true,
              "type": "array"
            }
          },
          "required": [
            "type",
            "invoiceNumber",
            "seller",
            "buyer",
            "itemsShipped",
            "totalPaymentDue"
          ],
          "title": "Invoice",
          "type": "object"
        },
        "proof": {
          "description": "A JSON Web Signature proof for a credential as defined by the VC data model",
          "properties": {
            "created": {
              "description": "Creation timestamp for the proof in the form of an XML datestring",
              "type": "string"
            },
            "jws": {
              "description": "The JSON Web Signature for the proof",
              "type": "string"
            },
            "proofPurpose": {
              "const": "assertionMethod",
              "description": "In the case of credentials, the proof should be the constant, 'assertionMethod'"
            },
            "type": {
              "description": "Signature suite for the proof",
              "enum": ["Ed25519Signature2018"],
              "type": "string"
            },
            "verificationMethod": {
              "description": "The fragment from which the public key can be de-referenced, in the form of a URI",
              "type": "string"
            }
          },
          "title": "proof",
          "type": "object"
        },
        "type": {
          "const": ["VerifiableCredential", "CommercialInvoiceCredential"],
          "default": ["VerifiableCredential", "CommercialInvoiceCredential"],
          "items": {
            "enum": ["VerifiableCredential", "CommercialInvoiceCredential"],
            "type": "string"
          },
          "readOnly": true,
          "type": "array"
        }
      },
      "title": "Commercial Invoice Credential",
      "type": "object"
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm2.0-w3id-traceability-commercial-invoice-credential-schema@1.2.1

# with Yarn
yarn add @cef-ebsi/vcdm2.0-w3id-traceability-commercial-invoice-credential-schema@1.2.1

# with pnpm
pnpm add @cef-ebsi/vcdm2.0-w3id-traceability-commercial-invoice-credential-schema@1.2.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm2.0-w3id-traceability-commercial-invoice-credential-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { CommercialInvoiceCredential } from "@cef-ebsi/vcdm2.0-w3id-traceability-commercial-invoice-credential-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
