# @cef-ebsi/vcdm2.0-vid-natural-person-schema

## 3.0.0

### Major Changes

- eb64397: Refactor schema properties, align identifiers with https://eur-lex.europa.eu/eli/reg_impl/2024/2977/oj.

## 2.0.0

### Major Changes

- 431fb17: Align PID schema with the latest specification approved in the Implementing Act.

## 1.0.4

### Patch Changes

- c7f7fa3: Update license text.

## 1.0.3

### Patch Changes

- 9e4aa93: Add "context" property to schema metadata.

## 1.0.3-next.0

### Patch Changes

- 9e4aa93: Add "context" property to schema metadata.

## 1.0.2

### Patch Changes

- e1e8cf3: Export examples in JS file.

## 1.0.1

### Patch Changes

- 85a6797: Embed examples in npm package, add JSON Schema to README file.

## 1.0.0

Define schema as npm package.

## Legacy versions

### 2023-08

- Initial schema
