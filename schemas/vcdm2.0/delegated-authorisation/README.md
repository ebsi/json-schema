![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm2.0-delegated-authorisation-schema

> EBSI Verifiable Delegated Authorisation
>
> The schema defines a delegated Verifiable Authorisation

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xa75fe7558f37097d1be4755ae20e75a343051ea516bb0d46ca38650ba0b6df6c` (hexadecimal)
- `zCGMuPN76TkEtuVBQ3nRsHbPCvgaT3rUYaJbH5GwsgoAs` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI Verifiable Delegated Authorisation",
  "description": "The schema defines a delegated Verifiable Authorisation",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm2.0-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "$ref": "#/$defs/credentialSubject"
        },

        "evidence": {
          "$ref": "./node_modules/@cef-ebsi/vcdm2.0-type-extensions-evidence-authorisation-delegation-schema/schema.json#/properties/evidence"
        }
      }
    }
  ],
  "required": [
    "@context",
    "id",
    "type",
    "issuer",
    "validFrom",
    "validUntil",
    "credentialSubject",
    "credentialSchema"
  ],
  "$defs": {
    "permission": {
      "description": "Permission instance",
      "type": "object",
      "properties": {
        "scope": {
          "description": "Scope for which the claims are being defined. Authorisation server MUST understand the scope.",
          "type": "string"
        },
        "claims": {
          "description": "One or more permission claims for the given scope. If authorisation server is unable to process the claims, access MUST be rejected.",
          "oneOf": [
            {
              "type": "array",
              "items": {
                "type": "string"
              }
            },
            {
              "type": "array",
              "items": {
                "type": "object"
              }
            }
          ]
        }
      },
      "required": ["scope"]
    },
    "credentialSubject": {
      "description": "Defines information about the subject that is defined by the type chain",
      "type": "object",
      "properties": {
        "id": {
          "description": "Defines the DID of the subject that is described by the issued credential",
          "type": "string",
          "format": "uri"
        },
        "permissions": {
          "oneOf": [
            {
              "$ref": "#/$defs/permission"
            },
            {
              "type": "array",
              "items": {
                "$ref": "#/$defs/permission"
              }
            }
          ]
        },
        "maxDelegationCount": {
          "description": "CONDITIONAL. MUST be present if isDelegate == true. Defines the possible delegation count from the top-level authorisation.",
          "type": "integer"
        },
        "isDelegated": {
          "description": "OPTIONAL. Defines whether the authorisation is delegated.",
          "type": "boolean",
          "default": "false"
        },
        "evidenceId": {
          "description": "CONDITIONAL. If delegationPolicy is present and if there are more than one evidences in the evidence property, the evidenceId is used to identify the evidence based on which this delegated authorisation VC has been issued.",
          "type": "string"
        }
      }
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm2.0-delegated-authorisation-schema@1.2.1

# with Yarn
yarn add @cef-ebsi/vcdm2.0-delegated-authorisation-schema@1.2.1

# with pnpm
pnpm add @cef-ebsi/vcdm2.0-delegated-authorisation-schema@1.2.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm2.0-delegated-authorisation-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSIVerifiableDelegatedAuthorisation } from "@cef-ebsi/vcdm2.0-delegated-authorisation-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
