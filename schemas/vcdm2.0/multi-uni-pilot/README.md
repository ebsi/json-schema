# EBSI Multi Uni Pilot

Schemas for:

- [Verifiable Employment Attestation](./employment-attestation)
- [Verifiable Work Certificate](./work-certificate)
