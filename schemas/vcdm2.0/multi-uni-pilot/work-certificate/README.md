![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm2.0-multi-uni-pilot-work-certificate-schema

> Verifiable Work Certificate
>
> Schema of Work Certificate for a natural persons.

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x4fc40c22e5a3db9849d4412fb737141c2c37e38ecde23d9984c7989f332dac79` (hexadecimal)
- `z6NNbRM5SoGa4cbULzaXqCKYWUWoKmkYsmVcrBD8zqvjJ` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Verifiable Work Certificate",
  "description": "Schema of Work Certificate for a natural persons.",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm2.0-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines additional information about the subject that is described by the Verifiable Certificate of Work",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines the DID of the subject that is described by the Verifiable Attestation",
              "type": "string",
              "format": "uri"
            },
            "familyName": {
              "description": "Defines current family name(s) of the credential subject",
              "type": "string"
            },
            "firstName": {
              "description": "Defines current first name(s) of the credential subject",
              "type": "string"
            },
            "startDate": {
              "description": "Defines date of entry in company of the credential subject",
              "type": "string",
              "format": "date-time"
            },
            "endDate": {
              "description": "Defines date of leaving the company of the credential subject",
              "type": "string",
              "format": "date-time"
            },
            "listOfOccupations": {
              "description": "Defines the list of occupations held in the company of the credential subject",
              "type": "array",
              "items": {
                "type": "object",
                "required": ["occupationalCategory", "startDate"],
                "properties": {
                  "occupationalCategory": {
                    "description": "Defines the function held in the company of the credential subject. Recommendation: see Occupational Category: https://schema.org/occupationalCategory",
                    "type": "string"
                  },
                  "startDate": {
                    "description": "Defines start date of employment held in company of the credential subject",
                    "type": "string",
                    "format": "date-time"
                  },
                  "endDate": {
                    "description": "Defines end date of employment held in company of the credential subject",
                    "type": "string",
                    "format": "date-time"
                  }
                }
              }
            },
            "socialBenefits": {
              "description": "Defines the prorogued benefits of the credential subject",
              "type": "string"
            }
          },
          "required": [
            "id",
            "familyName",
            "firstName",
            "startDate",
            "listOfOccupations"
          ]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm2.0-multi-uni-pilot-work-certificate-schema@1.2.1

# with Yarn
yarn add @cef-ebsi/vcdm2.0-multi-uni-pilot-work-certificate-schema@1.2.1

# with pnpm
pnpm add @cef-ebsi/vcdm2.0-multi-uni-pilot-work-certificate-schema@1.2.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm2.0-multi-uni-pilot-work-certificate-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { VerifiableWorkCertificate } from "@cef-ebsi/vcdm2.0-multi-uni-pilot-work-certificate-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
