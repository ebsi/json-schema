![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm2.0-multi-uni-pilot-employment-attestation-schema

> Verifiable Employment Attestation
>
> Schema of Employment Attestations for a natural persons.

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x7dcf6877f9de227cf91b1f78adf5a1f16e4833d5791bef78c81b658ac650aac6` (hexadecimal)
- `z9U7PuhydaTdw3abXAckZZ6yeJQ9521ePxXzaxeHb4qtZ` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Verifiable Employment Attestation",
  "description": "Schema of Employment Attestations for a natural persons.",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm2.0-attestation-schema/schema.json"
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Defines additional information about the subject that is described by the Verifiable Certificate of Work",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines the DID of the subject that is described by the Verifiable Attestation",
              "type": "string",
              "format": "uri"
            },
            "familyName": {
              "description": "Defines current family name(s) of the credential subject",
              "type": "string"
            },
            "firstName": {
              "description": "Defines current first name(s) of the credential subject",
              "type": "string"
            },
            "startDate": {
              "description": "Defines date of entry in company of the credential subject",
              "type": "string",
              "format": "date-time"
            },
            "occupationalCategory": {
              "description": "Defines the function held in the company of the credential subject. Recommendation: see Occupational Category: https://schema.org/occupationalCategory",
              "type": "string"
            }
          },
          "required": [
            "id",
            "familyName",
            "firstName",
            "startDate",
            "occupationalCategory"
          ]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm2.0-multi-uni-pilot-employment-attestation-schema@1.2.1

# with Yarn
yarn add @cef-ebsi/vcdm2.0-multi-uni-pilot-employment-attestation-schema@1.2.1

# with pnpm
pnpm add @cef-ebsi/vcdm2.0-multi-uni-pilot-employment-attestation-schema@1.2.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm2.0-multi-uni-pilot-employment-attestation-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { VerifiableEmploymentAttestation } from "@cef-ebsi/vcdm2.0-multi-uni-pilot-employment-attestation-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
