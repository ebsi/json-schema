![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm2.0-presentation-schema

> EBSI Verifiable Presentation
>
> Schema of an EBSI Verifiable Presentation

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xa87e3ab0b8fb5c37063b257b750e7d776522c7384fedd4d5b13ced1b86ec3026` (hexadecimal)
- `zCLj8bkgAVPZ5Bxpu5W43vFvztYhUEe6YbxxCiCWLXWX7` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EBSI Verifiable Presentation",
  "description": "Schema of an EBSI Verifiable Presentation",
  "type": "object",
  "properties": {
    "@context": {
      "description": "Defines semantic context of the Verifiable Presentation. First element MUST be https://www.w3.org/ns/credentials/v2",
      "type": "array",
      "items": {
        "type": "string",
        "format": "uri"
      },
      "contains": {
        "const": "https://www.w3.org/ns/credentials/v2"
      },
      "minItems": 1,
      "uniqueItems": true
    },
    "id": {
      "description": "Globally unique identifier for the Verifiable Presentation. It can be a UUID or another globally unique identifier.",
      "type": "string",
      "format": "uri"
    },
    "type": {
      "description": "Defines the Verifiable Presentation type",
      "type": "array",
      "items": {
        "type": "string"
      },
      "contains": {
        "const": "VerifiablePresentation"
      },
      "minItems": 1,
      "uniqueItems": true
    },
    "holder": {
      "oneOf": [
        {
          "description": "Defines unique identifier of the party who shares the Verifiable Presentation",
          "type": "string",
          "format": "uri"
        },
        {
          "type": "object",
          "required": ["id"],
          "properties": {
            "id": {
              "description": "Defines unique identifier of the party who shares the Verifiable Presentation",
              "type": "string",
              "format": "uri"
            }
          }
        }
      ]
    },
    "verifiableCredential": {
      "description": "Contains the personal information intended to be shared",
      "type": "array",
      "items": {
        "oneOf": [{ "type": "object" }, { "type": "string" }]
      }
    },
    "relatedResource": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "id": {
            "description": "URL the integrity information is for",
            "type": "string",
            "format": "uri"
          },
          "mediaType": {
            "description": "Expected media type for the indicated resource",
            "type": "string"
          },
          "digestSRI": {
            "description": "Digest value of Subresource Integrity",
            "type": "string"
          },
          "digestMultibase": {
            "description": "Digest value of multihash encoded in multibase.",
            "type": "string"
          }
        },
        "required": ["id"],
        "anyOf": [
          {
            "properties": {
              "digestSRI": {
                "description": "Digest value of Subresource Integrity",
                "type": "string"
              }
            },
            "required": ["digestSRI"]
          },
          {
            "properties": {
              "digestMultibase": {
                "description": "Digest value of multihash encoded in multibase.",
                "type": "string"
              }
            },
            "required": ["digestMultibase"]
          }
        ]
      }
    }
  },
  "required": ["@context", "type"]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm2.0-presentation-schema@1.0.4

# with Yarn
yarn add @cef-ebsi/vcdm2.0-presentation-schema@1.0.4

# with pnpm
pnpm add @cef-ebsi/vcdm2.0-presentation-schema@1.0.4
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm2.0-presentation-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { EBSIVerifiablePresentation } from "@cef-ebsi/vcdm2.0-presentation-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
