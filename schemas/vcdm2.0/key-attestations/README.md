![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm2.0-key-attestations-schema

> Key Attestation
>
> Schema for key attestation

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x6552a78a66ee3d389947718956415558760cc4afd6177609e293a4fd845146f3` (hexadecimal)
- `z7pXH6VJDnsb3dwFXvUda4X5cUQXNoLxj74uAvx1RhUEv` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Key Attestation",
  "description": "Schema for key attestation",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm2.0-attestation-schema/schema.json"
    },
    {
      "properties": {
        "type": {
          "description": "Defines the Verifiable Credential type. MUST contain KeyAttestation2023",
          "type": "array",
          "contains": {
            "type": "string",
            "const": "KeyAttestation2023"
          }
        }
      }
    },
    {
      "properties": {
        "credentialSubject": {
          "description": "Contains information about the signing key.",
          "type": "object",
          "properties": {
            "id": {
              "description": "DID key identifier to which the key attestation is being issued. DID+fragment",
              "type": "string",
              "format": "uri"
            },
            "jwk": {
              "description": "Public key in a JWK format. Claim is required for the did:ebsi and MUST NOT be present for did:key. It should contain only the required JWK claims.",
              "type": "string"
            },
            "keyType": {
              "description": "OPTIONAL. The type of a key. Default: public. https://w3c.github.io/webcrypto/#cryptokey-interface-types",
              "type": "string",
              "enum": ["public", "private", "secret"]
            },
            "keyUsage": {
              "description": "OPTIONAL. Default: sign. Defines the operations for which the key is intended to use. ETSI or webcrypto semantics can be used: https://www.etsi.org/deliver/etsi_en/319400_319499/31941202/02.02.01_60/en_31941202v020201p.pdf or https://w3c.github.io/webcrypto/#cryptokey-interface-types. SHOULD contain only 1 value - should match the value in the DID Document digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment, keyAgreement, keyCertSign, cRLSign, encipherOnly, and decipherOnly. There is also a set of additional key usage. Should also express the info from the DID Document verification method. Also: https://www.rfc-editor.org/rfc/rfc5280.html#section-4.2.1.12",
              "type": "array",
              "items": {
                "type": "string",
                "enum": [
                  "encrypt",
                  "decrypt",
                  "sign",
                  "verify",
                  "deriveKey",
                  "deriveBits",
                  "wrapKey",
                  "unwrapKey"
                ]
              }
            },
            "QCStatements": {
              "description": "Contains one or more (quality) statements about the key. See ETSI EN 319 412-5 V2.3.1 (2020-04) for guidance on processing.",
              "type": "object",
              "properties": {
                "QCCompliance": {
                  "description": "id-etsi-qcs-QcCompliance, OID:0.4.0.1862.1.1, QCStatement claiming that the certificate is a EU qualified certificate or a certificate being qualified within a defined legal framework from an identified country or set of countries"
                },
                "QCSSCD": {
                  "description": "id-etsi-qcs-QcSSCD, OID:0.4.0.1862.1.4, QCStatement claiming that the private key related to the certified public key resides in a QSCD"
                },
                "QCType": {
                  "description": "id-etsi-qcs-QcType, OID:0.4.0.1862.1.6, QCStatement claiming that the certificate is a certificate of a particular type",
                  "type": "string",
                  "enum": ["esign", "eseal", "web"]
                },
                "QCCCLegislation": {
                  "description": "id-etsi-qcs-QcCClegislation, OID:0.4.0.1862.1.7, QcStatement stating the country or set of countries under the legislation of which the certificate is issued as a qualified certificate",
                  "type": "array",
                  "items": {
                    "description": "ISO 3166-1 alpha-2 country code",
                    "type": "string",
                    "pattern": "^[A-Z]{2}$"
                  }
                },
                "QCLimitValue": {
                  "description": "id-etsi-qcs-QcLimitValue, OID:0.4.0.1862.1.2, QCStatement regarding limits on the value of transactions",
                  "type": "object",
                  "properties": {
                    "currency": {
                      "description": "ISO 4217 country code, numeric or alphabetic",
                      "oneOf": [
                        {
                          "type": "string",
                          "pattern": "^[A-Z]{3}"
                        },
                        {
                          "type": "integer",
                          "minimum": 1,
                          "maximum": 999
                        }
                      ]
                    },
                    "amount": {
                      "description": "value = amount * 10^exponent",
                      "type": "integer",
                      "minimum": 0
                    },
                    "exponent": {
                      "type": "integer",
                      "minimum": 0
                    }
                  }
                },
                "QCRetentionPeriod": {
                  "description": "id-etsi-qcs-QcRetentionPeriod, OID:0.4.0.1862.1.3, QCStatement indicating the duration of the retention period of material information",
                  "type": "integer"
                },
                "QCPDS": {
                  "description": "id-etsi-qcs-QcPDS, OID:0.4.0.1862.1.5, QCStatement regarding location of PKI Disclosure Statements (PDS)",
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "url": {
                        "type": "string",
                        "format": "uri"
                      },
                      "language": {
                        "description": "ISO 639-1 language code",
                        "type": "string",
                        "pattern": "^[a-z]{2}"
                      }
                    }
                  }
                }
              }
            }
          },
          "required": ["id"]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm2.0-key-attestations-schema@1.2.1

# with Yarn
yarn add @cef-ebsi/vcdm2.0-key-attestations-schema@1.2.1

# with pnpm
pnpm add @cef-ebsi/vcdm2.0-key-attestations-schema@1.2.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm2.0-key-attestations-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { KeyAttestation } from "@cef-ebsi/vcdm2.0-key-attestations-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
