![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm2.0-lpid-schema

> Legal Person Identification Data (LPID)
>
> Data Type for the Legal Person Identification Data (LPID)

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xe2a64248941def3e4d53026a8c078891cb0eb91d3a6dc31bff7dcc81e35b89ef` (hexadecimal)
- `zGFkBSGfJPxBeKSB3Ps7vpkCFXtgdbLJ7DQwAWaWncxPG` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Legal Person Identification Data (LPID)",
  "description": "Data Type for the Legal Person Identification Data (LPID)",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm2.0-attestation-schema/schema.json"
    },
    {
      "$ref": "#/$defs/lpid"
    }
  ],
  "$defs": {
    "lpid": {
      "type": "object",
      "properties": {
        "issuingAuthorityId": {
          "type": "string"
        },
        "issuingAuthority": {
          "type": "string"
        },
        "issuerCountry": {
          "type": "string"
        },
        "issuingJurisdiction": {
          "type": "string"
        },
        "issuanceDate": {
          "type": "string"
        },
        "expiryDate": {
          "type": "string"
        },
        "schemaId": {
          "type": "string"
        },
        "schemaVersion": {
          "type": "string"
        },
        "schemaLocation": {
          "type": "string"
        },
        "revocationId": {
          "type": "string"
        },
        "revocationLocation": {
          "type": "string"
        },
        "authenticSourceId": {
          "type": "string"
        },
        "authenticSourceName": {
          "type": "string"
        },
        "credentialSubject": {
          "type": "object",
          "description": "Data Type for the Legal Person Identification Data (LPID)",
          "properties": {
            "legalPersonId": {
              "type": "string"
            },
            "legalPersonName": {
              "type": "string"
            }
          },
          "required": ["legalPersonId", "legalPersonName"]
        }
      },
      "required": [
        "issuingAuthorityId",
        "issuingAuthority",
        "issuerCountry",
        "issuingJurisdiction",
        "issuanceDate",
        "expiryDate",
        "schemaId",
        "schemaVersion",
        "schemaLocation",
        "revocationId",
        "revocationLocation",
        "authenticSourceId",
        "authenticSourceName",
        "credentialSubject"
      ]
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm2.0-lpid-schema@1.0.0

# with Yarn
yarn add @cef-ebsi/vcdm2.0-lpid-schema@1.0.0

# with pnpm
pnpm add @cef-ebsi/vcdm2.0-lpid-schema@1.0.0
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm2.0-lpid-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { LegalPersonIdentificationDataLPID } from "@cef-ebsi/vcdm2.0-lpid-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
