![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm2.0-pm2-credential-schema

> PM2 Credential
>
> Schema of a PM2 Verifiable Credential

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xc1e76918867bf44a237b7806c455b0e76e405bb3a48c3c91dad80b1a9ade3a8f` (hexadecimal)
- `zE3vM6144Y35WzbExEzfjFXTk9huy5zMKpJBqRtLBT95x` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "PM2 Credential",
  "description": "Schema of a PM2 Verifiable Credential",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm2.0-attestation-schema/schema.json"
    },
    {
      "properties": {
        "type": {
          "description": "Defines the Verifiable Credential type.",
          "type": "array",
          "contains": {
            "type": "string",
            "const": "PM2Credential"
          }
        },
        "credentialSubject": {
          "description": "Defines additional properties on credentialSubject to describe IDs that do not have a substantial level of assurance.",
          "type": "object",
          "properties": {
            "id": {
              "description": "Defines a unique identifier of the credential subject",
              "type": "string"
            },
            "givenName": {
              "description": "Name strings that are the part of a person's name that is not their surname (see RFC4519).",
              "type": "string"
            },
            "familyName": {
              "description": "Name strings that are a person's surname (see RFC4519).",
              "type": "string"
            },
            "emailAddress": {
              "description": "Email address of the user.",
              "type": "string",
              "format": "email"
            },
            "courses": {
              "description": "Courses in this credential",
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "name": {
                    "description": "Name of course",
                    "type": "string"
                  }
                },
                "required": ["name"]
              }
            },
            "credentialDescription": {
              "description": "Description for this credential",
              "type": "string"
            },
            "credentialId": {
              "description": "Id for this credential",
              "type": "string"
            }
          },
          "required": [
            "id",
            "givenName",
            "familyName",
            "emailAddress",
            "courses",
            "credentialDescription",
            "credentialId"
          ]
        }
      }
    }
  ]
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm2.0-pm2-credential-schema@1.2.1

# with Yarn
yarn add @cef-ebsi/vcdm2.0-pm2-credential-schema@1.2.1

# with pnpm
pnpm add @cef-ebsi/vcdm2.0-pm2-credential-schema@1.2.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm2.0-pm2-credential-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { PM2Credential } from "@cef-ebsi/vcdm2.0-pm2-credential-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
