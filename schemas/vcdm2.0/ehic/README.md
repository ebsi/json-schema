![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm2.0-ehic

> electronic European Health Insurance Card (e)EHIC
>
> Data Type for the electronic European Health Insurance Card

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x9c34783ff96e293c54e9707b250f25d40eaaaa0fac4112f9390aa1f265267151` (hexadecimal)
- `zBWm1JyQAVS6WGcfdhXbbyWeavq2oX1xBTaHwsYjdepMe` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "electronic European Health Insurance Card (e)EHIC",
  "description": "Data Type for the electronic European Health Insurance Card",
  "type": "object",
  "allOf": [
    {
      "$ref": "./node_modules/@cef-ebsi/vcdm2.0-attestation-schema/schema.json"
    },
    {
      "type": "object",
      "properties": {
        "credentialSubject": {
          "description": "Data Type for the electronic European Health Insurance Card",
          "$ref": "#/$defs/eEHIC"
        }
      },
      "required": ["credentialSubject"]
    }
  ],
  "$defs": {
    "eEHIC": {
      "title": "electronic European Health Insurance Card (e)EHIC",
      "description": "Data Type for the electronic European Health Insurance Card",
      "type": "object",
      "allOf": [
        {
          "properties": {
            "v": {
              "title": "Schema version",
              "description": "Version of the schema, according to Semantic versioning (ISO, https://semver.org/ version 2.0.0 or newer)",
              "type": "string",
              "pattern": "^\\d+.\\d+.\\d+$",
              "examples": ["1.0.0", "1.3.0", "1.3.1", "1.3.2", "1.3.3"]
            },
            "hn": {
              "title": "Card Holder Name",
              "description": "Surname(s), forename(s) - in that order",
              "$ref": "#/$defs/person_name"
            },
            "dob": {
              "title": "Date of birth of card holder",
              "description": "Date of Birth of the person addressed in the DCC. ISO 8601 date format restricted to range 1900-2099 or empty",
              "type": "string",
              "pattern": "^((19|20)\\d\\d(-\\d\\d){0,2}){0,1}$",
              "examples": ["1979-04-14", "1950", "1901-08", ""]
            },
            "hi": {
              "title": "Card Holder Identification number",
              "description": "The personal identification number detail used by the issuing Member State.",
              "type": "string",
              "pattern": "\\d{0,20}",
              "maxLength": 20
            },
            "ii": {
              "title": "Card Issuer Identification number",
              "description": "Identification code awarded nationally to the 'institution', viz. the competent institution of insurance.",
              "type": "string",
              "pattern": "\\d{4,10}",
              "minLength": 4,
              "maxLength": 10
            },
            "in": {
              "title": "Card Issuer Name",
              "description": "The acronym of the institution is provided instead of the full name.",
              "type": "string",
              "maxLength": 21
            },
            "ci": {
              "title": "Card Identification number",
              "description": "Logical identification number of the card",
              "$ref": "#/$defs/certificate_id"
            },
            "ic": {
              "title": "Card Issuer Country",
              "description": "Identification code of the card issuer's state. (2 digit ISO country code (ISO 3166-1) + 'UK').",
              "type": "string",
              "$ref": "#/$defs/countryCode"
            },
            "sd": {
              "title": "Start date",
              "description": "Start date of the entitlement to receive health care during a temporary stay in a Member State other than the insuring Member State.",
              "type": "string",
              "format": "date"
            },
            "ed": {
              "title": "End date",
              "description": "End date of the entitlement to receive health care during a temporary stay in a Member State other than the insuring Member State.",
              "type": "string",
              "format": "date"
            }
          },
          "required": ["v", "hn", "ci", "ic", "sd", "ed"]
        },
        {
          "description": "The sum of lengths of ii and in must not exceed 25 characters.",
          "anyOf": [
            {
              "properties": {
                "ii": {
                  "maxLength": 10
                },
                "in": {
                  "maxLength": 15
                }
              }
            },
            {
              "properties": {
                "ii": {
                  "maxLength": 9
                },
                "in": {
                  "maxLength": 16
                }
              }
            },
            {
              "properties": {
                "ii": {
                  "maxLength": 8
                },
                "in": {
                  "maxLength": 17
                }
              }
            },
            {
              "properties": {
                "ii": {
                  "maxLength": 7
                },
                "in": {
                  "maxLength": 18
                }
              }
            },
            {
              "properties": {
                "ii": {
                  "maxLength": 6
                },
                "in": {
                  "maxLength": 19
                }
              }
            },
            {
              "properties": {
                "ii": {
                  "maxLength": 5
                },
                "in": {
                  "maxLength": 20
                }
              }
            },
            {
              "properties": {
                "ii": {
                  "maxLength": 4
                },
                "in": {
                  "maxLength": 21
                }
              }
            }
          ]
        }
      ]
    },
    "certificate_id": {
      "description": "Certificate Identifier, Format Explained",
      "type": "string",
      "minLength": 20,
      "maxLength": 20
    },
    "person_name": {
      "description": "Person name: The person's name consisting at least of a separate standardised surname, or a standardised forename, or both - with standardisation done according to the rules defined in ICAO Doc 9303 Part 3",
      "anyOf": [
        {
          "required": ["fnt"]
        },
        {
          "required": ["gnt"]
        }
      ],
      "type": "object",
      "properties": {
        "fn": {
          "title": "Surname",
          "description": "The surname or primary name(s) of the person addressed in the certificate",
          "type": "string",
          "maxLength": 80,
          "examples": ["d'Červenková Panklová"]
        },
        "fnt": {
          "title": "Standardised surname",
          "description": "The surname(s) of the person, transliterated ICAO 9303",
          "type": "string",
          "pattern": "^[A-Z<]*$",
          "maxLength": 80,
          "examples": ["DCERVENKOVA<PANKLOVA"]
        },
        "gn": {
          "title": "Forename",
          "description": "The forename(s) of the person addressed in the certificate",
          "type": "string",
          "maxLength": 80,
          "examples": ["Jiřina-Maria Alena"]
        },
        "gnt": {
          "title": "Standardised forename",
          "description": "The forename(s) of the person, transliterated ICAO 9303",
          "type": "string",
          "pattern": "^[A-Z<]*$",
          "maxLength": 80,
          "examples": ["JIRINA<MARIA<ALENA"]
        }
      }
    },
    "countryCode": {
      "title": "Country Code",
      "description": "Identification code of the country (2 digit ISO country code (ISO 3166-1)), with the addition of UK for GB.",
      "enum": [
        "AT",
        "BE",
        "BG",
        "HR",
        "CY",
        "CZ",
        "DK",
        "EE",
        "FI",
        "FR",
        "DE",
        "EL",
        "HU",
        "IS",
        "IE",
        "IT",
        "LV",
        "LI",
        "LT",
        "LU",
        "MT",
        "NL",
        "NO",
        "PL",
        "PT",
        "RO",
        "SK",
        "SI",
        "ES",
        "SE",
        "CH",
        "UK"
      ]
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm2.0-ehic@1.2.1

# with Yarn
yarn add @cef-ebsi/vcdm2.0-ehic@1.2.1

# with pnpm
pnpm add @cef-ebsi/vcdm2.0-ehic@1.2.1
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import { schema, metadata } from "@cef-ebsi/vcdm2.0-ehic";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { ElectronicEuropeanHealthInsuranceCardEEHIC } from "@cef-ebsi/vcdm2.0-ehic";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
