# @cef-ebsi/vcdm2.0-ehic

## 1.2.1

### Patch Changes

- c7f7fa3: Update license text.
- Updated dependencies [c7f7fa3]
  - @cef-ebsi/vcdm2.0-attestation-schema@1.2.1

## 1.2.0

### Minor Changes

- 191a769: Support `credentialStatus` as an array.

### Patch Changes

- Updated dependencies [9e4aa93]
- Updated dependencies [191a769]
  - @cef-ebsi/vcdm2.0-attestation-schema@1.2.0

## 1.2.0-next.0

### Minor Changes

- 191a769: Support `credentialStatus` as an array.

### Patch Changes

- Updated dependencies [9e4aa93]
- Updated dependencies [191a769]
  - @cef-ebsi/vcdm2.0-attestation-schema@1.2.0-next.0

## 1.1.0

### Minor Changes

- 021e4c2: Remove enum from `credentialSchema.type` on base Verifiable Attestation schema.

### Patch Changes

- Updated dependencies [021e4c2]
  - @cef-ebsi/vcdm2.0-attestation-schema@1.1.0

## 1.0.2

### Patch Changes

- e1e8cf3: Export examples in JS file.
- Updated dependencies [e1e8cf3]
  - @cef-ebsi/vcdm2.0-attestation-schema@1.0.2

## 1.0.1

### Patch Changes

- 85a6797: Embed examples in npm package, add JSON Schema to README file.
- Updated dependencies [85a6797]
  - @cef-ebsi/vcdm2.0-attestation-schema@1.0.1

## 1.0.0

### Major Changes

- 3822a05: Create initial version of electronic European Health Insurance Card (e)EHIC schema.
