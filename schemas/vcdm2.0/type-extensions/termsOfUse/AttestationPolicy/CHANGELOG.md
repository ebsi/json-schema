# @cef-ebsi/vcdm2.0-type-extensions-terms-of-use-attestation-policy-schema

## 1.0.3

### Patch Changes

- c7f7fa3: Update license text.

## 1.0.2

### Patch Changes

- e1e8cf3: Export examples in JS file.

## 1.0.1

### Patch Changes

- 85a6797: Embed examples in npm package, add JSON Schema to README file.

## 1.0.0

Define schema as npm package.

## Legacy versions

### 2023-10

- Initial schema.
