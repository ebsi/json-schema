![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm2.0-type-extensions-terms-of-use-attestation-policy-schema

> Attestation Policy
>
> Defines the Attestation Policy under which the Verifiable Credential has been issued

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0x0df30d548391a0ee2a26b49ca0c78b8a25db317fa9f455f424e18836aab89d30` (hexadecimal)
- `zwTFt3rYS78X1rSpqdoH7KuqhftS1sEEteQsNtjcH8mD` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Attestation Policy",
  "description": "Defines the Attestation Policy under which the Verifiable Credential has been issued",
  "type": "object",
  "properties": {
    "termsOfUse": {
      "anyOf": [
        {
          "$ref": "#/$defs/attestationPolicy"
        },
        {
          "type": "array",
          "description": "One item must match the type extension",
          "contains": {
            "$ref": "#/$defs/attestationPolicy"
          }
        }
      ]
    }
  },
  "$defs": {
    "attestationPolicy": {
      "description": "Defines the Attestation Policy under which the Verifiable Credential has been issued",
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "description": "Type of the termsOfUse entry. It MUST be AttestationPolicy",
          "enum": ["AttestationPolicy"]
        },
        "parentAccreditation": {
          "type": "string",
          "description": "URL to access the parent entity in the trust chain supporting the attestation",
          "format": "uri"
        },
        "rootAuthorisation": {
          "type": "string",
          "description": "URL to access the root entity in the trust chain supporting the attestation",
          "format": "uri"
        }
      },
      "required": ["type", "parentAccreditation", "rootAuthorisation"]
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm2.0-type-extensions-terms-of-use-attestation-policy-schema@1.0.3

# with Yarn
yarn add @cef-ebsi/vcdm2.0-type-extensions-terms-of-use-attestation-policy-schema@1.0.3

# with pnpm
pnpm add @cef-ebsi/vcdm2.0-type-extensions-terms-of-use-attestation-policy-schema@1.0.3
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm2.0-type-extensions-terms-of-use-attestation-policy-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { AttestationPolicy } from "@cef-ebsi/vcdm2.0-type-extensions-terms-of-use-attestation-policy-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
