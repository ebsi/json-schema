![EBSI Logo](https://ec.europa.eu/digital-building-blocks/sites/images/logo/default-space-logo.svg)

# @cef-ebsi/vcdm2.0-type-extensions-evidence-authorisation-delegation-schema

> Authorisation Delegation
>
> Defines how to include a proof to issue a delegated authorisation

The schema is published to the [Trusted Schemas Registry](https://hub.ebsi.eu/apis/pilot/trusted-schemas-registry) with the IDs:

- `0xc0a0826d048fa7a45f25d64e7509547af066d21dd068bc622a5930fb76a39b8e` (hexadecimal)
- `zDxwEY3CJCWB9Z19pztkGz2T7tkiCkfSwqsE3u5yCHCX7` (multibase base58btc)

## Table of Contents

- [JSON Schema](#json-schema)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## JSON Schema

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Authorisation Delegation",
  "description": "Defines how to include a proof to issue a delegated authorisation",
  "type": "object",
  "properties": {
    "evidence": {
      "anyOf": [
        {
          "$ref": "#/$defs/evidence"
        },
        {
          "type": "array",
          "description": "One item must match the type extension",
          "contains": {
            "$ref": "#/$defs/evidence"
          }
        }
      ]
    }
  },
  "$defs": {
    "evidence": {
      "description": "Contains information about the process which resulted in the issuance of the credential",
      "type": "object",
      "properties": {
        "id": {
          "description": "CONDITIONAL. A unique id that identifies the evidence.MUST be used if more than one evidence is present",
          "type": "string"
        },
        "type": {
          "anyOf": [
            {
              "description": "Defines the evidence type extension",
              "const": "AuthorisationDelegation"
            },
            {
              "description": "Defines the evidence type extension",
              "type": "array",
              "contains": {
                "const": "AuthorisationDelegation"
              }
            }
          ]
        },
        "delegation": {
          "description": "Verifiable Credential based on which the delegated Verifiable Authorisation has been issued",
          "oneOf": [
            {
              "description": "Compact serialised JWS",
              "type": "string"
            },
            {
              "description": "JSON serialised JWS",
              "type": "object"
            }
          ]
        }
      },
      "required": ["type", "delegation"]
    }
  }
}
```

## Installation

```sh
# with npm
npm add @cef-ebsi/vcdm2.0-type-extensions-evidence-authorisation-delegation-schema@1.0.3

# with Yarn
yarn add @cef-ebsi/vcdm2.0-type-extensions-evidence-authorisation-delegation-schema@1.0.3

# with pnpm
pnpm add @cef-ebsi/vcdm2.0-type-extensions-evidence-authorisation-delegation-schema@1.0.3
```

## Usage

The package exports the schema and its metadata as JavaScript objects:

```js
import {
  schema,
  metadata,
} from "@cef-ebsi/vcdm2.0-type-extensions-evidence-authorisation-delegation-schema";

// you can now use the schema and metadata
```

In addition, the package exports a TypeScript type corresponding to the schema:

```ts
import type { AuthorisationDelegation } from "@cef-ebsi/vcdm2.0-type-extensions-evidence-authorisation-delegation-schema";
```

## License

Copyright (C) 2024 European Union

This program is free software: you can redistribute it and/or modify it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 as published by the European Union.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the EUROPEAN UNION PUBLIC LICENCE v. 1.2 for further details.

You should have received a copy of the EUROPEAN UNION PUBLIC LICENCE v. 1.2. along with this program. If not, see <https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12>.
