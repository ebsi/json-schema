# @cef-ebsi/vcdm2.0-attestation-schema

## 1.2.1

### Patch Changes

- c7f7fa3: Update license text.

## 1.2.0

### Minor Changes

- 191a769: Support `credentialStatus` as an array.

### Patch Changes

- 9e4aa93: Add "context" property to schema metadata.

## 1.2.0-next.0

### Minor Changes

- 191a769: Support `credentialStatus` as an array.

### Patch Changes

- 9e4aa93: Add "context" property to schema metadata.

## 1.1.0

### Minor Changes

- 021e4c2: Remove enum from `credentialSchema.type` on base Verifiable Attestation schema.

## 1.0.2

### Patch Changes

- e1e8cf3: Export examples in JS file.

## 1.0.1

### Patch Changes

- 85a6797: Embed examples in npm package, add JSON Schema to README file.

## 1.0.0

Define schema as npm package.

## Legacy versions

### 2023-10 (VCDM 2.0)

- Update `@context` to only be set

### 2023-09 (VCDM 2.0)

- Update `evidence` to contain one or many objects

### 2023-08 (VCDM 2.0)

The purpose is to transition to W3C Verifiable Credentials Data Model V2.0.

- Remove the `issuanceDate`, `expirationDate`, and `issued` properties
- Update the requirement for the @context
- Extend the `issuer` property: can be a URI or an object. Object MUST have an id property
- Make the `proof` property more generic
- Allow `credentialSubject` property to be an array of objects or an object
- Migrate to [JsonSchema](https://www.w3.org/TR/vc-json-schema/#jsonschema)
- Update examples
- Add `ShaclValidator2017`
