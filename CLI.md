# CLI

The repository comes with a CLI.

In order to use the CLI, you must have a functional development environment including Node.js. We recommend using [nvm](https://github.com/nvm-sh/nvm) or [fnm](https://github.com/Schniz/fnm) to install the Node.js version specified in the `.nvmrc` file present in this repository.

Once Node.js is installed, enable [Corepack](https://nodejs.org/api/corepack.html) to install the package manager (we use [pnpm](https://pnpm.io/)):

```sh
corepack enable
```

Install the dependencies:

```sh
pnpm i
```

You can now call the CLI by running `pnpm ejsc` (EBSI JSON Schema CLI).

## `compute-id` command <!-- omit in toc -->

```sh
# Compute the ID of a specific schema
# Example: pnpm ejsc compute-id schemas/ebsi-attestation/2021-11/schema.json
pnpm ejsc compute-id [path]

# Compute all IDs
pnpm ejsc compute-id --all

# Compute IDs of chosen files (a prompt will appear)
pnpm ejsc compute-id
```

## `publish` command <!-- omit in toc -->

```sh
# Publish a specific schema
# Example: ejsc publish schemas/ebsi-attestation/2021-11/schema.json
pnpm ejsc publish [path]

# Publish all schemas
pnpm ejsc publish --all

# Publish the chosen schemas (a prompt will appear)
pnpm ejsc publish
```

Tip: you can create a `.env` file to store your credentials, so you don't have to enter them every time you run the `publish` command.

Example:

```sh
EBSI_ENV=pilot
TSR_API_VERSION=v3
KID="did:ebsi:z...#..."
PRIVATE_KEY=0x...
```

The CLI supports 4 different environment variables:

- `EBSI_ENV`: the environment to which the schemas should be deployed. Valid values: `test`, `conformance`, `pilot`.
- `TSR_API_VERSION`: version of TSR API to which the schemas will be published. Valid values: `v2`, `v3`, `v4`.
- `KID`: the deployer's KID.
- `PRIVATE_KEY`: the deployer's private key, in hexadecimal, prefixed with `0x`.

## `sign` command <!-- omit in toc -->

This command helps signing arbitrary JSON objects. Keys (public and private) can be found from [actors.js](cli/commands/sign/actors.js) for each valid actor. The signer depends on the use case and is selectable by the caller. Use `--type jwt` for JSON Web Token (JWT) and `--type jws` for General JSON Web Signature (JWS) Serialisation.

```sh
# Sign all files found with the glob in JWT as Alice. Will target all 2022-05 examples
pnpm ejsc sign --actors alice --type jwt schemas/**/2022-05/examples/**

# Sign all files found with the glob in JWT as issuer. Will target all 2022-05 examples
pnpm ejsc sign --actors issuer --type jwt schemas/**/2022-05/examples/**

# Sign all files found with the glob in JWS as both issuer and issuer2. Will target all 2022-05 examples
pnpm ejsc sign --actors issuer issuer2 --type jws schemas/**/2022-05/examples/**
```
