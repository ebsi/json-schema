# JAdES v1.1.1 + DPKI JSON schemas and examples

> Here you can find the official JAdES v1.1.1 JSON schemas and examples from the [DSS Library](https://github.com/esig/dss/tree/master/specs-jades/src/test/resources) and example signed and unsigned headers extend with DPKI properties.

## Overview

Directory is structured as follows

- examples: directory that contains example JAdES signatures and protected/unprotected headers
- rfcs: directory that contains JSON schemas of JOSE header parameters as defined in the corresponding RFCs
- `19182-*.json` files are JAdES JSON schemas
- `definitions.json` is a collection of common definitions

Note: all JSON schemas are defined according to the JSON schema draft-07 specification.

## Validation

### Install ajv validator or use online validator

To validate the examples against the JSON schemas you either need to install the ajv CLI or use one of the [online JSON schema validation tools](<https://json-schema.org/implementations.html#validator-web%20(online)>). Ensure that the online validator you select supports JSON schema version `draft-07`.

To install `ajv` CLI follow the [official instructions](https://www.npmjs.com/package/ajv-cli).

### Validation using AJV CLI

> All examples will return warnings about "unknown" format. Issue will be resolved once schemas are migrated to version JSON schema version 2020-12.

Validate the JAdES protected header:

```sh
ajv validate --strict=false -s 19182-protected-jsonSchema.json -d examples/jades-lta-protected.json
```

Validate the JAdES unprotected header:

```sh
ajv validate --strict=false -s 19182-unprotected-jsonSchema.json -d examples/jades-lta-unprotected.json
```

We also define two additional examples that contain additional claims used to validate the signature and the signer using decentralised PKI.

Validate the JAdES+DPKI protected header:

```sh
ajv validate --strict=false -s 19182-protected-jsonSchema.json -d examples/jades-lta-protected+dpki.json
```

Validate the JAdES+DPKI unprotected header:

```sh
ajv validate --strict=false -s 19182-unprotected-jsonSchema.json -d examples/jades-lta-unprotected+dpki.json
```

## References

- [DSS library examples](https://github.com/esig/dss/tree/master/specs-jades/src/test/resources)
- [DSS JSON Schemas](https://github.com/esig/dss/tree/master/specs-jades/src/main/resources/schema)
- [ETSI signatures conformance checker](https://signatures-conformance-checker.etsi.org/pub/index.php)
