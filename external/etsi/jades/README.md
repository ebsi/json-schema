# JSON Advanced electronic Digital Signatures

> JSON schemas for JSON AdES signatures

## Changes

### 2023-07

- Add first schemas

### v1.1.1 - ETSI JAdES JSON schemas

Unmodified JAdES JSON schemas from [ETSI specifications](https://www.etsi.org/deliver/etsi_ts/119100_119199/11918201/01.01.01_60/ts_11918201v010101p.pdf). Version v1.1.1

Reference repository: [ETSI JAdES Repository](https://forge.etsi.org/rep/esi/x19_182_JAdES)

[ETSI License](https://forge.etsi.org/rep/esi/x19_182_JAdES/-/blob/master/LICENSE)
